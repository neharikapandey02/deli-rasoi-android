package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.ui.change_password.ChangePassword;
import com.example.desirasoi.adapters.ForgotPasswordMain;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.facebook.login.LoginManager;
import com.google.android.material.snackbar.Snackbar;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.et_oldPassword)
    EditText et_oldPassword;

    @BindView(R.id.et_newPassword)
    EditText et_newPassword;

    @BindView(R.id.et_confirmNewPassword)
    EditText et_confirmNewPassword;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imgview_back)
    ImageView imgview_back;


    @BindView(R.id.btn_changePassword)
    Button btn_changePassword;

    View view;
    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        //tv_title_name=getWindow().getDecorView().getRootView().findViewById(R.id.tv_title_name);
    }

    @Override
    public void onResume() {
        super.onResume();
        //tv_title_name=getWindow().getDecorView().getRootView().findViewById(R.id.tv_title_name);
        //tv_title_name.setText("Change Password");
    }

    @OnClick(R.id.btn_changePassword)
    public void updatePasswordClicked(View view){
        //Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                String oldPassword = et_oldPassword.getText().toString().trim();
                String newPassword = et_newPassword.getText().toString().trim();
                sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
                String email=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL);
                RetrofitHelper.getInstance().doChangePassword(forgotPasswordCallback, newPassword, newPassword,oldPassword,email);
            }



        } else {
            Snackbar.make(btn_changePassword, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }


    Callback<ForgotPasswordMain> forgotPasswordCallback = new Callback<ForgotPasswordMain>() {
        @Override
        public void onResponse(Call<ForgotPasswordMain> call, Response<ForgotPasswordMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess().equalsIgnoreCase("true")){
                    sharedPreferencesData.clearSharedPreferenceData(Constants.USER_CREATE);
                    LoginManager.getInstance().logOut();
                    startActivity(new Intent(ChangePasswordActivity.this, LoginActivity.class));
                    Toast.makeText(ChangePasswordActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Snackbar.make(btn_changePassword,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<ForgotPasswordMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    public boolean validation(){
        if (et_oldPassword.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_changePassword, getResources().getString(R.string.enter_old_password), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_newPassword.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_changePassword, getResources().getString(R.string.enter_new_password), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_confirmNewPassword.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_changePassword, getResources().getString(R.string.re_enter_password), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (!et_confirmNewPassword.getText().toString().equalsIgnoreCase(et_newPassword.getText().toString())){
            Snackbar.make(btn_changePassword, getResources().getString(R.string.new_password_and_confirm), Snackbar.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
    @OnClick(R.id.imgview_back)
    public void backClicked(View view){
        onBackPressed();
    }
}
