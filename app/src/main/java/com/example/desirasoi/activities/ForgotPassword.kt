package com.example.desirasoi.activities

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.desirasoi.R
import com.example.desirasoi.constants.NetworkUtil
import com.example.desirasoi.retrofit.RetrofitHelper
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPassword : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE) //will hide the title
        supportActionBar!!.hide()
        setContentView(R.layout.activity_forgot_password)
        viewFinds()
    }

    private fun viewFinds() {
        btn_submit.setOnClickListener {

            //Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
            try {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            } catch (e: Exception) {
                // TODO: handle exception
            }
            if (et_email.text.toString().equals("")){
                Snackbar.make(btn_submit,resources.getString(R.string.enter_email),Snackbar.LENGTH_LONG).show()
            }else{
                //Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                if (NetworkUtil.checkNetworkStatus(applicationContext)) {
                        progressBar.visibility = View.VISIBLE
                        val email: String = et_email.getText().toString().trim({ it <= ' ' })
                    if (!email.equals("")){
                        RetrofitHelper.getInstance().doForgotPassword(forgotPasswordCallback, email)
                    }else{
                        Snackbar.make(btn_submit, resources.getString(R.string.enter_email), Snackbar.LENGTH_LONG).show()
                    }


                } else {
                    Snackbar.make(btn_submit, resources.getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }


    var forgotPasswordCallback: Callback<com.example.desirasoi.models.ForgotPasswordMain> = object : Callback<com.example.desirasoi.models.ForgotPasswordMain> {
        override fun onResponse(call: Call<com.example.desirasoi.models.ForgotPasswordMain>, response: Response<com.example.desirasoi.models.ForgotPasswordMain>) {
            progressBar.visibility = View.GONE
            if (response.isSuccessful) {
                if (response.body()!!.success.equals("true", ignoreCase = true)) {
                    et_email.setText("");
                    val alert = android.app.AlertDialog.Builder(this@ForgotPassword)
                    alert.setTitle("Alert")
                    alert.setMessage(response.body()!!.message)
                    alert.setPositiveButton("oK") {
                        dialog, which -> dialog.dismiss()
                    }
                    val alertDialog: android.app.AlertDialog = alert.create()
                    alertDialog.show()
                } else {
                    Snackbar.make(btn_submit, response.body()!!.message, Snackbar.LENGTH_LONG).show()
                }
            }
        }

        override fun onFailure(call: Call<com.example.desirasoi.models.ForgotPasswordMain>, t: Throwable) {
            progressBar.visibility = View.GONE
        }
    }


}
