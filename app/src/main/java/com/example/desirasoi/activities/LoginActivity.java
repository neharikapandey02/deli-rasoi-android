package com.example.desirasoi.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.addaddress.SelectAddressActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.btn_login)
    Button btn_login;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.tv_forgotPassword)
    TextView tv_forgotPassword;

    private SharedPreferencesData sharedPreferencesData;

    CallbackManager callbackManager;
    LoginButton imgview_facebookLogin;
    private static final String EMAIL = "email";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        viewFinds();
        forFacebookLogin();
        //Log.e("TAG","hash="+printKeyHash(LoginActivity.this));
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finishAffinity();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        sharedPreferencesData.createNewSharedPreferences(Constants.USER_CREATE);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("device_id", ""+task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.DEVICE_TOKEN, token);
                        // Log and toast
                        Log.e("device_id", token);
//                        Toast.makeText(SplashActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @OnClick(R.id.tv_signup_user)
    public void signupClicked(View view){

        try {
                Intent intent=new Intent(getApplicationContext(), SignupActivity.class);
                intent.putExtra(Constants.CART_ID, getIntent().getStringExtra(Constants.CART_ID));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
        }catch (Exception e){
            e.printStackTrace();
            Intent intent=new Intent(getApplicationContext(), SignupActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }

        /*Intent intent=new Intent(getApplicationContext(),TestActivity.class);
        startActivity(intent);*/
    }

    @OnClick(R.id.tv_forgotPassword)
    public void forgotClicked(View view){
        Intent intent=new Intent(getApplicationContext(), ForgotPassword.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_login)
    public void loginClicked(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                String email = et_email.getText().toString().trim();
                String password = et_password.getText().toString().trim();
                RetrofitHelper.getInstance().doLogin(loginCallback, email, password,
                        sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.DEVICE_TOKEN)
                        , sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID));
            }
        } else {
            Snackbar.make(btn_login, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }


    private boolean validation(){
        if (et_email.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_email,getResources().getString(R.string.enter_email),Snackbar.LENGTH_LONG).show();
            return false;
        }else
            if (et_password.getText().toString().equalsIgnoreCase("")){
                Snackbar.make(et_email,getResources().getString(R.string.enter_password),Snackbar.LENGTH_LONG).show();
                return false;
        }
        return true;
    }


    Callback<LoginMain> loginCallback = new Callback<LoginMain>() {
        @Override
        public void onResponse(Call<LoginMain> call, Response<LoginMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess().equalsIgnoreCase("true")){
                    if (response.body().getResult()!=null){
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID,response.body().getResult().getId());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME,response.body().getResult().getFirstName());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME,response.body().getResult().getLastName());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.MOBILE_NUMBER,response.body().getResult().getContact());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL,response.body().getResult().getEmail());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.HOUSE,response.body().getResult().getHouse());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.CITY,response.body().getResult().getCity());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.STATE,response.body().getResult().getStreet());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.POSTAL_CODE,response.body().getResult().getPostalCode());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.COUNTRY,response.body().getResult().getCountry());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.AUTH_TOKEN,response.body().getResult().getAuthToken());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE,response.body().getResult().getImage());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.NEWSLETTER_STATUS, response.body().getResult().getNewsletter_status());

                        Log.d("TAG","AuthToken="+response.body().getResult().getAuthToken());
                        Log.d("TAG","UserId="+response.body().getResult().getId());
                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                        try {
                            if (getIntent().getStringExtra("page").equalsIgnoreCase("cart")){

                                Intent intent=new Intent(getApplicationContext(), OrderSummaryActivity.class);
                                intent.putExtra(Constants.CHECKOUT_ID, getIntent().getStringExtra(Constants.CART_ID));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Intent intent=new Intent(getApplicationContext(), MainPageActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Intent intent=new Intent(getApplicationContext(), MainPageActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }
                }else{
                    Snackbar.make(et_email,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<LoginMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.tv_forgotPassword)
    public void forgotPasswordClicked(View view){

    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    private void forFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        imgview_facebookLogin = findViewById(R.id.imgview_facebookLogin);
        //login_button.setReadPermissions(Arrays.asList(EMAIL));
        //login_button.setReadPermissions(Arrays.asList("user_status"));
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserProfile(AccessToken.getCurrentAccessToken());
                // App code
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        imgview_facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(EMAIL));
            }
        });
    }
    //facebook data getting from integration
    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("TAG=", object.toString());
                        try {
                            String first_name = object.getString("first_name");
                            String last_name = object.getString("last_name");
                            String email = object.getString("email");
                            String id = object.getString("id");
                            String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";

                            //txtUsername.setText("First Name: " + first_name + "\nLast Name: " + last_name);
                            //txtEmail.setText(email);
                            //Picasso.with(LoginActivity.this).load(image_url).into(imageView);

                            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                progressBar.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getSocialLogin(socialloginCallback,id,email,first_name+" "+last_name,image_url,
                                        Constants.DEVICE_ID,
                                        sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.DEVICE_TOKEN)
                                        ,
                                        sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID));

                            } else {
                                Snackbar.make(btn_login, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_SHORT).show();
                            }
                            Log.e("FB=",first_name+"     ,"+last_name+"   ,"+email+"   ,"+id+"    ,"+image_url+"    ");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
       /* if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }*/
    }
    Callback<LoginMain> socialloginCallback = new Callback<LoginMain>() {
        @Override
        public void onResponse(Call<LoginMain> call, Response<LoginMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getResult()!=null) {
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID,response.body().getResult().getId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME,response.body().getResult().getFirstName());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME,response.body().getResult().getLastName());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.MOBILE_NUMBER,response.body().getResult().getContact());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL,response.body().getResult().getEmail());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.HOUSE,response.body().getResult().getHouse());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.CITY,response.body().getResult().getCity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.STATE,response.body().getResult().getStreet());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.POSTAL_CODE,response.body().getResult().getPostalCode());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.COUNTRY,response.body().getResult().getCountry());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.AUTH_TOKEN,response.body().getResult().getAuthToken());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE,response.body().getResult().getImage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.NEWSLETTER_STATUS, response.body().getResult().getNewsletter_status());

                    Log.d("TAG","AuthToken="+response.body().getResult().getAuthToken());
                    Log.d("TAG","UserId="+response.body().getResult().getId());
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(getApplicationContext(), MainPageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    Snackbar.make(btn_login, response.body().getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onFailure(Call<LoginMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
