package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Window;

import com.example.desirasoi.R;
import com.example.desirasoi.adapters.RestaurantListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
     @BindView(R.id.recyclerview_list)
     RecyclerView recyclerview_list;
     private RestaurantListAdapter restaurantListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        //recyclerview_list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        //recyclerview_list.setHasFixedSize(true);
       // restaurantListAdapter =new RestaurantListAdapter(getApplicationContext());
       // recyclerview_list.setAdapter(restaurantListAdapter);
    }
}
