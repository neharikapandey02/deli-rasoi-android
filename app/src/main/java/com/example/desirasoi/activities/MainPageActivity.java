package com.example.desirasoi.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.aboutus.AboutUsFragment;
import com.example.desirasoi.activities.eventandmnagmt.EventAndManagementFragment;
import com.example.desirasoi.activities.ui.change_password.ChangePassword;
import com.example.desirasoi.activities.ui.history.HistoryFragment;
import com.example.desirasoi.activities.ui.home.HomeFragment;
import com.example.desirasoi.activities.ui.mycart.MyCart;
import com.example.desirasoi.activities.ui.myprofile.MyProfileFragment;
import com.example.desirasoi.activities.ui.reservation.ReservationTable;
import com.example.desirasoi.activities.ui.reserved_table.ReservedTableFragment;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.GetGuestDetails;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.provider.Settings;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.Menu;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPageActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawer;

    @BindView(R.id.imgView_navigationIcon)
    ImageView imgView_navigationIcon;


    @BindView(R.id.tv_title_name)
    TextView tv_title_name;

    public static SwipeRefreshLayout swipeRefresh;



    TextView tv_name;
    TextView tv_email;
    ImageView imageView;

    NavController navController;
    Fragment fragment;
    NavigationView navigationView;
    View view;

    private SharedPreferencesData sharedPreferencesData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ButterKnife.bind(this);
        swipeRefresh = findViewById(R.id.swipeRefresh);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());

        FloatingActionButton fab = findViewById(R.id.fab);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        drawer= findViewById(R.id.drawer_layout);
        final CoordinatorLayout content = (CoordinatorLayout) findViewById(R.id.content);
        navigationView= findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        tv_name=headerView.findViewById(R.id.tv_name);
        tv_email=headerView.findViewById(R.id.tv_email);
        imageView=headerView.findViewById(R.id.imageView);
        if (sharedPreferencesData!=null){
            tv_name.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME)
            +" "+sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME));
            tv_email.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL));
            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE)!=null
                    && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE).equalsIgnoreCase("")){
                Picasso.with(getApplicationContext())
                        .load(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE))
                        .error(R.drawable.profile_placeholder)
                        .fit()
                        .placeholder(R.drawable.profile_placeholder)
                        .into(imageView);
            }
        }

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_reservedtable, R.id.nav_aboutAndContact, R.id.nav_newsAndEvent, R.id.nav_share,R.id.nav_logout, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();


        drawer.setScrimColor(Color.TRANSPARENT);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                content.setTranslationX(slideX);
                //hideKeyboardFrom(getApplicationContext(), getCurrentFocus());

            }
        };


        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        //navigationView.setNavigationItemSelectedListener(MainPageActivity.this);


        navController= Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);



        if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {

            Menu menu = navigationView.getMenu();
            MenuItem item = menu.findItem(R.id.nav_logout);
            item.setVisible(false);
        }

//        logoutClickListner(navigationView);
        try {
            String page = getIntent().getStringExtra("page");
            if (page.equalsIgnoreCase("cart")){
//                fragment = new MyCart();
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
//                flag = true;
                navController.navigate(R.id.nav_slideshow);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    //For logout Dialogue open for logout
    private void logoutClickListner(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id=item.getItemId();
                //it's possible to do more actions on several items, if there is a large amount of items I prefer switch(){case} instead of if()
                if (id==R.id.nav_logout){
                    final Dialog dialog = new Dialog(MainPageActivity.this);
                    dialog.setContentView(R.layout.dialog_logout);
                    Window window = dialog.getWindow();
                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    Button button_yes = dialog.findViewById(R.id.button_yes);
                    button_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            sharedPreferencesData.clearSharedPreferenceData(Constants.USER_CREATE);
                            Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });


                    Button button_no = dialog.findViewById(R.id.button_no);
                    button_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });

                    dialog.show();

                }else if (id==R.id.nav_changepassword){
                    if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {
                        fragment = new ChangePassword();
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

                        flag = true;
                    }
                }else if (id==R.id.nav_gallery){
                    if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {
                        fragment = new MyProfileFragment();
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        flag = true;
                    }
                }else if (id==R.id.nav_slideshow){
//                    if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {
                        fragment = new MyCart();
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        flag = true;
//                    }
                }else if (id==R.id.nav_tools){
                    if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {
                        clearStack();
                        fragment = new HistoryFragment();
                   /* Bundle bundle=new Bundle();
                    bundle.putString(Constants.bundleData,"History");
                    fragment.setArguments(bundle);*/
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        //flag=true;
                        flag = true;
                    }
                }else if (id==R.id.nav_home){
//                    clearStack();
//                    fragment= new HomeFragment();
//                    getSupportFragmentManager().beginTransaction()
//                            .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                    flag=false;

                    navController= Navigation.findNavController(MainPageActivity.this, R.id.nav_host_fragment);
//                    startActivity(new Intent(MainPageActivity.this, MainPageActivity.class));
                    //tv_title_name.setText(getResources().getString(R.string.menu_home));

                }else if (id==R.id.nav_reservedtable){
                    if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {

                        clearStack();
                        fragment = new ReservedTableFragment();
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        flag = true;
                        //tv_title_name.setText(getResources().getString(R.string.menu_home));
                    }

                }else if (id==R.id.nav_aboutAndContact){
                    clearStack();
                    fragment= new AboutUsFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                    flag=true;
                    //tv_title_name.setText(getResources().getString(R.string.menu_home));

                }else if (id==R.id.nav_newsAndEvent){
                    clearStack();
                    fragment= new EventAndManagementFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                    flag=true;
                    //tv_title_name.setText(getResources().getString(R.string.menu_home));

                }
                //This is for maintaining the behavior of the Navigation view
                //NavigationUI.setupWithNavController(navigationView, navController);
                //This is for closing the drawer after acting on it
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //tv_title_name.setText(getResources().getString(R.string.menu_home));
    }

    static  boolean flag=false;

    @Override
    public void onBackPressed() {
        try {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE).equalsIgnoreCase("false")){
                navController.navigate(R.id.nav_home);
            }
        else{
                AlertDialog.Builder alert = new AlertDialog.Builder(MainPageActivity.this);
                alert.setTitle("Exit");
                alert.setMessage("Do you want to exit from the app?");
                alert.setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                   dialog.dismiss();
                    }
                });

                alert.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            dialog.dismiss();
                            finishAffinity();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                alert.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearStack() {
        //Here we are clearing back stack fragment entries
        int backStackEntry = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }

        //Here we are removing all the fragment that are shown here
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
                Fragment mFragment = getSupportFragmentManager().getFragments().get(i);
                if (mFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
                }
            }
        }
    }

    @OnClick(R.id.imgView_navigationIcon)
    public void humbergerClick(View view){
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
           drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_page, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }



}
