package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.addaddress.SelectAddressActivity;
import com.example.desirasoi.activities.ui.mycart.MyCart;
import com.example.desirasoi.activities.ui.mycart.SlideshowViewModel;
import com.example.desirasoi.adapters.CartRecycleViewAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.CartMain;
import com.example.desirasoi.models.CartMenuMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCartActivity extends AppCompatActivity {

    private SlideshowViewModel slideshowViewModel;

    View view1;
    String date = "";
    @BindView(R.id.recycleView_cart)
    RecyclerView recycleView_cart;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.subtotalAmount)
    TextView subtotalAmount;

    @BindView(R.id.taxFeeAmount)
    TextView taxFeeAmount;


    @BindView(R.id.btn_checkOut)
    Button btn_checkOut;

    private SharedPreferencesData sharedPreferencesData;
    private List<CartMenuMainData> addMinusCartMainData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recycleView_cart.setLayoutManager(new GridLayoutManager(MyCartActivity.this, 1));
        recycleView_cart.setHasFixedSize(true);




        if (NetworkUtil.checkNetworkStatus(MyCartActivity.this)) {
            sharedPreferencesData = new SharedPreferencesData(MyCartActivity.this);
            String b_date = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
            String b_time = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
            if (!b_date.equalsIgnoreCase("")){
                date = b_date;
            }
            else {
                Date c = Calendar.getInstance().getTime();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                date = df.format(c);
            }

            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doCartList(cartCallback, userId,
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
                    date);


        } else {
            Snackbar.make(recycleView_cart, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }


    }

    Callback<CartMain> cartCallback = new Callback<CartMain>() {
        @Override
        public void onResponse(Call<CartMain> call, Response<CartMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        addMinusCartMainData = new ArrayList<>();
                        addMinusCartMainData.clear();
                        addMinusCartMainData.addAll(response.body().getCart().getMenus());
                        String restaurantId = response.body().getCart().getMenus().get(0).getRestaurantId();
                        CartRecycleViewAdapter cartRecycleViewAdapter = new CartRecycleViewAdapter(MyCartActivity.this, addMinusCartMainData, MyCartActivity.this, restaurantId);
                        recycleView_cart.setAdapter(cartRecycleViewAdapter);
                        //Toast.makeText(getContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                        String cartAmount = response.body().getCart().getCartAmount();
                        cartAmount = cartAmount.replace(".", ",");
                        subtotalAmount.setText(cartAmount + getResources().getString(R.string.euro_sign));
                        String taxAmount = response.body().getCart().getTax_amount();
                        taxAmount = taxAmount.replace(".", ",");
                        taxFeeAmount.setText(taxAmount+ getResources().getString(R.string.euro_sign));

                    } else {
                        addMinusCartMainData = new ArrayList<>();
                        addMinusCartMainData.clear();
                        String restaurantId = "";
                        CartRecycleViewAdapter cartRecycleViewAdapter = new CartRecycleViewAdapter(MyCartActivity.this, addMinusCartMainData, MyCartActivity.this, restaurantId);
                        recycleView_cart.setAdapter(cartRecycleViewAdapter);
                        Toast.makeText(MyCartActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<CartMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    public void callGetCartWhenItemChanged() {
        if (NetworkUtil.checkNetworkStatus(MyCartActivity.this)) {
            sharedPreferencesData = new SharedPreferencesData(MyCartActivity.this);
            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doCartList(cartCallback, userId,
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
                    date);


        } else {
            Snackbar.make(recycleView_cart, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_checkOut)
    public void checkOutClicked(View view){
        if (addMinusCartMainData!=null&&addMinusCartMainData.size()>0){
            boolean check = false;
            StringBuilder item = new StringBuilder();
            for (int i=0;i<addMinusCartMainData.size();i++){
                if (addMinusCartMainData.get(i).getAvailable_status() == 0) {
                    item.append(addMinusCartMainData.get(i).getMenu() + ",");
                    check = true;
                }
            }
            if (check) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyCartActivity.this);
                alertDialog.setTitle(item+ " are no longer available.");
                alertDialog.setMessage("Please remove it from your cart and proceed to checkout.");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            }else {

                if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("")) {
                    Intent intent = new Intent(MyCartActivity.this, LoginActivity.class);
                    intent.putExtra("page", "cart");
                    intent.putExtra(Constants.CART_ID, addMinusCartMainData.get(0).getCartId());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MyCartActivity.this, SelectAddressActivity.class);
                    intent.putExtra("page", "cart");
                    intent.putExtra(Constants.CART_ID, addMinusCartMainData.get(0).getCartId());
                    startActivity(intent);
                }
            }
        }else{
            Snackbar.make(btn_checkOut,getResources().getString(R.string.add_item_in_cart),Snackbar.LENGTH_LONG).show();
        }

    }
}