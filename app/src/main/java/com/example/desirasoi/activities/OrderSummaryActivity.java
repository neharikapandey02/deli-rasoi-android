package com.example.desirasoi.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.addaddress.SelectAddressActivity;
import com.example.desirasoi.activities.paymenttype.ApplyCouponMain;
import com.example.desirasoi.activities.paymenttype.ApplyCouponsAdapter;
import com.example.desirasoi.activities.paymenttype.CouponMain;
import com.example.desirasoi.activities.paymenttype.CouponMainData;
import com.example.desirasoi.activities.paymenttype.OrderPlaceMain;
import com.example.desirasoi.activities.ui.reserved_table.AvailableDates;
import com.example.desirasoi.activities.ui.reserved_table.AvailableTiming;
import com.example.desirasoi.activities.ui.reserved_table.DateObject;
import com.example.desirasoi.activities.ui.reserved_table.TimeObject;
import com.example.desirasoi.adapters.OrderDetailsCartAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.CheckoutSummary;
import com.example.desirasoi.models.CheckoutSummaryCart;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class OrderSummaryActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview_menuItem)
    RecyclerView recyclerview_menuItem;
    private MapView mapView;

    @BindView(R.id.tv_subTotal)
    TextView tv_subTotal;

    @BindView(R.id.distanceToRestTv)
    TextView distanceToRestTv;

   @BindView(R.id.distanceRl)
   RelativeLayout distanceRl;

   @BindView(R.id.additionalCommentTv)
    TextView additionalCommentTv;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.cod)
    RadioButton cod;

    @BindView(R.id.paynow)
    RadioButton paynow;
    @BindView(R.id.estimateTimeTv)
    TextView estimateTimeTv;

    @BindView(R.id.estimateTimeChangeTv)
    TextView estimateTimeChangeTv;

   public static TextView deliveryAddressTv;

    @BindView(R.id.deliveryLL)
    LinearLayout deliveryLL;

    @BindView(R.id.tv_delivertyCharges)
    TextView tv_delivertyCharges;

    @BindView(R.id.tv_totalPrice)
    TextView tv_totalPrice;

    @BindView(R.id.tv_couponDiscount)
    TextView tv_couponDiscount;
    String serviceType = "";

    @BindView(R.id.couponTv)
    TextView couponTv;
    String couponCode = "";

    Dialog dialog;
    ApplyCouponsAdapter applyCouponsAdapter;
    @BindView(R.id.tv_afterDiscount)
    TextView tv_afterDiscount;

    Spinner dateSpn;
    Spinner timeSpn;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_number)
    TextView tv_number;

    @BindView(R.id.tv_status)
    TextView tv_status;

    @BindView(R.id.orderStatusTv)
    TextView orderStatusTv;

    @BindView(R.id.orderTypeTv)
    TextView orderTypeTv;

    @BindView(R.id.imageView_back)
    ImageView imageView_back;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    String orderDate, orderTime;
    String paymentType = "";

    @BindView(R.id.placeOrderBtn)
    Button placeOrderBtn;
    List<CouponMainData> couponMains;
    int datePos = 0, timePos = 0;
    ArrayList<AvailableDates> datesArrayList;
    ArrayList<AvailableTiming> timingArrayList;
    private int position;
    private int status;
    private SharedPreferencesData sharedPreferencesData;
    String checkOutId;
    String userId;
    String orderType = "";
    public static String addressId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_order_summary);
        ButterKnife.bind(this);
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        viewFinds();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public void refresh(){
        if (NetworkUtil.checkNetworkStatus(OrderSummaryActivity.this)) {

            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getCheckoutSummary(summaryCallback, userId, getIntent().getStringExtra(Constants.CHECKOUT_ID),
                    getIntent().getStringExtra("res_lat"),  getIntent().getStringExtra("res_lng"),
                    getIntent().getStringExtra("service_type"),
                    getIntent().getStringExtra("order_type"),
                    getIntent().getStringExtra("o_date"),
                    getIntent().getStringExtra("o_time"),
                    couponCode
                    );

        } else {
            Snackbar.make(recyclerview_menuItem, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    private void viewFinds() {
        couponMains = new ArrayList<>();
        deliveryAddressTv = findViewById(R.id.deliveryAddressTv);
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
//        checkOutId = getIntent().getStringExtra(Constants.CHECKOUT_ID);
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getCheckoutSummary(summaryCallback, userId, getIntent().getStringExtra(Constants.CHECKOUT_ID),
                    getIntent().getStringExtra("res_lat"),  getIntent().getStringExtra("res_lng"),
                    getIntent().getStringExtra("service_type"),
                    getIntent().getStringExtra("order_type"),
                    getIntent().getStringExtra("o_date"),
                    getIntent().getStringExtra("o_time"),
                    couponCode
            );
        } else {
            Snackbar.make(recyclerview_menuItem, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }


        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceType.equalsIgnoreCase("1")){
                    if (addressId.equalsIgnoreCase("")){
                        Snackbar.make(placeOrderBtn, "Please select address", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    progressBar.setVisibility(View.VISIBLE);


                    if (cod.isChecked()){
                        paymentType = "2";
                    }else {
                        paymentType = "1";
                    }

                    if (orderType.equalsIgnoreCase("1")){
                        orderTime = "";
                        orderDate = "";
                    }else {
                        orderDate = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
                        orderTime = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
                    }

                    RetrofitHelper.getInstance().checkoutUpdate(checkoutUpdateCallback, userId, checkOutId,
                            additionalCommentTv.getText().toString().trim(), orderType, orderDate, orderTime);
                     } else {
                    Snackbar.make(placeOrderBtn, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        couponTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if (couponMains != null && couponMains.size() > 0) {
                        dialog = new Dialog(OrderSummaryActivity.this);
                        dialog.setContentView(R.layout.dialog_apply_coupons);
                        dialog.setCancelable(true);
                        Window window = dialog.getWindow();
                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        RecyclerView recyclerview_applyCoupons = dialog.findViewById(R.id.recyclerview_applyCoupons);
                        recyclerview_applyCoupons.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                        recyclerview_applyCoupons.setHasFixedSize(true);
                        applyCouponsAdapter = new ApplyCouponsAdapter(getApplicationContext(), couponMains, OrderSummaryActivity.this);
                        recyclerview_applyCoupons.setAdapter(applyCouponsAdapter);
                        dialog.show();
                    } else {
                        Snackbar.make(couponTv, getResources().getString(R.string.no_coupons_available), Snackbar.LENGTH_LONG).show();

                }

            }
        });



    }

    Callback<OrderPlaceMain> checkoutUpdateCallback = new Callback<OrderPlaceMain>() {
        @Override
        public void onResponse(Call<OrderPlaceMain> call, Response<OrderPlaceMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    if (paymentType.equalsIgnoreCase("2")) {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().doOrderPlace(placeOrderCallback, userId, checkOutId, paymentType, orderType,
                                orderDate, orderTime, additionalCommentTv.getText().toString().trim());
                    }else {
                        Intent intent=new Intent(getApplicationContext(), WebViewPaymentActivity.class);
                        intent.putExtra("checkoutId", checkOutId);
                        startActivity(intent);
                        finish();
                        Snackbar.make(paynow, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<OrderPlaceMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<OrderPlaceMain> placeOrderCallback = new Callback<OrderPlaceMain>() {
        @Override
        public void onResponse(Call<OrderPlaceMain> call, Response<OrderPlaceMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    if (paymentType.equalsIgnoreCase("2")) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainPageActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);
                    }
//                    else {
//                        Intent intent=new Intent(getApplicationContext(), WebViewPaymentActivity.class);
//                        intent.putExtra("checkoutId", checkOutId);
//                        startActivity(intent);
//                        finish();
//                        Snackbar.make(paynow, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
//                    }
                }
            }
        }

        @Override
        public void onFailure(Call<OrderPlaceMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };



    public void selectCouponType(int position) {
        dialog.cancel();
        int positionOfCoupons = position;
        couponTv.setText(couponMains.get(position).getCouponCode());
        couponCode = couponMains.get(position).getCouponCode();
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doApplyCoupons(doApplyCallback, userId, checkOutId, couponMains.get(position).getCouponCode());


        } else {
            Snackbar.make(recyclerview_menuItem, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }

    }

    Callback<ApplyCouponMain> doApplyCallback = new Callback<ApplyCouponMain>() {
        @Override
        public void onResponse(Call<ApplyCouponMain> call, Response<ApplyCouponMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getCheckoutSummary(summaryCallback, userId, getIntent().getStringExtra(Constants.CHECKOUT_ID),
                            getIntent().getStringExtra("res_lat"),  getIntent().getStringExtra("res_lng"),
                            getIntent().getStringExtra("service_type"),
                            getIntent().getStringExtra("order_type"),
                            getIntent().getStringExtra("o_date"),
                            getIntent().getStringExtra("o_time"),
                            couponCode
                    );
                } else {
                    Snackbar.make(placeOrderBtn, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<ApplyCouponMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<CouponMain> getCouponsCallback = new Callback<CouponMain>() {
        @Override
        public void onResponse(Call<CouponMain> call, Response<CouponMain> response) {

            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    couponMains.clear();
                    couponMains.addAll(response.body().getData());
                } else {


                }
            }
        }

        @Override
        public void onFailure(Call<CouponMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<CheckoutSummary> summaryCallback = new Callback<CheckoutSummary>() {
        @Override
        public void onResponse(Call<CheckoutSummary> call, Response<CheckoutSummary> response) {
            try {
                progressBar.setVisibility(View.GONE);
                ArrayList<CheckoutSummaryCart> arrayList;
                arrayList = response.body().getData().getCart();
                recyclerview_menuItem.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
                recyclerview_menuItem.setHasFixedSize(true);
                OrderDetailsCartAdapter historyAdapter = new OrderDetailsCartAdapter(getApplicationContext(), arrayList, userId, response.body().getData().getRestaurant_id(), progressBar,
                        OrderSummaryActivity.this);
                recyclerview_menuItem.setAdapter(historyAdapter);
                checkOutId = response.body().getData().getId();
                String tax = response.body().getData().getTax();
                tax = tax.replace(".", ",");
                tv_subTotal.setText(tax + getResources().getString(R.string.euro_sign));
                String deliveryCharges = response.body().getData().getDelivery_charge();
                deliveryCharges = deliveryCharges.replace(".", ",");
                tv_delivertyCharges.setText(deliveryCharges + getResources().getString(R.string.euro_sign));
                String grandTotal = response.body().getData().getSub_total();
                grandTotal = grandTotal.replace(".", ",");
                tv_totalPrice.setText(grandTotal + getResources().getString(R.string.euro_sign));
                double amount = Double.parseDouble(response.body().getData().getGrand_total());
                if (response.body().getData().getCoupon_discount() != null) {
                    String couponDiscount = response.body().getData().getCoupon_discount();
                    couponDiscount = couponDiscount.replace(".", ",");
                    tv_couponDiscount.setText("- " + couponDiscount + getResources().getString(R.string.euro_sign));
//        double cd = Double.parseDouble(response.body().getData().getCoupon_discount());
//        amount -= cd;
                } else {
                    tv_couponDiscount.setText("- 0" + getResources().getString(R.string.euro_sign));
                }
                String finalAmount = String.valueOf(amount);
                finalAmount = finalAmount.replace(".", ",");
                String gTot = response.body().getData().getGrand_total();
                gTot = gTot.replace(".", ",");
                tv_afterDiscount.setText(gTot + getResources().getString(R.string.euro_sign));

                serviceType = response.body().getData().getService_type();
                if (serviceType.equalsIgnoreCase("1")) {
                    tv_status.setText("Delivery");
//                    deliveryAddressTv.setTextColor(getResources().getColor(R.color.blue));
                    String address = response.body().getData().getDelivery_address().getName()+" "+
                            response.body().getData().getDelivery_address().getLastName()+"\n"
                            +"Phone: "+response.body().getData().getDelivery_address().getContact()+"\n"
                            +response.body().getData().getDelivery_address().getStreet()+"\n"
                            +response.body().getData().getDelivery_address().getPostalCode()+", "
                            +response.body().getData().getDelivery_address().getCity()+".";

                    deliveryAddressTv.setText(address);
                    addressId = response.body().getData().getDelivery_address().getId();
                    deliveryLL.setVisibility(View.VISIBLE);
                    deliveryAddressTv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(OrderSummaryActivity.this, SelectAddressActivity.class);
                            intent.putExtra(Constants.CART_ID, checkOutId);
                            startActivity(intent);
                        }
                    });
                    distanceRl.setVisibility(View.GONE);

                } else if (serviceType.equalsIgnoreCase("2")) {
                    tv_status.setText("Take away");
                    deliveryLL.setVisibility(View.GONE);
                    try {
                        String distance = response.body().getData().getRestaurant().getDistance();

                        if (distance != null) {
                            Log.e("dkfjd", distance);
                            distance = distance.replace(".", ",");
                            distanceToRestTv.setText(distance + " Km");
                            distanceRl.setVisibility(View.VISIBLE);
                        } else {
                            distanceRl.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                orderType = response.body().getData().getOrder_type();

                if (orderType.equalsIgnoreCase("1")) {
                    if (response.body().getData().getService_type().equalsIgnoreCase("1")) {

                        estimateTimeTv.setText("Delivery in " + response.body().getData().getRestaurant().getFood_ready_time_delivery() + " minutes");
                    } else {
                        estimateTimeTv.setText("Take away in " + response.body().getData().getRestaurant().getFood_ready_time_delivery() + " minutes");
                    }
                } else {
                    estimateTimeTv.setText("Schedule Order " + parseDateToddMMyyyy1(response.body().getData().getOrder_date() + " " + response.body().getData().getOrder_time()));
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE, response.body().getData().getOrder_date());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME, response.body().getData().getOrder_time());
                }
                tv_address.setText(response.body().getData().getRestaurant().getAddress());

                tv_number.setText(response.body().getData().getRestaurant().getContact());

                estimateTimeChangeTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Dialog dialog = new Dialog(OrderSummaryActivity.this);
                        dialog.setContentView(R.layout.advance_order_layout);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        LinearLayout nowLL = dialog.findViewById(R.id.nowLL);
                        LinearLayout futureLL = dialog.findViewById(R.id.futureLL);
                        ImageView nowImg = dialog.findViewById(R.id.nowImg);
                        ImageView futureImg = dialog.findViewById(R.id.futureImg);
                        int stt = 1;

                        if (response.body().getData().getRestaurant().getRestaurant_status().equalsIgnoreCase("Off")) {
                            stt = 2;
                            nowImg.setVisibility(View.INVISIBLE);
                            nowLL.setVisibility(View.GONE);
                            futureImg.setVisibility(View.VISIBLE);
                        }


                        nowLL.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                nowImg.setVisibility(View.VISIBLE);
                                futureImg.setVisibility(View.INVISIBLE);
                                orderType = "1";
                                sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE, "");
                                sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME, "");
//                        startActivity(new Intent(RestaurantMenuDetailsActivity.this, MyCartActivity.class));
                                estimateTimeTv.setText("As soon as possible");
                                dialog.dismiss();
                            }
                        });

                        futureLL.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                nowImg.setVisibility(View.INVISIBLE);
                                futureImg.setVisibility(View.VISIBLE);

                                Dialog dialog1 = new Dialog(OrderSummaryActivity.this);
                                dialog1.setContentView(R.layout.order_for_future_layout);
                                dialog1.setCancelable(false);
                                dateSpn = dialog1.findViewById(R.id.dateSpn);
                                timeSpn = dialog1.findViewById(R.id.timeSpn);
                                TextView continueBtn = dialog1.findViewById(R.id.continueBtn);
                                TextView closeBtn = dialog1.findViewById(R.id.closeBtn);


                                RetrofitHelper.getInstance().getOrderDates(datesCallback, response.body().getData().getRestaurant_id(), response.body().getData().getService_type());


                                timeSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        timePos = position;
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                dateSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        datePos = position;
                                        if (NetworkUtil.checkNetworkStatus(OrderSummaryActivity.this)) {
                                            RetrofitHelper.getInstance().getOrderTime(timesCallback, response.body().getData().getRestaurant_id(), datesArrayList.get(position).getBook_date(),
                                                    response.body().getData().getService_type());
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                closeBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog1.dismiss();
                                    }
                                });

                                continueBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (!dateSpn.getSelectedItem().toString().equalsIgnoreCase("") && !timeSpn.getSelectedItem().toString().equalsIgnoreCase("")) {
//                                    startActivity(new Intent(RestaurantMenuDetailsActivity.this, MyCartActivity.class));
                                            orderType = "2";
                                            estimateTimeTv.setText("Schedule Order " + datesArrayList.get(datePos).getFormat_date() + " " + timingArrayList.get(timePos).getFormatted_time());
                                            sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE, datesArrayList.get(datePos).getBook_date());
                                            sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME, timingArrayList.get(timePos).getBooking_time() + ":00");
                                            dialog1.dismiss();
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(OrderSummaryActivity.this, "Please select both date and time.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                dialog1.show();
                            }
                        });
                        dialog.show();
                    }
                });

                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(@NonNull MapboxMap mapboxMap) {
                        try {
                            mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(response.body().getData().getRestaurant().getLatitude()), Double.parseDouble(response.body().getData().getRestaurant().getLongitude())), 13.0));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        List<Feature> symbolLayerIconFeatureList = new ArrayList<>();
                        symbolLayerIconFeatureList.add(Feature.fromGeometry(
                                Point.fromLngLat(Double.parseDouble(response.body().getData().getRestaurant().getLongitude()), Double.parseDouble(response.body().getData().getRestaurant().getLatitude()))));
//            symbolLayerIconFeatureList.add(Feature.fromGeometry(
//                    Point.fromLngLat(-54.14164, -33.981818)));
//            symbolLayerIconFeatureList.add(Feature.fromGeometry(
//                    Point.fromLngLat(-56.990533, -30.583266)));

                        mapboxMap.setStyle(new Style.Builder().fromUri("mapbox://styles/mapbox/cjf4m44iw0uza2spb3q0a7s41")

// Add the SymbolLayer icon image to the map style
                                .withImage("ICON_ID", BitmapFactory.decodeResource(
                                        OrderSummaryActivity.this.getResources(), R.drawable.mapbox_marker_icon_default))

// Adding a GeoJson source for the SymbolLayer icons.
                                .withSource(new GeoJsonSource("SOURCE_ID",
                                        FeatureCollection.fromFeatures(symbolLayerIconFeatureList)))

// Adding the actual SymbolLayer to the map style. An offset is added that the bottom of the red
// marker icon gets fixed to the coordinate, rather than the middle of the icon being fixed to
// the coordinate point. This is offset is not always needed and is dependent on the image
// that you use for the SymbolLayer icon.
                                .withLayer(new SymbolLayer("LAYER_ID", "SOURCE_ID")
                                        .withProperties(
                                                iconImage("ICON_ID"),
                                                iconAllowOverlap(true),
                                                iconIgnorePlacement(true)
                                        )
                                ), new Style.OnStyleLoaded() {
                            @Override
                            public void onStyleLoaded(@NonNull Style style) {

// Map is set up and the style has loaded. Now you can add additional data or make other map adjustments.


                            }
                        });

//            try {
//                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(response.body().getData().getRestaurant().getLatitude()), Double.parseDouble(response.body().getData().getRestaurant().getLongitude())), 13.0));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
//                @Override
//                public void onStyleLoaded(@NonNull Style style) {
//
//                    // Map is set up and the style has loaded. Now you can add data or make other map adjustments
//                }
//            });
                    }
                });

                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

                    RetrofitHelper.getInstance().doGetCoupons(getCouponsCallback, userId);

                } else {
                    Snackbar.make(placeOrderBtn, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<CheckoutSummary> call, Throwable t) {

        }
    };

    Callback<TimeObject> timesCallback = new Callback<TimeObject>() {
        @Override
        public void onResponse(Call<TimeObject> call, Response<TimeObject> response) {
            try {
                if (response.body().isSuccess()) {
                    timePos = 0;
                    timingArrayList = new ArrayList<>();
                    timingArrayList = response.body().getArrayList();
                    SelectTimeAdapter selectTableAdapter = new SelectTimeAdapter(OrderSummaryActivity.this, R.layout.item_spinner_text_home, timingArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    timeSpn.setAdapter(selectTableAdapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<TimeObject> call, Throwable t) {

        }
    };


    Callback<DateObject> datesCallback = new Callback<DateObject>() {
        @Override
        public void onResponse(Call<DateObject> call, Response<DateObject> response) {
            try {
                progressBar.setVisibility(View.GONE);
                if (response.body().isSuccess()) {
                    datePos = 0;
                    datesArrayList = new ArrayList<>();
                    datesArrayList = response.body().getArrayList();
                    SelectDatesAdapter selectTableAdapter = new SelectDatesAdapter(OrderSummaryActivity.this, R.layout.item_spinner_text_home, datesArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    //Setting the ArrayAdapter data on the Spinner
                    dateSpn.setAdapter(selectTableAdapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DateObject> call, Throwable t) {

        }
    };

    class SelectDatesAdapter extends ArrayAdapter<String> {
        public SelectDatesAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(datesArrayList.get(position).getFormat_date());
            return mySpinner;
        }
    }

    class SelectTimeAdapter extends ArrayAdapter<String> {
        public SelectTimeAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(timingArrayList.get(position).getFormatted_time());
            return mySpinner;
        }
    }




    public String parseDateToddMMyyyy1(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "d.MM.yyyy H.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }
    @OnClick(R.id.imageView_back)
    public void backClicked(View view) {
        onBackPressed();
    }

}