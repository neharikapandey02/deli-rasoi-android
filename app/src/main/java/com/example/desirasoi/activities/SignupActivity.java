package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.addaddress.SelectAddressActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.MainPageCalling;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.RegistrationMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
    @BindView(R.id.et_firstName)
    EditText et_firstName;

    @BindView(R.id.et_lastName)
    EditText et_lastName;

    @BindView(R.id.et_number)
    EditText et_number;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.streetAddressEt)
    EditText streetAddressEt;

   @BindView(R.id.cityEt)
    EditText cityEt;

   @BindView(R.id.postCodeEt)
    EditText postCodeEt;

    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.et_confirmPassword)
    EditText et_confirmPassword;


    @BindView(R.id.btn_login)
    Button btn_login;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
    }

    @OnClick(R.id.btn_login)
    public void registreClicked(View view) {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()) {
                progressBar.setVisibility(View.VISIBLE);
                String firstname = et_firstName.getText().toString().trim();
                String lastname = et_lastName.getText().toString().trim();
                String number = et_number.getText().toString().trim();
                String email = et_email.getText().toString().trim();
                String password = et_password.getText().toString().trim();

                MapboxGeocoding mapboxGeocoding = MapboxGeocoding.builder()
                        .accessToken(getString(R.string.mapbox_access_token))
                        .query(streetAddressEt.getText().toString().trim()+" "
                                + cityEt.getText().toString().trim() +" "+
                                postCodeEt.getText().toString().trim())
                        .build();

                mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                    @Override
                    public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {

                        List<CarmenFeature> results = response.body().features();

                        if (results.size() > 0) {

                            Point firstResultPoint = results.get(0).center();

                            String lng = String.valueOf(firstResultPoint.coordinates().get(0));
                            String lat = String.valueOf(firstResultPoint.coordinates().get(1));
                            RetrofitHelper.getInstance().doSignUp(registerCallback, firstname,lastname, number, email, password,
                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.DEVICE_TOKEN),
                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID),
                                    streetAddressEt.getText().toString().trim(), cityEt.getText().toString().trim(),
                                    postCodeEt.getText().toString().trim(), lat, lng);
                        } else {

                            // No result for your request were found.
                            Log.e("onResponse", "onResponse: No result found");

                        }
                    }

                    @Override
                    public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
            }
        } else {
            Snackbar.make(getCurrentFocus(), getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }


    }

    Callback<RegistrationMain> registerCallback = new Callback<RegistrationMain>() {
        @Override
        public void onResponse(Call<RegistrationMain> call, Response<RegistrationMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess() == true) {
                    if (response.body().getResult() != null) {
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID, response.body().getResult().getId());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME,response.body().getResult().getLast_name());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME,response.body().getResult().getFirst_name());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.MOBILE_NUMBER, response.body().getResult().getContact());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.EMAIL, response.body().getResult().getEmail());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOUSE, response.body().getResult().getHouse());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.CITY, response.body().getResult().getCity());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.STATE, response.body().getResult().getState());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.POSTAL_CODE, response.body().getResult().getPostalCode());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.COUNTRY, response.body().getResult().getCountry());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.AUTH_TOKEN, response.body().getResult().getAuth_token());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.NEWSLETTER_STATUS, response.body().getResult().getNewsletter_status());

                        try {
                            if (getIntent().getStringExtra("page").equalsIgnoreCase("cart")){
                                Intent intent=new Intent(getApplicationContext(), OrderSummaryActivity.class);
                                intent.putExtra(Constants.CHECKOUT_ID, getIntent().getStringExtra(Constants.CART_ID));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Intent intent=new Intent(getApplicationContext(), MainPageActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Intent intent=new Intent(getApplicationContext(), MainPageActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                } else {
                    Snackbar.make(et_firstName, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<RegistrationMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    private boolean validation() {
        if (et_firstName.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.firstname_), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_lastName.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.lastname), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_number.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.enter_number), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_email.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.enter_email), Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (streetAddressEt.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.please_enter_street_add), Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (cityEt.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.please_enter_city), Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (postCodeEt.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.please_enter_postcode), Snackbar.LENGTH_LONG).show();
            return false;
        }


        else if (et_password.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.enter_password), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_confirmPassword.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(et_firstName, getResources().getString(R.string.enter_confirm_password), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!et_confirmPassword.getText().toString().equalsIgnoreCase(et_password.getText().toString())) {
            Snackbar.make(et_firstName, getResources().getString(R.string.both_password_not_matched), Snackbar.LENGTH_LONG).show();
            return false;
        }


        return true;
    }
}
