package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Window;

import com.example.desirasoi.R;
import com.example.desirasoi.adapters.ExtraItemAdapter;
import com.example.desirasoi.adapters.RestaurantListSpacialityAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleItemDetails extends AppCompatActivity {
    @BindView(R.id.recyclerview_extra_item)
    RecyclerView recyclerview_extra_item;

    ExtraItemAdapter extraItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_single_item_details);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        recyclerview_extra_item.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recyclerview_extra_item.setHasFixedSize(true);
        //recyclerview_specialitylist.setNestedScrollingEnabled(false);
        extraItemAdapter =new ExtraItemAdapter(getApplicationContext());
        recyclerview_extra_item.setAdapter(extraItemAdapter);
    }
}
