package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.desirasoi.R;

import com.example.desirasoi.adapters.RestaurantListSpacialityAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;


import com.example.desirasoi.models.RestaurantDetailsMain;
import com.example.desirasoi.models.RestaurantDetailsMainData;
import com.example.desirasoi.models.RestaurantListMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpecialityRestaurantList extends AppCompatActivity {
    @BindView(R.id.recyclerview_specialitylist)
    RecyclerView recyclerview_specialitylist;


    @BindView(R.id.imgview_back)
    ImageView imgview_back;

    @BindView(R.id.imgview_restaurant)
    ImageView imgview_restaurant;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private String id_restaurant;

    private RestaurantListSpacialityAdapter restaurantListAdapter;
    private List<RestaurantDetailsMainData> restaurantDetailsMains;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_speciality_restaurant_list);
        ButterKnife.bind(this);
        viewFind();

    }

    private void viewFind() {
        id_restaurant=getIntent().getStringExtra(Constants.ID_RESTAURANT);
        recyclerview_specialitylist.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        recyclerview_specialitylist.setHasFixedSize(true);
        //recyclerview_specialitylist.setNestedScrollingEnabled(false);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doResturantDetailsList(restaurantDetailsCallback,id_restaurant);

        } else {
            Snackbar.make(recyclerview_specialitylist, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }






    }



    Callback<RestaurantDetailsMain> restaurantDetailsCallback = new Callback<RestaurantDetailsMain>() {
        @Override
        public void onResponse(Call<RestaurantDetailsMain> call, Response<RestaurantDetailsMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        if (response.body().getRestaurantDetail()!=null){
                            Picasso.with(getApplicationContext())
                                    .load(response.body().getRestaurantDetail().getImage())
                                    .error(R.drawable.restaurant_list)
                                    .fit()
                                    .placeholder(R.drawable.restaurant_list)
                                    .into(imgview_restaurant);
                            restaurantDetailsMains=new ArrayList<>();
                            restaurantDetailsMains.clear();
                            restaurantDetailsMains.add(response.body().getRestaurantDetail());
                            restaurantListAdapter =new RestaurantListSpacialityAdapter(getApplicationContext(),restaurantDetailsMains);
                            recyclerview_specialitylist.setAdapter(restaurantListAdapter);
                        }else{
                            Snackbar.make(recyclerview_specialitylist, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                        }

                    } else {
                        Snackbar.make(recyclerview_specialitylist, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<RestaurantDetailsMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    @OnClick(R.id.imgview_back)
    public void backClicked(View view){
        onBackPressed();
    }
}
