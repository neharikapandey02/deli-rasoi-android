package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;

import com.example.desirasoi.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class TestActivity extends AppCompatActivity {
    PieChart pieChart;
    PieData pieData;
    PieDataSet pieDataSet;
    ArrayList pieEntries;
    ArrayList<Integer> PieEntryLabels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        pieChart = findViewById(R.id.pieChart);
        PieEntryLabels=new ArrayList();
        PieEntryLabels.add(getResources().getColor(R.color.red));
        PieEntryLabels.add(getResources().getColor(R.color.red));
        PieEntryLabels.add(getResources().getColor(R.color.red));
        PieEntryLabels.add(getResources().getColor(R.color.red));
        PieEntryLabels.add(getResources().getColor(R.color.red));
        PieEntryLabels.add(getResources().getColor(R.color.blue));
        PieEntryLabels.add(getResources().getColor(R.color.red));
        PieEntryLabels.add(getResources().getColor(R.color.red));


        getEntries();
        pieDataSet = new PieDataSet(pieEntries, "");
        pieChart.setHoleRadius(25f);
        pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieDataSet.setColors(PieEntryLabels);
        pieDataSet.setDrawValues(false);
        pieDataSet.setValueTextColor(getResources().getColor(R.color.red));
        pieDataSet.setSliceSpace(5f);
        pieChart.setRotationEnabled(false);




    }
    private void getEntries() {
        pieEntries = new ArrayList<>();
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
        pieEntries.add(new PieEntry(2f, "This is test",0));
    }
}
