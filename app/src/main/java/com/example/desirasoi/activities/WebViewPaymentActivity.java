package com.example.desirasoi.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.desirasoi.R;

public class WebViewPaymentActivity extends AppCompatActivity {
    @BindView(R.id.webview)
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_web_view_payment);
        ButterKnife.bind(this);
        webview.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.e("WebView", "your current url when webpage loading.." + url);
            }

            public void onPageFinished(WebView view, String url) {
                if (url.equalsIgnoreCase("https://licpremium.com/dev/delirasoi/payment/successm")){
                    Intent intent=new Intent(getApplicationContext(),MainPageActivity.class);
                    startActivity(intent);
                    finish();
                }else if (url.equalsIgnoreCase("https://licpremium.com/dev/delirasoi/payment/failedm")){
                    Toast.makeText(getApplicationContext(),"Payment Failed, Try Again",Toast.LENGTH_LONG).show();
                    finish();

                }
                Log.e("WebView", "your current url when webpage loading.. finish" + url);
                super.onPageFinished(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onLoadResource(view, url);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("when you click on any interlink on webview that time you got url :-" + url);
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.loadUrl("https://licpremium.com/dev/delirasoi/payment/initiate/"+ getIntent().getStringExtra("checkoutId")+"?mode=app");
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
