package com.example.desirasoi.activities.aboutus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutMainData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("main_heading")
    @Expose
    private String mainHeading;
    @SerializedName("main_heading_fi")
    @Expose
    private String mainHeadingFi;
    @SerializedName("main_content")
    @Expose
    private String mainContent;
    @SerializedName("main_content_fi")
    @Expose
    private String mainContentFi;
    @SerializedName("first_image")
    @Expose
    private String firstImage;
    @SerializedName("second_image")
    @Expose
    private String secondImage;
    @SerializedName("first_sub_image")
    @Expose
    private String firstSubImage;
    @SerializedName("first_sub_heading")
    @Expose
    private String firstSubHeading;
    @SerializedName("first_sub_heading_fi")
    @Expose
    private String firstSubHeadingFi;
    @SerializedName("first_sub_content")
    @Expose
    private String firstSubContent;
    @SerializedName("first_sub_content_fi")
    @Expose
    private String firstSubContentFi;
    @SerializedName("second_sub_image")
    @Expose
    private String secondSubImage;
    @SerializedName("second_sub_heading")
    @Expose
    private String secondSubHeading;
    @SerializedName("second_sub_heading_fi")
    @Expose
    private String secondSubHeadingFi;
    @SerializedName("second_sub_content")
    @Expose
    private String secondSubContent;
    @SerializedName("second_sub_content_fi")
    @Expose
    private String secondSubContentFi;
    @SerializedName("third_sub_image")
    @Expose
    private String thirdSubImage;
    @SerializedName("third_sub_heading")
    @Expose
    private String thirdSubHeading;
    @SerializedName("third_sub_heading_fi")
    @Expose
    private String thirdSubHeadingFi;
    @SerializedName("third_sub_content")
    @Expose
    private String thirdSubContent;
    @SerializedName("third_sub_content_fi")
    @Expose
    private String thirdSubContentFi;
    @SerializedName("second_heading")
    @Expose
    private String secondHeading;
    @SerializedName("second_heading_fi")
    @Expose
    private String secondHeadingFi;
    @SerializedName("third_heading")
    @Expose
    private String thirdHeading;
    @SerializedName("third_heading_fi")
    @Expose
    private String thirdHeadingFi;
    @SerializedName("blog_heading")
    @Expose
    private String blogHeading;
    @SerializedName("blog_heading_fi")
    @Expose
    private String blogHeadingFi;
    @SerializedName("blog_content")
    @Expose
    private String blogContent;
    @SerializedName("blog_content_fi")
    @Expose
    private String blogContentFi;
    @SerializedName("fourth_heading")
    @Expose
    private String fourthHeading;
    @SerializedName("fourth_heading_fi")
    @Expose
    private String fourthHeadingFi;
    @SerializedName("fourth_sub_heading")
    @Expose
    private String fourthSubHeading;
    @SerializedName("fourth_sub_heading_fi")
    @Expose
    private String fourthSubHeadingFi;
    @SerializedName("fourth_content")
    @Expose
    private String fourthContent;
    @SerializedName("fourth_content_fi")
    @Expose
    private String fourthContentFi;
    @SerializedName("fifth_heading")
    @Expose
    private String fifthHeading;
    @SerializedName("fifth_heading_fi")
    @Expose
    private String fifthHeadingFi;
    @SerializedName("fifth_sub_heading")
    @Expose
    private String fifthSubHeading;
    @SerializedName("fifth_sub_heading_fi")
    @Expose
    private String fifthSubHeadingFi;
    @SerializedName("fifth_content")
    @Expose
    private String fifthContent;
    @SerializedName("fifth_content_fi")
    @Expose
    private String fifthContentFi;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainHeading() {
        return mainHeading;
    }

    public void setMainHeading(String mainHeading) {
        this.mainHeading = mainHeading;
    }

    public String getMainHeadingFi() {
        return mainHeadingFi;
    }

    public void setMainHeadingFi(String mainHeadingFi) {
        this.mainHeadingFi = mainHeadingFi;
    }

    public String getMainContent() {
        return mainContent;
    }

    public void setMainContent(String mainContent) {
        this.mainContent = mainContent;
    }

    public String getMainContentFi() {
        return mainContentFi;
    }

    public void setMainContentFi(String mainContentFi) {
        this.mainContentFi = mainContentFi;
    }

    public String getFirstImage() {
        return firstImage;
    }

    public void setFirstImage(String firstImage) {
        this.firstImage = firstImage;
    }

    public String getSecondImage() {
        return secondImage;
    }

    public void setSecondImage(String secondImage) {
        this.secondImage = secondImage;
    }

    public String getFirstSubImage() {
        return firstSubImage;
    }

    public void setFirstSubImage(String firstSubImage) {
        this.firstSubImage = firstSubImage;
    }

    public String getFirstSubHeading() {
        return firstSubHeading;
    }

    public void setFirstSubHeading(String firstSubHeading) {
        this.firstSubHeading = firstSubHeading;
    }

    public String getFirstSubHeadingFi() {
        return firstSubHeadingFi;
    }

    public void setFirstSubHeadingFi(String firstSubHeadingFi) {
        this.firstSubHeadingFi = firstSubHeadingFi;
    }

    public String getFirstSubContent() {
        return firstSubContent;
    }

    public void setFirstSubContent(String firstSubContent) {
        this.firstSubContent = firstSubContent;
    }

    public String getFirstSubContentFi() {
        return firstSubContentFi;
    }

    public void setFirstSubContentFi(String firstSubContentFi) {
        this.firstSubContentFi = firstSubContentFi;
    }

    public String getSecondSubImage() {
        return secondSubImage;
    }

    public void setSecondSubImage(String secondSubImage) {
        this.secondSubImage = secondSubImage;
    }

    public String getSecondSubHeading() {
        return secondSubHeading;
    }

    public void setSecondSubHeading(String secondSubHeading) {
        this.secondSubHeading = secondSubHeading;
    }

    public String getSecondSubHeadingFi() {
        return secondSubHeadingFi;
    }

    public void setSecondSubHeadingFi(String secondSubHeadingFi) {
        this.secondSubHeadingFi = secondSubHeadingFi;
    }

    public String getSecondSubContent() {
        return secondSubContent;
    }

    public void setSecondSubContent(String secondSubContent) {
        this.secondSubContent = secondSubContent;
    }

    public String getSecondSubContentFi() {
        return secondSubContentFi;
    }

    public void setSecondSubContentFi(String secondSubContentFi) {
        this.secondSubContentFi = secondSubContentFi;
    }

    public String getThirdSubImage() {
        return thirdSubImage;
    }

    public void setThirdSubImage(String thirdSubImage) {
        this.thirdSubImage = thirdSubImage;
    }

    public String getThirdSubHeading() {
        return thirdSubHeading;
    }

    public void setThirdSubHeading(String thirdSubHeading) {
        this.thirdSubHeading = thirdSubHeading;
    }

    public String getThirdSubHeadingFi() {
        return thirdSubHeadingFi;
    }

    public void setThirdSubHeadingFi(String thirdSubHeadingFi) {
        this.thirdSubHeadingFi = thirdSubHeadingFi;
    }

    public String getThirdSubContent() {
        return thirdSubContent;
    }

    public void setThirdSubContent(String thirdSubContent) {
        this.thirdSubContent = thirdSubContent;
    }

    public String getThirdSubContentFi() {
        return thirdSubContentFi;
    }

    public void setThirdSubContentFi(String thirdSubContentFi) {
        this.thirdSubContentFi = thirdSubContentFi;
    }

    public String getSecondHeading() {
        return secondHeading;
    }

    public void setSecondHeading(String secondHeading) {
        this.secondHeading = secondHeading;
    }

    public String getSecondHeadingFi() {
        return secondHeadingFi;
    }

    public void setSecondHeadingFi(String secondHeadingFi) {
        this.secondHeadingFi = secondHeadingFi;
    }

    public String getThirdHeading() {
        return thirdHeading;
    }

    public void setThirdHeading(String thirdHeading) {
        this.thirdHeading = thirdHeading;
    }

    public String getThirdHeadingFi() {
        return thirdHeadingFi;
    }

    public void setThirdHeadingFi(String thirdHeadingFi) {
        this.thirdHeadingFi = thirdHeadingFi;
    }

    public String getBlogHeading() {
        return blogHeading;
    }

    public void setBlogHeading(String blogHeading) {
        this.blogHeading = blogHeading;
    }

    public String getBlogHeadingFi() {
        return blogHeadingFi;
    }

    public void setBlogHeadingFi(String blogHeadingFi) {
        this.blogHeadingFi = blogHeadingFi;
    }

    public String getBlogContent() {
        return blogContent;
    }

    public void setBlogContent(String blogContent) {
        this.blogContent = blogContent;
    }

    public String getBlogContentFi() {
        return blogContentFi;
    }

    public void setBlogContentFi(String blogContentFi) {
        this.blogContentFi = blogContentFi;
    }

    public String getFourthHeading() {
        return fourthHeading;
    }

    public void setFourthHeading(String fourthHeading) {
        this.fourthHeading = fourthHeading;
    }

    public String getFourthHeadingFi() {
        return fourthHeadingFi;
    }

    public void setFourthHeadingFi(String fourthHeadingFi) {
        this.fourthHeadingFi = fourthHeadingFi;
    }

    public String getFourthSubHeading() {
        return fourthSubHeading;
    }

    public void setFourthSubHeading(String fourthSubHeading) {
        this.fourthSubHeading = fourthSubHeading;
    }

    public String getFourthSubHeadingFi() {
        return fourthSubHeadingFi;
    }

    public void setFourthSubHeadingFi(String fourthSubHeadingFi) {
        this.fourthSubHeadingFi = fourthSubHeadingFi;
    }

    public String getFourthContent() {
        return fourthContent;
    }

    public void setFourthContent(String fourthContent) {
        this.fourthContent = fourthContent;
    }

    public String getFourthContentFi() {
        return fourthContentFi;
    }

    public void setFourthContentFi(String fourthContentFi) {
        this.fourthContentFi = fourthContentFi;
    }

    public String getFifthHeading() {
        return fifthHeading;
    }

    public void setFifthHeading(String fifthHeading) {
        this.fifthHeading = fifthHeading;
    }

    public String getFifthHeadingFi() {
        return fifthHeadingFi;
    }

    public void setFifthHeadingFi(String fifthHeadingFi) {
        this.fifthHeadingFi = fifthHeadingFi;
    }

    public String getFifthSubHeading() {
        return fifthSubHeading;
    }

    public void setFifthSubHeading(String fifthSubHeading) {
        this.fifthSubHeading = fifthSubHeading;
    }

    public String getFifthSubHeadingFi() {
        return fifthSubHeadingFi;
    }

    public void setFifthSubHeadingFi(String fifthSubHeadingFi) {
        this.fifthSubHeadingFi = fifthSubHeadingFi;
    }

    public String getFifthContent() {
        return fifthContent;
    }

    public void setFifthContent(String fifthContent) {
        this.fifthContent = fifthContent;
    }

    public String getFifthContentFi() {
        return fifthContentFi;
    }

    public void setFifthContentFi(String fifthContentFi) {
        this.fifthContentFi = fifthContentFi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
