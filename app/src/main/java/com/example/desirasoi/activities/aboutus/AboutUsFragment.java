package com.example.desirasoi.activities.aboutus;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.contact_us.ContactUsActivity;
import com.example.desirasoi.activities.eventandmnagmt.EventListAdapter;
import com.example.desirasoi.activities.eventandmnagmt.NewsMain;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AboutUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutUsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.btn_aboutUs)
    Button btn_aboutUs;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.tv_mainContent)
    TextView tv_mainContent;


    @BindView(R.id.circleImageView_firstImage)
    CircleImageView circleImageView_firstImage;

    @BindView(R.id.tv_firstSubHeading)
    TextView tv_firstSubHeading;

    @BindView(R.id.tv_firstSubContent)
    TextView tv_firstSubContent;

    @BindView(R.id.tv_thirdHeading)
    TextView tv_thirdHeading;

    @BindView(R.id.tv_blogHeading)
    TextView tv_blogHeading;

    @BindView(R.id.tv_fourthHeading)
    TextView tv_fourthHeading;

    @BindView(R.id.tv_fourthSubHeading)
    TextView tv_fourthSubHeading;

    @BindView(R.id.tv_fouthContent)
    TextView tv_fouthContent;


    @BindView(R.id.imageView_thirdSubImage)
    ImageView imageView_thirdSubImage;
    private SharedPreferencesData sharedPreferencesData;
    public AboutUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AboutUsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AboutUsFragment newInstance(String param1, String param2) {
        AboutUsFragment fragment = new AboutUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferencesData = new SharedPreferencesData(getContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        MainPageActivity.swipeRefresh.setEnabled(false);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        if (NetworkUtil.checkNetworkStatus(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doGetAboutUs(newAboutMina);

        } else {
            Snackbar.make(btn_aboutUs, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<AboutMain> newAboutMina = new Callback<AboutMain>() {
        @Override
        public void onResponse(Call<AboutMain> call, Response<AboutMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        tv_mainContent.setText(response.body().getData().getMainContent().replaceAll("\\<.*?\\>", ""));
                        Picasso.with(getContext())
                                .load(response.body().getData().getFirstImage())
                                .into(circleImageView_firstImage);
                        Picasso.with(getContext())
                                .load(response.body().getData().getThirdSubImage())
                                .into(imageView_thirdSubImage);
                        tv_firstSubHeading.setText(response.body().getData().getFirstSubHeading().replaceAll("\\<.*?\\>", ""));
                        tv_firstSubContent.setText(response.body().getData().getFirstSubContent().replaceAll("\\<.*?\\>", ""));
                        tv_thirdHeading.setText(response.body().getData().getThirdHeading().replaceAll("\\<.*?\\>", ""));
                        tv_blogHeading.setText(response.body().getData().getBlogHeading().replaceAll("\\<.*?\\>", ""));
                        tv_fourthHeading.setText(response.body().getData().getFourthHeading().replaceAll("\\<.*?\\>", ""));
                        tv_fourthSubHeading.setText(response.body().getData().getFourthSubHeading().replaceAll("\\<.*?\\>", ""));
                        tv_fouthContent.setText(response.body().getData().getFourthContent().replaceAll("\\<.*?\\>", ""));


                    } else {
                        Snackbar.make(btn_aboutUs, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<AboutMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };


    @OnClick(R.id.btn_aboutUs)
    public void contactUsClicked(View view) {
        Intent intent = new Intent(getContext(), ContactUsActivity.class);
        startActivity(intent);
    }
}
