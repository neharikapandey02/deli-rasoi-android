package com.example.desirasoi.activities.addaddress;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddAddressMain;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;

import java.util.List;

public class AddNewAddressActivity extends AppCompatActivity {

    @BindView(R.id.et_name)
    EditText et_name;
    
   @BindView(R.id.lastNameEt)
    EditText lastNameEt;

    @BindView(R.id.et_contact)
    EditText et_contact;
    
    @BindView(R.id.et_apartment)
    EditText et_apartment;
    
    @BindView(R.id.et_street)
    EditText et_street;
    
    @BindView(R.id.et_city)
    EditText et_city;
    
    @BindView(R.id.et_zipCode)
    EditText et_zipCode; 
    
    @BindView(R.id.btn_addAddress)
    Button btn_addAddress;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SharedPreferencesData sharedPreferencesData;
    private String userId;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_add_new_address);
        ButterKnife.bind(this);
        viewFinds();

    }

    private void viewFinds() {
    sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
    userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
    et_name.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME));
    lastNameEt.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME));
    et_contact.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.MOBILE_NUMBER));
    }

    @OnClick(R.id.imgview_back)
    public void backClicked(View view) {
        onBackPressed();
    }

    @OnClick(R.id.btn_addAddress)
    public void addAddressClicked(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                String name = et_name.getText().toString().trim();
                String lastName = lastNameEt.getText().toString().trim();
                String contact = et_contact.getText().toString().trim();
                String apartment = et_apartment.getText().toString().trim();
                String street = et_street.getText().toString().trim();
                String city = et_city.getText().toString().trim();
                String postalcode = et_zipCode.getText().toString().trim();

                MapboxGeocoding mapboxGeocoding = MapboxGeocoding.builder()
                        .accessToken(getString(R.string.mapbox_access_token))
                        .query(street +" "+city +" "+postalcode)
                        .build();

                mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                    @Override
                    public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {

                        List<CarmenFeature> results = response.body().features();

                        if (results.size() > 0) {

                            Point firstResultPoint = results.get(0).center();

                            String lng = String.valueOf(firstResultPoint.coordinates().get(0));
                            String lat = String.valueOf(firstResultPoint.coordinates().get(1));
                RetrofitHelper.getInstance().doAddAddress(addAddressCallback,userId, name, contact,apartment,street,city,postalcode,lat,lng, lastName);

                        } else {

                            // No result for your request were found.
                            Log.e("onResponse", "onResponse: No result found");

                        }
                    }

                    @Override
                    public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });

            }
        } else {
            Snackbar.make(btn_addAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<AddAddressMain> addAddressCallback = new Callback<AddAddressMain>() {
        @Override
        public void onResponse(Call<AddAddressMain> call, Response<AddAddressMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()){
                    finish();
                    Snackbar.make(btn_addAddress,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }else{
                    Snackbar.make(btn_addAddress,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<AddAddressMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
    private boolean validation(){
        if (et_name.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_name),Snackbar.LENGTH_LONG).show();
            return false;
        }else
        if (et_contact.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_number),Snackbar.LENGTH_LONG).show();
            return false;
        }
//        else
//        if (et_apartment.getText().toString().equalsIgnoreCase("")){
//            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_apartment),Snackbar.LENGTH_LONG).show();
//            return false;
//        }
        else
        if (et_street.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_street),Snackbar.LENGTH_LONG).show();
            return false;
        }else
        if (et_city.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_city),Snackbar.LENGTH_LONG).show();
            return false;
        }else
        if (et_zipCode.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_postalcode),Snackbar.LENGTH_LONG).show();
            return false;
        }



        return true;
    }
}
