package com.example.desirasoi.activities.addaddress;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddAddressMain;
import com.example.desirasoi.models.EditAddressMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

public class EditAddress extends AppCompatActivity {
    @BindView(R.id.et_name)
    EditText et_name;

   @BindView(R.id.lastNameEt)
    EditText lastNameEt;

    @BindView(R.id.et_contact)
    EditText et_contact;

    @BindView(R.id.et_apartment)
    EditText et_apartment;

    @BindView(R.id.et_street)
    EditText et_street;

    @BindView(R.id.et_city)
    EditText et_city;

    @BindView(R.id.et_zipCode)
    EditText et_zipCode;

    @BindView(R.id.btn_addAddress)
    Button btn_addAddress;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.btn_deleteAddress)
    Button btn_deleteAddress;

    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private String addressId;
    ArrayList editAddresslist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_edit_address);
        ButterKnife.bind(this);
        viewFinds();
    }


    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
        editAddresslist=getIntent().getStringArrayListExtra(Constants.EDIT_ADDRESS);
        if (editAddresslist!=null){
            addressId=editAddresslist.get(0).toString();

            et_contact.setText(editAddresslist.get(2).toString());
//            et_apartment.setText(editAddresslist.get(3).toString());
            et_street.setText(editAddresslist.get(4).toString());
            et_city.setText(editAddresslist.get(5).toString());
            et_zipCode.setText(editAddresslist.get(6).toString());
            try {
                et_name.setText(editAddresslist.get(1).toString());
                lastNameEt.setText(editAddresslist.get(7).toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @OnClick(R.id.imgview_back)
    public void backClicked(View view) {
        onBackPressed();
    }

    @OnClick(R.id.btn_addAddress)
    public void addAddressClicked(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                String name = et_name.getText().toString().trim();
                String lastName = lastNameEt.getText().toString().trim();
                String contact = et_contact.getText().toString().trim();
                String apartment = "";
                String street = et_street.getText().toString().trim();
                String city = et_city.getText().toString().trim();
                String postalcode = et_zipCode.getText().toString().trim();

                MapboxGeocoding mapboxGeocoding = MapboxGeocoding.builder()
                        .accessToken(getString(R.string.mapbox_access_token))
                        .query(street +" "+city +" "+postalcode)
                        .build();

                mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                    @Override
                    public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {

                        List<CarmenFeature> results = response.body().features();

                        if (results.size() > 0) {

                            Point firstResultPoint = results.get(0).center();

                            String lng = String.valueOf(firstResultPoint.coordinates().get(0));
                            String lat = String.valueOf(firstResultPoint.coordinates().get(1));
                            RetrofitHelper.getInstance().doEditAddress(addAddressCallback,userId,addressId, name, contact,apartment,street,city,postalcode,lat,lng, lastName);

                        } else {

                            // No result for your request were found.
                            Log.e("onResponse", "onResponse: No result found");

                        }
                    }

                    @Override
                    public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
            }
        } else {
            Snackbar.make(btn_addAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<EditAddressMain> addAddressCallback = new Callback<EditAddressMain>() {
        @Override
        public void onResponse(Call<EditAddressMain> call, Response<EditAddressMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()){
                    onBackPressed();
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                }else{
                    Snackbar.make(btn_addAddress,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<EditAddressMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
    private boolean validation(){
        if (et_name.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_name),Snackbar.LENGTH_LONG).show();
            return false;
        }else
        if (et_contact.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_number),Snackbar.LENGTH_LONG).show();
            return false;
        }
//        else
//        if (et_apartment.getText().toString().equalsIgnoreCase("")){
//            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_apartment),Snackbar.LENGTH_LONG).show();
//            return false;
//        }
        else if (et_street.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_street),Snackbar.LENGTH_LONG).show();
            return false;
        }else
        if (et_city.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_city),Snackbar.LENGTH_LONG).show();
            return false;
        }else
        if (et_zipCode.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_addAddress,getResources().getString(R.string.enter_postalcode),Snackbar.LENGTH_LONG).show();
            return false;
        }



        return true;
    }

    @OnClick(R.id.btn_deleteAddress)
    public void deletedAddress(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().doDeleteAddress(addAddressCallback,userId,addressId);

            }
        } else {
            Snackbar.make(btn_addAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }
}
