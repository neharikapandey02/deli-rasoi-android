package com.example.desirasoi.activities.addaddress;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.OrderSummaryActivity;
import com.example.desirasoi.activities.history_details.HistoryDetailsAdapter;
import com.example.desirasoi.activities.paymenttype.PaymentTypeActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddAddressMain;
import com.example.desirasoi.models.GetAddressMain;
import com.example.desirasoi.models.GetAddressMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SelectAddressActivity extends AppCompatActivity {
    @BindView(R.id.recyclerview_selectAddress)
    RecyclerView recyclerview_selectAddress;
    @BindView(R.id.linearLayout_addNewAddress)
    LinearLayout linearLayout_addNewAddress;

    @BindView(R.id.selectAddressTv)
    TextView selectAddressTv;

    @BindView(R.id.bookingRG)
    RadioGroup bookingRG;

   @BindView(R.id.nowRb)
    RadioButton nowRb;

   @BindView(R.id.futureRb)
   RadioButton futureRb;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imgview_back)
    ImageView imgview_back;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.radioButton_delivery)
    RadioButton radioButton_delivery;

    @BindView(R.id.radioButton_takeAway)
    RadioButton radioButton_takeAway;

    @BindView(R.id.radioButton_diveIn)
    RadioButton radioButton_diveIn;

    @BindView(R.id.btn_choose)
    Button btn_choose;

    String userId;
    SelectAddressAdapter historyAdapter;

    List<GetAddressMainData> getAddressMainData;
    public static int addressPosition = 0;

    private SharedPreferencesData sharedPreferencesData;

    private String cartId;
    private String serviceType="";
    private String addressId="";
    private String orderType="1";
    private String time="";
    private String date="";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        ButterKnife.bind(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        viewFinds();
    }

    private void viewFinds() {



        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);

        recyclerview_selectAddress.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recyclerview_selectAddress.setHasFixedSize(true);

        serviceType = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.SERVICE_TYPE);
        if (serviceType.equalsIgnoreCase("1")){
            recyclerview_selectAddress.setVisibility(View.VISIBLE);
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().doGetAddress(getAddressCallback, userId);

            } else {
                Snackbar.make(linearLayout_addNewAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
            }

        }
        else if (serviceType.equalsIgnoreCase("2")){

            linearLayout_addNewAddress.setVisibility(View.GONE);
            selectAddressTv.setVisibility(View.VISIBLE);
            selectAddressTv.setText("Please click on continue to checkout");
            toolbar.setTitle("Take away");
            recyclerview_selectAddress.setVisibility(View.GONE);
            btn_choose.setText("Continue");
        }

//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//               switch (i){
//                   case R.id.radioButton_delivery:
//                       recyclerview_selectAddress.setVisibility(View.VISIBLE);
//                       serviceType="1";
//                       break;
//                   case R.id.radioButton_takeAway:
//                       recyclerview_selectAddress.setVisibility(View.GONE);
//                       serviceType="2";
//                       break;
//                   case R.id.radioButton_diveIn:
//                       recyclerview_selectAddress.setVisibility(View.GONE);
//                       serviceType="3";
//                       break;
//               }
//            }
//        });

//        bookingRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.nowRb){
//                    orderType = "1";
//                    date = "";
//                    time = "";
//                }else {
//                    orderType = "2";
//                    Calendar mcurrentTime = Calendar.getInstance();
//                    Date c = Calendar.getInstance().getTime();
//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                    date = df.format(c);
//                    Log.e("dateee", date);
//                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                    int minute = mcurrentTime.get(Calendar.MINUTE);
//                    TimePickerDialog mTimePicker;
//                    mTimePicker = new TimePickerDialog(SelectAddressActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                        @Override
//                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//
//                            time = selectedHour+":"+selectedMinute+":00";
//                        }
//                    }, hour, minute, true);//Yes 24 hour time
//                    mTimePicker.setTitle("Select Time");
//                    mTimePicker.show();
//                }
//            }
//        });

        String b_date = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
        String b_time = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
        if (!b_date.equalsIgnoreCase("") && !b_time.equalsIgnoreCase("")){
            orderType = "2";
            date = b_date;
            time = b_time;
        }
        else {
            orderType = "1";
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            date = df.format(c);
            time = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        }





    }

    Callback<GetAddressMain> getAddressCallback = new Callback<GetAddressMain>() {
        @Override
        public void onResponse(Call<GetAddressMain> call, Response<GetAddressMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    getAddressMainData = new ArrayList<>();
                    getAddressMainData.clear();
                    getAddressMainData.addAll(response.body().getData());
                    historyAdapter = new SelectAddressAdapter(getApplicationContext(), getAddressMainData, addressPosition, SelectAddressActivity.this);
                    recyclerview_selectAddress.setAdapter(historyAdapter);
                    //Snackbar.make(linearLayout_addNewAddress,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                } else {
                    getAddressMainData = new ArrayList<>();
                    getAddressMainData.clear();
                    historyAdapter = new SelectAddressAdapter(getApplicationContext(), getAddressMainData, addressPosition, SelectAddressActivity.this);
                    recyclerview_selectAddress.setAdapter(historyAdapter);
                    Snackbar.make(linearLayout_addNewAddress, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<GetAddressMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<CheckoutMain> getCheckoutCallback = new Callback<CheckoutMain>() {
        @Override
        public void onResponse(Call<CheckoutMain> call, Response<CheckoutMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
//                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
//                    //Snackbar.make(linearLayout_addNewAddress,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
//                    Intent intent=new Intent(getApplicationContext(), OrderSummaryActivity.class);
//                    intent.putExtra(Constants.CHECKOUT_ID,response.body().getData().getId());
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                    finish();
                } else {

                    Snackbar.make(linearLayout_addNewAddress, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<CheckoutMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    @OnClick(R.id.linearLayout_addNewAddress)
    public void addNewAddressClicked(View view) {
        Intent intent = new Intent(getApplicationContext(), AddNewAddressActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (serviceType.equalsIgnoreCase("1")){
            recyclerview_selectAddress.setVisibility(View.VISIBLE);
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().doGetAddress(getAddressCallback, userId);

            } else {
                Snackbar.make(linearLayout_addNewAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
            }

        }
        else if (serviceType.equalsIgnoreCase("2")){

            linearLayout_addNewAddress.setVisibility(View.GONE);
            selectAddressTv.setVisibility(View.VISIBLE);
            selectAddressTv.setText("Please click on continue to checkout");
            toolbar.setTitle("Take away");
            recyclerview_selectAddress.setVisibility(View.GONE);
            btn_choose.setText("Continue");
        }
    }

    public void positonOfAddress(int position) {
        addressPosition = position;
    }

    @OnClick(R.id.imgview_back)
    public void backClicked(View view) {
        onBackPressed();
    }


    @OnClick(R.id.btn_choose)
    public void chooseClicked(View view){
        if (serviceType.equalsIgnoreCase("1")){
            if (getAddressMainData.size()>0) {
                addressId = getAddressMainData.get(addressPosition).getId();
                String address = getAddressMainData.get(addressPosition).getName()+" "+
                        getAddressMainData.get(addressPosition).getLastName()+"\n"
                        +"Phone: "+getAddressMainData.get(addressPosition).getContact()+"\n"
                        +getAddressMainData.get(addressPosition).getStreet()+"\n"
                        +getAddressMainData.get(addressPosition).getPostalCode()+", "
                        +getAddressMainData.get(addressPosition).getCity()+".";

                OrderSummaryActivity.addressId = addressId;
                OrderSummaryActivity.deliveryAddressTv.setTextColor(getResources().getColor(R.color.black));
                OrderSummaryActivity.deliveryAddressTv.setText(address);
            }
            else {
                Snackbar.make(linearLayout_addNewAddress, "Please add address", Snackbar.LENGTH_LONG).show();
                return;
            }
        }else{

            addressId="";
        }
//        Log.d("TAG","addressid="+addressId+",cartId="+cartId+",serviceType="+serviceType);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            progressBar.setVisibility(View.VISIBLE);
            cartId=getIntent().getStringExtra(Constants.CART_ID);
            RetrofitHelper.getInstance().docheckout2(getCheckoutCallback, userId,cartId,addressId);

        } else {
            Snackbar.make(linearLayout_addNewAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }




    }
}
