package com.example.desirasoi.activities.addaddress;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.models.GetAddressMainData;


import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class SelectAddressAdapter extends RecyclerView.Adapter<SelectAddressAdapter.RestaurantList> {
    private Context context;
    private List<GetAddressMainData> getAddressMainData;
    public static int row_item_selected;
    ArrayList editarraylist;
    SelectAddressActivity selectAddressActivity;
    public SelectAddressAdapter(Context context,List<GetAddressMainData> getAddressMainData,int row_item_selectednew,SelectAddressActivity selectAddressActivity){
        this.context=context;
        this.getAddressMainData=getAddressMainData;
        row_item_selected=row_item_selectednew;
        this.selectAddressActivity=selectAddressActivity;
        editarraylist=new ArrayList();
    }


    @NonNull
    @Override
    public SelectAddressAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_select_address,null);
        SelectAddressAdapter.RestaurantList restaurantList=new SelectAddressAdapter.RestaurantList(view);
        return restaurantList;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SelectAddressAdapter.RestaurantList holder, int position) {
        //clickDetails(holder);
        holder.tv_address.setText(getAddressMainData.get(position).getName()+" "+
                getAddressMainData.get(position).getLastName()+"\n"
                        +"Phone: "+getAddressMainData.get(position).getContact()+"\n"
                        +getAddressMainData.get(position).getStreet()+",\n"
        +getAddressMainData.get(position).getPostalCode()+", "
        +getAddressMainData.get(position).getCity()+"."
        );
        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_item_selected=position;
                selectAddressActivity.positonOfAddress(position);
                notifyDataSetChanged();

            }
        });
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAddressActivity.positonOfAddress(position);
                Intent intent=new Intent(context, EditAddress.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                editarraylist.add(getAddressMainData.get(position).getId());
                editarraylist.add(getAddressMainData.get(position).getName());
                editarraylist.add(getAddressMainData.get(position).getContact());
                editarraylist.add(getAddressMainData.get(position).getApartment());
                editarraylist.add(getAddressMainData.get(position).getStreet());
                editarraylist.add(getAddressMainData.get(position).getCity());
                editarraylist.add(getAddressMainData.get(position).getPostalCode());
                editarraylist.add(getAddressMainData.get(position).getLastName());
                intent.putStringArrayListExtra(Constants.EDIT_ADDRESS,editarraylist);
                context.startActivity(intent);
            }
        });
        if (row_item_selected==position){
            holder.radioButton.setChecked(true);
            holder.btn_edit.setVisibility(View.VISIBLE);
        }else{
            holder.radioButton.setChecked(false);
            holder.btn_edit.setVisibility(View.GONE);
        }

    }

    private void clickDetails(@NonNull SelectAddressAdapter.RestaurantList holder) {

    }

    @Override
    public int getItemCount() {
        return getAddressMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        CardView cardview_one,cardview_two;
        RelativeLayout relativeLayout_top;
        TextView tv_address;
        RadioButton radioButton;
        Button btn_edit;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            //cardview_one=itemView.findViewById(R.id.cardview_one);
            //cardview_two=itemView.findViewById(R.id.cardview_two);
            relativeLayout_top=itemView.findViewById(R.id.relativeLayout_top);
            tv_address=itemView.findViewById(R.id.tv_address);
            radioButton=itemView.findViewById(R.id.radioButton);
            btn_edit=itemView.findViewById(R.id.btn_edit);
        }
    }
}
