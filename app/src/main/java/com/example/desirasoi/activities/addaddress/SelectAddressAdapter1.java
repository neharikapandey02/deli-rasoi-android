package com.example.desirasoi.activities.addaddress;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.models.GetAddressMainData;

import java.util.ArrayList;
import java.util.List;

public class SelectAddressAdapter1 extends RecyclerView.Adapter<SelectAddressAdapter1.RestaurantList> {
    private Context context;
    private List<GetAddressMainData> getAddressMainData;
    public static int row_item_selected;
    ArrayList editarraylist;
    public SelectAddressAdapter1(Context context, List<GetAddressMainData> getAddressMainData){
        this.context=context;
        this.getAddressMainData=getAddressMainData;
        editarraylist=new ArrayList();
    }


    @NonNull
    @Override
    public SelectAddressAdapter1.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_select_address_new,null);
        SelectAddressAdapter1.RestaurantList restaurantList=new SelectAddressAdapter1.RestaurantList(view);
        return restaurantList;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SelectAddressAdapter1.RestaurantList holder, int position) {
        //clickDetails(holder);
        holder.tv_address.setText(getAddressMainData.get(position).getName()+" "+
                getAddressMainData.get(position).getLastName()+"\n"
                        +"Phone: "+getAddressMainData.get(position).getContact()+"\n"
                        +getAddressMainData.get(position).getStreet()+",\n"
        +getAddressMainData.get(position).getPostalCode()+", "
        +getAddressMainData.get(position).getCity()+"."
        );
        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, EditAddress.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                editarraylist.add(getAddressMainData.get(position).getId());
                editarraylist.add(getAddressMainData.get(position).getName());
                editarraylist.add(getAddressMainData.get(position).getContact());
                editarraylist.add(getAddressMainData.get(position).getApartment());
                editarraylist.add(getAddressMainData.get(position).getStreet());
                editarraylist.add(getAddressMainData.get(position).getCity());
                editarraylist.add(getAddressMainData.get(position).getPostalCode());
                editarraylist.add(getAddressMainData.get(position).getLastName());
                intent.putStringArrayListExtra(Constants.EDIT_ADDRESS,editarraylist);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return getAddressMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        RelativeLayout relativeLayout_top;
        TextView tv_address;
        RadioButton radioButton;
        Button btn_edit;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);

            relativeLayout_top=itemView.findViewById(R.id.relativeLayout_top);
            tv_address=itemView.findViewById(R.id.tv_address);
            radioButton=itemView.findViewById(R.id.radioButton);
            btn_edit=itemView.findViewById(R.id.btn_edit);
        }
    }
}
