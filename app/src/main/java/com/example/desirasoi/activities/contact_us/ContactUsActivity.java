package com.example.desirasoi.activities.contact_us;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.ui.reserved_table.ReservedTableFragment;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class ContactUsActivity extends AppCompatActivity {
    @BindView(R.id.spinner_selectRestaurant)
    Spinner spinner_selectRestaurant;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_subject)
    EditText et_subject;
    @BindView(R.id.et_message)
    EditText et_message;
    @BindView(R.id.btn_sendMessage)
    Button btn_sendMessage;

    List<GetRestaurantMainData> getRestaurantMainData;
    SelectRestaurantAdapter selectRestaurantAdapter;

    private SharedPreferencesData sharedPreferencesData;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        String userName = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.FIRST_NAME) + " " +
                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.LAST_NAME);
        String email = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.EMAIL);
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
        et_name.setText(userName);
        et_email.setText(email);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doGetResturantList(restaurantList);

        } else {
            Snackbar.make(spinner_selectRestaurant, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_sendMessage)
    public void sendMessageClicked(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                String restaurantId=getRestaurantMainData.get(spinner_selectRestaurant.getSelectedItemPosition()).getId();
                String name=et_name.getText().toString();
                String email=et_email.getText().toString();
                String subject=et_subject.getText().toString();
                String message=et_message.getText().toString();
                RetrofitHelper.getInstance().doContactUs(contactCallback,userId,restaurantId,name,email,subject,message);
            }


        } else {
            Snackbar.make(spinner_selectRestaurant, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    private boolean validation() {
        if (et_name.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.enter_name),Toast.LENGTH_LONG).show();
            return false;
        }else
        if (et_email.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.enter_email),Toast.LENGTH_LONG).show();
            return false;
        }else
        if (et_subject.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.enter_subject),Toast.LENGTH_LONG).show();
            return false;
        }else
        if (et_message.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.enter_message),Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    Callback<GetRestaurantMain> restaurantList = new Callback<GetRestaurantMain>() {
        @Override
        public void onResponse(Call<GetRestaurantMain> call, Response<GetRestaurantMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        getRestaurantMainData = new ArrayList();
                        getRestaurantMainData.clear();
                        getRestaurantMainData.addAll(response.body().getData());

                        selectRestaurantAdapter = new SelectRestaurantAdapter(getApplicationContext(), R.layout.item_spinner_text_home, getRestaurantMainData);
                        selectRestaurantAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                        spinner_selectRestaurant.setAdapter(selectRestaurantAdapter);

                    } else {
                        Snackbar.make(spinner_selectRestaurant, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<GetRestaurantMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };


    Callback<ContactUsMain> contactCallback = new Callback<ContactUsMain>() {
        @Override
        public void onResponse(Call<ContactUsMain> call, Response<ContactUsMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        et_subject.setText("");
                        et_message.setText("");
                        Snackbar.make(spinner_selectRestaurant, response.body().getMessage(), Snackbar.LENGTH_LONG).show();

                    } else {
                        Snackbar.make(spinner_selectRestaurant, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<ContactUsMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    //Select restaurant adapter
    class SelectRestaurantAdapter extends ArrayAdapter<String> {
        public SelectRestaurantAdapter(Context ctx, int txtViewResourceId, List arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(getRestaurantMainData.get(position).getRestaurantName());
            return mySpinner;
        }
    }
}
