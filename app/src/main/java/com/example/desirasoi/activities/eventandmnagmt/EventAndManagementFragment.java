package com.example.desirasoi.activities.eventandmnagmt;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.contact_us.ContactUsActivity;
import com.example.desirasoi.activities.contact_us.GetRestaurantMain;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventAndManagementFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventAndManagementFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.recyclerview_event)
    RecyclerView recyclerview_event;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SharedPreferencesData sharedPreferencesData;
    EventListAdapter restaurantListAdapter;
    List<NewMainData> newMainDataList;

    public EventAndManagementFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EventAndManagementFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EventAndManagementFragment newInstance(String param1, String param2) {
        EventAndManagementFragment fragment = new EventAndManagementFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event_and_management, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferencesData = new SharedPreferencesData(getContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        MainPageActivity.swipeRefresh.setEnabled(false);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        recyclerview_event.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerview_event.setHasFixedSize(true);



        if (NetworkUtil.checkNetworkStatus(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doGetNewsList(newListCallback);

        } else {
            Snackbar.make(recyclerview_event, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<NewsMain> newListCallback = new Callback<NewsMain>() {
        @Override
        public void onResponse(Call<NewsMain> call, Response<NewsMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        newMainDataList=new ArrayList<>();
                        newMainDataList.clear();
                        newMainDataList.addAll(response.body().getData());
                        restaurantListAdapter= new EventListAdapter(getContext(),newMainDataList);
                        recyclerview_event.setAdapter(restaurantListAdapter);

                    } else {
                        Snackbar.make(recyclerview_event, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<NewsMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };
}
