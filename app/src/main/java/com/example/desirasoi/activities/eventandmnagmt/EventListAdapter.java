package com.example.desirasoi.activities.eventandmnagmt;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.menu_details.RestaurantMenuDetailsActivity;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.models.RestaurantListMainData;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.RestaurantList> {
    private Context context;
    private List<NewMainData> newMainData;
    public EventListAdapter(Context context,List<NewMainData> newMainData){
        this.context=context;
        this.newMainData=newMainData;

    }


    @NonNull
    @Override
    public EventListAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_event_and_management,null);
        EventListAdapter.RestaurantList restaurantList=new EventListAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull EventListAdapter.RestaurantList holder, int position) {

    Picasso.with(context).load(newMainData.get(position).getImage())
            .placeholder(R.drawable.restaurant_list)
            .error(R.drawable.restaurant_list)
            .into(holder.imgview_restImage);
        holder.tv_restName.setText(newMainData.get(position).getMainHeading());
        holder.tv_content.setText(newMainData.get(position).getContent().replaceAll("\\<.*?\\>", ""));
    /* holder.cardview_two.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             Intent intent=new Intent(context, SpecialityRestaurantList.class);
             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             context.startActivity(intent);
         }
     });*/
    }

    @Override
    public int getItemCount() {
        return newMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{

   ImageView imgview_restImage;
   TextView tv_restName;
   TextView tv_content;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            imgview_restImage=itemView.findViewById(R.id.imgview_restImage);
            tv_restName=itemView.findViewById(R.id.tv_restName);
            tv_content=itemView.findViewById(R.id.tv_content);


        }
    }
}
