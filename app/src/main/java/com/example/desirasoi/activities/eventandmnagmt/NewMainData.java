package com.example.desirasoi.activities.eventandmnagmt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewMainData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("main_heading")
    @Expose
    private String mainHeading;
    @SerializedName("main_heading_fi")
    @Expose
    private String mainHeadingFi;
    @SerializedName("sub_heading")
    @Expose
    private String subHeading;
    @SerializedName("sub_heading_fi")
    @Expose
    private String subHeadingFi;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("content_fi")
    @Expose
    private String contentFi;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMainHeading() {
        return mainHeading;
    }

    public void setMainHeading(String mainHeading) {
        this.mainHeading = mainHeading;
    }

    public String getMainHeadingFi() {
        return mainHeadingFi;
    }

    public void setMainHeadingFi(String mainHeadingFi) {
        this.mainHeadingFi = mainHeadingFi;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }

    public String getSubHeadingFi() {
        return subHeadingFi;
    }

    public void setSubHeadingFi(String subHeadingFi) {
        this.subHeadingFi = subHeadingFi;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentFi() {
        return contentFi;
    }

    public void setContentFi(String contentFi) {
        this.contentFi = contentFi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
