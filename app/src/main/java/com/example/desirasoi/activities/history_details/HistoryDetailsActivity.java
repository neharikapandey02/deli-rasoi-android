package com.example.desirasoi.activities.history_details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.ui.HistoryDetails;
import com.example.desirasoi.activities.ui.history.HistoryMain;
import com.example.desirasoi.activities.ui.history.HistoryOrderMainData;
import com.example.desirasoi.activities.ui.history.HistoryOrderMenuMainData;
import com.example.desirasoi.adapters.HistoryAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class HistoryDetailsActivity extends AppCompatActivity {
    @BindView(R.id.recyclerview_menuItem)
    RecyclerView recyclerview_menuItem;

    @BindView(R.id.tv_subTotal)
    TextView tv_subTotal;

    @BindView(R.id.subtotalTv)
    TextView subtotalTv;

    @BindView(R.id.deliveryRl)
    RelativeLayout deliveryRl;

    @BindView(R.id.discountRl)
    RelativeLayout discountRl;

    @BindView(R.id.tv_delivertyCharges)
    TextView tv_delivertyCharges;

     @BindView(R.id.addressText)
    TextView addressText;

    @BindView(R.id.tv_totalPrice)
    TextView tv_totalPrice;

    @BindView(R.id.tv_couponDiscount)
    TextView tv_couponDiscount;

    @BindView(R.id.tv_afterDiscount)
    TextView tv_afterDiscount;

    @BindView(R.id.tv_address)
    TextView tv_address;

    @BindView(R.id.tv_number)
    TextView tv_number;

    @BindView(R.id.tv_status)
    TextView tv_status;

    @BindView(R.id.orderTimeStatusTv)
    TextView orderTimeStatusTv;

   @BindView(R.id.orderTimeTv)
    TextView orderTimeTv;

    @BindView(R.id.orderStatusTv)
    TextView orderStatusTv;

    @BindView(R.id.orderTypeTv)
    TextView orderTypeTv;

    @BindView(R.id.restNameTv)
    TextView restNameTv;

    @BindView(R.id.deliveryAddresTv)
    TextView deliveryAddresTv;

    @BindView(R.id.rest_address)
    TextView rest_address;

    @BindView(R.id.restNumberTv)
    TextView restNumberTv;

    @BindView(R.id.imageView_back)
    ImageView imageView_back;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;

    @BindView(R.id.deliveryAddRL)
    RelativeLayout deliveryAddRL;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    List<HistoryOrderMenuMainData> orderMenuMainData;
    HistoryOrderMainData historyMains;
    private int position;
    private int status;
    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_history_details);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        position = getIntent().getIntExtra(Constants.POSITION, 0);
        status = Integer.parseInt(getIntent().getStringExtra(Constants.STATUS));

        historyMains = (HistoryOrderMainData) getIntent().getSerializableExtra("array");
        orderMenuMainData = historyMains.getOrderMenus();
        recyclerview_menuItem.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recyclerview_menuItem.setHasFixedSize(true);
        HistoryDetailsAdapter historyAdapter = new HistoryDetailsAdapter(getApplicationContext(), orderMenuMainData);
        recyclerview_menuItem.setAdapter(historyAdapter);

        String stotal = historyMains.getSubTotal();
        stotal = stotal.replace(".", ",");
        subtotalTv.setText(stotal + getResources().getString(R.string.euro_sign));
        String subTotal = historyMains.getTax();
        Log.e("subtotal", subTotal);
        subTotal = subTotal.replace(".", ",");
        Log.e("subtotal", subTotal);
        tv_subTotal.setText(subTotal + getResources().getString(R.string.euro_sign));
        String deliveryCharges = historyMains.getDeliveryCharge();
        deliveryCharges = deliveryCharges.replace(".", ",");
        tv_delivertyCharges.setText(deliveryCharges + getResources().getString(R.string.euro_sign));
        String grandTotal = historyMains.getGrandTotal();
        grandTotal = grandTotal.replace(".", ",");
        tv_totalPrice.setText(grandTotal + getResources().getString(R.string.euro_sign));
        double amount = Double.parseDouble(historyMains.getGrandTotal());
        if (historyMains.getCoupon_discount() != null) {
            String couponDiscount = historyMains.getCoupon_discount();
            couponDiscount = couponDiscount.replace(".", ",");
            tv_couponDiscount.setText("- " + couponDiscount + getResources().getString(R.string.euro_sign));

        } else {
            tv_couponDiscount.setText("- 0" + getResources().getString(R.string.euro_sign));
        }
        String finalAmount = String.valueOf(amount);
        finalAmount = finalAmount.replace(".", ",");
        tv_afterDiscount.setText(finalAmount + getResources().getString(R.string.euro_sign));
        if (status == 0) {
            orderStatusTv.setText("Pending from restaurant");

        } else if (status == 1) {
            orderStatusTv.setText("Order Accepted");
        } else if (status == 2) {
            orderStatusTv.setText("Order Completed");
            btn_cancel.setVisibility(View.GONE);
        } else if (status == 3) {
            orderStatusTv.setText("Cancelled by Restaurant");
            btn_cancel.setVisibility(View.GONE);
        } else if (status == 4) {
            orderStatusTv.setText("Cancelled by User");
            btn_cancel.setVisibility(View.GONE);
        } else if (status == 5) {
            orderStatusTv.setText("Order in progress");
            btn_cancel.setVisibility(View.GONE);
        } else if (status == 6) {
            orderStatusTv.setText("Order is ready");
            btn_cancel.setVisibility(View.GONE);
        } else if (status == 7) {
            orderStatusTv.setText("In delivery");
            btn_cancel.setVisibility(View.GONE);
        }
        String a = historyMains.getPayment().getPaymentType();
        if (a.equalsIgnoreCase("1")) {
            tv_address.setText("Online paid");
        } else {
            tv_address.setText("COD");
        }

        restNameTv.setText(historyMains.getRestaurant().getRestaurantName());

        restNumberTv.setText(historyMains.getRestaurant().getContact());
        tv_number.setText(historyMains.getOrder_number());
        String b = historyMains.getServiceType();
        if (b.equalsIgnoreCase("1")) {
            orderTypeTv.setText("Delivery");
            deliveryRl.setVisibility(View.VISIBLE);
            deliveryAddresTv.setText(historyMains.getDelivery_address().getStreet()+",\n"
                    +historyMains.getDelivery_address().getPostalCode()+", "
                    +historyMains.getDelivery_address().getCity()+""
            );
            rest_address.setText(historyMains.getRestaurant().getAddress());
            orderTimeStatusTv.setText("Delivery Time");
        } else if (b.equalsIgnoreCase("2")) {
            deliveryAddRL.setVisibility(View.GONE);
            orderTypeTv.setText("Pick Up");
            addressText.setText("Restaurant address");
            rest_address.setText(historyMains.getRestaurant().getAddress());
            deliveryRl.setVisibility(View.GONE);
            orderTimeStatusTv.setText("Take Away Time");
        }
        orderTimeTv.setText(parseDateToddMMyyyy(historyMains.getOrder_date()+" "+
                historyMains.getOrder_time()));

        tv_status.setText(parseDateToddMMyyyy(historyMains.getCreatedAt()));

        String orderDate = historyMains.getOrder_date()+" "+historyMains.getOrder_time();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date strDate = null;
        try {
            strDate = sdf.parse(orderDate);
            Log.e("dafdkfj", strDate.toString());
            Log.e("dafdkfj", orderDate.toString());
            if (new Date().after(strDate)) {
                btn_cancel.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "d.M.yyyy H.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @OnClick(R.id.imageView_back)
    public void backClicked(View view) {
        onBackPressed();
    }

    @OnClick(R.id.btn_cancel)
    public void buttonCancelClicked(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
            String order_id=orderMenuMainData.get(0).getOrderId();
            RetrofitHelper.getInstance().doCancelOrder(cancelOrderCallback, userId,order_id);

        } else {
            Snackbar.make(tv_subTotal, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<CancelMainData> cancelOrderCallback = new Callback<CancelMainData>() {
        @Override
        public void onResponse(Call<CancelMainData> call, Response<CancelMainData> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    Snackbar.make(tv_subTotal,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                    btn_cancel.setVisibility(View.GONE);
                    tv_status.setText("Cancelled by User");
                } else {
                    Snackbar.make(tv_subTotal,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<CancelMainData> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
