package com.example.desirasoi.activities.history_details;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.ui.history.HistoryMain;
import com.example.desirasoi.activities.ui.history.HistoryOrderMenuMainData;
import com.example.desirasoi.adapters.HistoryAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class HistoryDetailsAdapter  extends RecyclerView.Adapter<HistoryDetailsAdapter.RestaurantList> {
    private Context context;
    List<HistoryOrderMenuMainData> orderMenuMainData;

    public HistoryDetailsAdapter(Context context,List<HistoryOrderMenuMainData> orderMenuMainData){
        this.context=context;
        this.orderMenuMainData=orderMenuMainData;

    }


    @NonNull
    @Override
    public HistoryDetailsAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_order_cart,null);
        HistoryDetailsAdapter.RestaurantList restaurantList=new HistoryDetailsAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryDetailsAdapter.RestaurantList holder, int position) {
        holder.tv_itemName.setText(orderMenuMainData.get(position).getMenuName());
        String price = orderMenuMainData.get(position).getPrice();
        price = price.replace(".", ",");
        holder.tv_rate.setText(price + context.getResources().getString(R.string.euro_sign));
        holder.tv_quantity.setText(orderMenuMainData.get(position).getQuantity());
        Double p = Double.valueOf(orderMenuMainData.get(position).getPrice());
        int q = Integer.parseInt(orderMenuMainData.get(position).getQuantity());
        p = p*q;
        String totalPrice = String.format("%.2f", p);
        totalPrice = totalPrice.replace(".", ",");
        holder.tv_Totalprice.setText(totalPrice+context.getResources().getString(R.string.euro_sign));
        try {
            if (orderMenuMainData.get(position).getDiscount() != null) {
                String discount = orderMenuMainData.get(position).getDiscount();
                discount = discount.replace(".", ",");
                holder.discountTv.setText("-"+discount + context.getResources().getString(R.string.euro_sign));
            }
            else {
//                holder.discountTv.setText("-0" + context.getResources().getString(R.string.euro_sign));
                holder.discountTv.setVisibility(View.GONE);
                holder.disTv.setVisibility(View.GONE);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            }


        clickDetails(holder);
    }

    private void clickDetails(@NonNull HistoryDetailsAdapter.RestaurantList holder) {

    }

    @Override
    public int getItemCount() {
        return orderMenuMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        CardView cardview_one,cardview_two;
        RelativeLayout relativeLayout_top;
        TextView tv_itemName;
        TextView tv_rate;
        TextView tv_quantity;
        TextView tv_Totalprice, discountTv, disTv;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            //cardview_one=itemView.findViewById(R.id.cardview_one);
            //cardview_two=itemView.findViewById(R.id.cardview_two);
            relativeLayout_top=itemView.findViewById(R.id.relativeLayout_top);
            tv_itemName=itemView.findViewById(R.id.tv_itemName);
            tv_rate=itemView.findViewById(R.id.tv_rate);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            tv_Totalprice=itemView.findViewById(R.id.tv_Totalprice);
            discountTv=itemView.findViewById(R.id.discountTv);
            disTv=itemView.findViewById(R.id.disTv);
        }
    }
}
