package com.example.desirasoi.activities.lunch_details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.menu_details.CategoryListAdapter;
import com.example.desirasoi.models.CategoryListMainData;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class LunchCategoryAdapter extends RecyclerView.Adapter<LunchCategoryAdapter.RestaurantList> {
    private Context context;
    private List foodMenuMainDataList;
    public static int row_index ;
    LunchDetailsActivity lunchDetailsActivity;
    public LunchCategoryAdapter(Context context,List foodMenuMainDataList,LunchDetailsActivity lunchDetailsActivity){
        this.context=context;
        this.foodMenuMainDataList=foodMenuMainDataList;
        this.lunchDetailsActivity=lunchDetailsActivity;
        row_index=0;
    }


    @NonNull
    @Override
    public LunchCategoryAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_category,null);
        LunchCategoryAdapter.RestaurantList restaurantList=new LunchCategoryAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull LunchCategoryAdapter.RestaurantList holder, int position) {

        holder.tv_categoryName.setText(foodMenuMainDataList.get(position).toString());
        holder.tv_categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.tv_categoryName.setBackgroundResource(R.color.logo_color);
                holder.tv_categoryName.setTextColor(ContextCompat.getColor(context, R.color.white));
                lunchDetailsActivity.callByCategory(position==0?"0":"1");
                row_index=position;
                notifyDataSetChanged();

            }
        });
        if(row_index==position){
            holder.tv_categoryName.setBackgroundResource(R.color.logo_color);
            holder.tv_categoryName.setTextColor(ContextCompat.getColor(context, R.color.white));

        }
        else
        {
            holder.tv_categoryName.setBackgroundResource(R.color.gray_border);
            holder.tv_categoryName.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return foodMenuMainDataList.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{


        TextView tv_categoryName;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            tv_categoryName=itemView.findViewById(R.id.tv_categoryName);

        }
    }
}
