package com.example.desirasoi.activities.lunch_details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.menu_details.CategoryListAdapter;
import com.example.desirasoi.activities.menu_details.RestaurantMenuDetailsAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.CategoryListMain;
import com.example.desirasoi.models.CategoryListMainData;
import com.example.desirasoi.models.FoodMenuMain;
import com.example.desirasoi.models.FoodMenuMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class LunchDetailsActivity extends AppCompatActivity {
    @BindView(R.id.imgview_back)
    ImageView imgview_back;

    @BindView(R.id.recyclerview_details)
    RecyclerView recyclerview_details;

    @BindView(R.id.recyclerview_category)
    RecyclerView recyclerview_category;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    RestaurantLunchDetailsAdapter restaurantListAdapter;
    LunchCategoryAdapter categoryListAdapter;
    private SharedPreferencesData sharedPreferencesData;

    private List<FoodMenuMainData> foodMenuMainDataList;
    private List<CategoryListMainData> categoryListMainData;
    private List listofCategory;

    String restaurantId;
    int lunchTodayOrWeekId=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_lunch_details);
        ButterKnife.bind(this);
        viewFinds();
    }
    private void viewFinds() {
        restaurantId=getIntent().getStringExtra(Constants.ID_RESTAURANT);

        recyclerview_details.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recyclerview_details.setHasFixedSize(true);

        recyclerview_category.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerview_category.setHasFixedSize(true);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
            String userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
            progressBar.setVisibility(View.VISIBLE);
            //RetrofitHelper.getInstance().doCategoryList(categoryCallback);
            listofCategory=new ArrayList();
            listofCategory.clear();
            listofCategory.add("Today Lunch");
            listofCategory.add("Week Lunch");
            categoryListAdapter= new LunchCategoryAdapter(getApplicationContext(),listofCategory,LunchDetailsActivity.this);
            recyclerview_category.setAdapter(categoryListAdapter);
            //0 for today and 1 for next weeks
            String week_menu=0+"";
            RetrofitHelper.getInstance().doLunchMenuList(menuListCallback,userId,restaurantId,week_menu,
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));

        } else {
            Snackbar.make(recyclerview_details, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }






    }



    Callback<FoodMenuMain> menuListCallback = new Callback<FoodMenuMain>() {
        @Override
        public void onResponse(Call<FoodMenuMain> call, Response<FoodMenuMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {

                        foodMenuMainDataList=new ArrayList<>();
                        foodMenuMainDataList.clear();
                        foodMenuMainDataList.addAll(response.body().getData());

                        restaurantListAdapter= new RestaurantLunchDetailsAdapter(getApplicationContext(),foodMenuMainDataList,restaurantId,progressBar,lunchTodayOrWeekId);
                        recyclerview_details.setAdapter(restaurantListAdapter);

                    } else {
                        Snackbar.make(recyclerview_details, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                        foodMenuMainDataList=new ArrayList<>();
                        foodMenuMainDataList.clear();
                        restaurantListAdapter= new RestaurantLunchDetailsAdapter(getApplicationContext(),foodMenuMainDataList,restaurantId,progressBar,lunchTodayOrWeekId);
                        recyclerview_details.setAdapter(restaurantListAdapter);
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<FoodMenuMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    Callback<CategoryListMain> categoryCallback = new Callback<CategoryListMain>() {
        @Override
        public void onResponse(Call<CategoryListMain> call, Response<CategoryListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {

                        categoryListMainData=new ArrayList<>();
                        categoryListMainData.clear();
                        categoryListMainData.addAll(response.body().getData());
                        categoryListAdapter= new LunchCategoryAdapter(getApplicationContext(),categoryListMainData,LunchDetailsActivity.this);
                        recyclerview_category.setAdapter(categoryListAdapter);

                    } else {
                        Snackbar.make(recyclerview_details, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<CategoryListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };




    @OnClick(R.id.imgview_back)
    public void backClicked(View view){
        onBackPressed();
    }

    public void callByCategory(String categoryId){
        lunchTodayOrWeekId=Integer.parseInt(categoryId);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
            String userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
            progressBar.setVisibility(View.VISIBLE);
            // RetrofitHelper.getInstance().doCategoryList(categoryCallback);
            RetrofitHelper.getInstance().doLunchMenuList(menuListCallback,userId,restaurantId,categoryId,
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));

        } else {
            Snackbar.make(recyclerview_details, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }
}
