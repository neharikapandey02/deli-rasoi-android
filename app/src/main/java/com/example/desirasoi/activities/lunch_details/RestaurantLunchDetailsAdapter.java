package com.example.desirasoi.activities.lunch_details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.menu_details.RestaurantMenuDetailsAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddMinusMain;
import com.example.desirasoi.models.FoodMenuMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantLunchDetailsAdapter extends RecyclerView.Adapter<RestaurantLunchDetailsAdapter.RestaurantList> {
    private Context context;
    private List<FoodMenuMainData> foodMenuMainDataList;
    SharedPreferencesData sharedPreferencesData;
    String userId;
    String restaurantId;
    private int position;
    private String quantity;
    ProgressBar progressBar;
    TextView tv_count;
    int lunchTodayOrWeekId;
    public RestaurantLunchDetailsAdapter(Context context,List<FoodMenuMainData> foodMenuMainDataList,String restaurantId,ProgressBar progressBar,int lunchTodayOrWeekId){
        this.context=context;
        this.foodMenuMainDataList=foodMenuMainDataList;
        sharedPreferencesData=new SharedPreferencesData(context);
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
        this.restaurantId=restaurantId;
        this.progressBar=progressBar;
        this.lunchTodayOrWeekId=lunchTodayOrWeekId;
    }


    @NonNull
    @Override
    public RestaurantLunchDetailsAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_menu_list_details,null);
        RestaurantLunchDetailsAdapter.RestaurantList restaurantList=new RestaurantLunchDetailsAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantLunchDetailsAdapter.RestaurantList holder, int position) {
        if (foodMenuMainDataList.get(position).getImage()!=null){
            Picasso.with(context)
                    .load(foodMenuMainDataList.get(position).getImage())
                    .error(R.drawable.restaurant_list)
                    .fit()
                    .placeholder(R.drawable.restaurant_list)
                    .into(holder.imageView_restaurant);
        }


        holder.tv_price.setText("$ "+foodMenuMainDataList.get(position).getPrice());
        holder.tv_menuName.setText(foodMenuMainDataList.get(position).getMenu());
        holder.tv_quantity.setText(foodMenuMainDataList.get(position).getCartQuantity()+"");
        if (lunchTodayOrWeekId==1){
            holder.layout_LinearAddMinus.setVisibility(View.GONE);
        }else{
            holder.layout_LinearAddMinus.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return foodMenuMainDataList.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{

        ImageView imageView_restaurant;
        TextView tv_menuName,tv_quantity,tv_price;
        RatingBar ratingBar_menu;
        LinearLayout addLinearLayout,minusLinearLayout,layout_LinearAddMinus;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            imageView_restaurant=itemView.findViewById(R.id.imageView_restaurant);
            tv_menuName=itemView.findViewById(R.id.tv_menuName);
            tv_price=itemView.findViewById(R.id.tv_price);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            addLinearLayout=itemView.findViewById(R.id.addLinearLayout);
            minusLinearLayout=itemView.findViewById(R.id.minusLinearLayout);
            layout_LinearAddMinus=itemView.findViewById(R.id.layout_LinearAddMinus);
            tv_count=tv_quantity;
            addLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        if (lunchTodayOrWeekId==0){
                            progressBar.setVisibility(View.VISIBLE);
                            position=getAdapterPosition();
                            quantity=(foodMenuMainDataList.get(position).getCartQuantity()+1)+"";
                            RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,foodMenuMainDataList.get(getAdapterPosition()).getId(),quantity,
                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));
                        }else{
                            Snackbar.make(imageView_restaurant, context.getResources().getString(R.string.only_today_book), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(imageView_restaurant, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            minusLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        if (foodMenuMainDataList.get(getAdapterPosition()).getCartQuantity()!=0){
                            progressBar.setVisibility(View.VISIBLE);
                            position=getAdapterPosition();
                            quantity=(foodMenuMainDataList.get(position).getCartQuantity()-1)+"";
                            RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,foodMenuMainDataList.get(getAdapterPosition()).getId(),quantity,
                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));

                        }

                    } else {
                        Snackbar.make(imageView_restaurant, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

        }

        }

    Callback<AddMinusMain> addItemCallback = new Callback<AddMinusMain>() {
        @Override
        public void onResponse(Call<AddMinusMain> call, Response<AddMinusMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                //if (response.body() != null) {
                if (response.body().getSuccess()) {
                    FoodMenuMainData foodMenuMainData=new FoodMenuMainData();
                    foodMenuMainData.setId(foodMenuMainDataList.get(position).getId());
                    foodMenuMainData.setMenu(foodMenuMainDataList.get(position).getMenu());
                    foodMenuMainData.setMenuType(foodMenuMainDataList.get(position).getMenuType());
                    foodMenuMainData.setImage(foodMenuMainDataList.get(position).getImage());
                    foodMenuMainData.setDescription(foodMenuMainDataList.get(position).getDescription());
                    foodMenuMainData.setPrice(foodMenuMainDataList.get(position).getPrice());
                    foodMenuMainData.setStatus(foodMenuMainDataList.get(position).getStatus());
                    foodMenuMainData.setCreatedAt(foodMenuMainDataList.get(position).getCreatedAt());
                    foodMenuMainData.setUpdatedAt(foodMenuMainDataList.get(position).getUpdatedAt());
                    foodMenuMainData.setCategory(foodMenuMainDataList.get(position).getCategory());
                    foodMenuMainData.setCartQuantity(Integer.parseInt(quantity));
                    foodMenuMainDataList.set(position,foodMenuMainData);
                    notifyDataSetChanged();
                    Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
                }
                // }
            }else{
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<AddMinusMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };
}
