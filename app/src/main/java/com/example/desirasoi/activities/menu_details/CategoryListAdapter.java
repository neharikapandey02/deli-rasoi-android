package com.example.desirasoi.activities.menu_details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.models.CategoryListMainData;
import com.example.desirasoi.models.FoodMenuMainData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.RestaurantList> {
    static Context context;
//    private List<CategoryListMainData> foodMenuMainDataList;
    private RestaurantMenuDetailsActivity restaurantMenuDetailsActivity;
    public static int row_index ;
    HashMap<String, ArrayList<FoodMenuMainData>> hashMap;
    public CategoryListAdapter(Context context, HashMap<String, ArrayList<FoodMenuMainData>> hashMap, RestaurantMenuDetailsActivity restaurantMenuDetailsActivity){
        this.context=context;
        this.hashMap = hashMap;
//        this.foodMenuMainDataList=foodMenuMainDataList;
        this.restaurantMenuDetailsActivity = restaurantMenuDetailsActivity;
        row_index=0;
    }

    public void updateList(HashMap<String, ArrayList<FoodMenuMainData>> hashMap){
        this.hashMap = hashMap;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public CategoryListAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_category,null);
        CategoryListAdapter.RestaurantList restaurantList=new CategoryListAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListAdapter.RestaurantList holder, int position) {

        holder.tv_categoryName.setText(hashMap.keySet().toArray()[position].toString());
        holder.tv_categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.tv_categoryName.setBackgroundResource(R.color.logo_color);
                holder.tv_categoryName.setTextColor(ContextCompat.getColor(context, R.color.white));

                RestaurantMenuDetailsActivity.ScrollToPosition(position);

//                restaurantMenuDetailsActivity.callByCategory(foodMenuMainDataList.get(position).getId());
                row_index=position;
                notifyDataSetChanged();

            }
        });
        if(row_index==position){
            holder.tv_categoryName.setBackgroundResource(R.color.logo_color);
            holder.tv_categoryName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tv_categoryName.setFocusable(true);
        }
        else
        {
            holder.tv_categoryName.setBackgroundResource(R.color.gray_border);
            holder.tv_categoryName.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return hashMap.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{


        TextView tv_categoryName;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);

            tv_categoryName=itemView.findViewById(R.id.tv_categoryName);

        }
    }

    public static void getData(int position){
        row_index = position;
    }
}


