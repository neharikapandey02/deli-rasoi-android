package com.example.desirasoi.activities.menu_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetTag {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private String message;

@SerializedName("tags")
    @Expose
    private ArrayList<TagData> tags;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TagData> getTags() {
        return tags;
    }

    public void setTags(ArrayList<TagData> tags) {
        this.tags = tags;
    }
}
