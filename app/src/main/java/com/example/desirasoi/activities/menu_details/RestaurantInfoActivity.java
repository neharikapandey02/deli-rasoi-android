package com.example.desirasoi.activities.menu_details;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.models.RestaurantListMainData;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestaurantInfoActivity extends AppCompatActivity {

    @BindView(R.id.backImage)
    ImageView backImage;

    @BindView(R.id.restNameTv)
    TextView restNameTv;

    @BindView(R.id.specialHourTv)
    TextView specialHourTv;

   @BindView(R.id.specialTv)
    TextView specialTv;

   @BindView(R.id.specilatyTv)
    TextView specilatyTv;

  @BindView(R.id.addressTv)
    TextView addressTv;

  @BindView(R.id.directionTv)
    TextView directionTv;

  @BindView(R.id.mondayTv)
    TextView mondayTv;
  @BindView(R.id.tuesdayTv)
    TextView tuesdayTv;

 @BindView(R.id.wednesdayTv)
    TextView wednesdayTv;

@BindView(R.id.thursdayTv)
    TextView thursdayTv;

@BindView(R.id.fridayTv)
    TextView fridayTv;
  @BindView(R.id.saturdayTv)
    TextView saturdayTv;

 @BindView(R.id.sundayTv)
    TextView sundayTv;

 @BindView(R.id.deliveryTimeTv)
    TextView deliveryTimeTv;

@BindView(R.id.takeAwayTimeTv)
    TextView takeAwayTimeTv;

@BindView(R.id.contactTv)
    TextView contactTv;

@BindView(R.id.deliveryChargesTv)
    TextView deliveryChargesTv;

@BindView(R.id.openSiteTv)
    TextView openSiteTv;

    TabItem restTbItem;
    TabItem sprestTbItem;

    TabItem delivertTbItem;
    TabItem spdelivertTbItem;
    TabItem restaurantTbItem;
    TabItem sprestaurantTbItem;
    TabLayout tabbar;
    TabLayout spTabBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_restaurant_info);
        ButterKnife.bind(this);

        handleView();
    }

    private void handleView() {
        restTbItem = findViewById(R.id.restTbItem);
        sprestTbItem = findViewById(R.id.sprestTbItem);
        delivertTbItem = findViewById(R.id.delivertTbItem);
        spdelivertTbItem = findViewById(R.id.spdelivertTbItem);
        restaurantTbItem = findViewById(R.id.restaurantTbItem);
        sprestaurantTbItem = findViewById(R.id.sprestaurantTbItem);
        tabbar = findViewById(R.id.tabbar);
        spTabBar = findViewById(R.id.spTabBar);
        RestaurantListMainData data;
        data = (RestaurantListMainData) getIntent().getSerializableExtra("rest_obj");

        if (data != null) {
            restNameTv.setText(data.getRestaurantName());
            specilatyTv.setText(data.getSpeciality());
            addressTv.setText(data.getAddress());
            deliveryTimeTv.setText(data.getFood_ready_time_delivery()+" minutes");
            takeAwayTimeTv.setText(data.getFood_ready_time_take_away()+" minutes");
            contactTv.setText(data.getContact());
            String charges =data.getDelivery_charge();
            charges = charges.replace(".", ",");
            deliveryChargesTv.setText( charges+getString(R.string.euro_sign));
            for (int i=0; i<data.getOpenDaysArrayList().size();i++){
                switch (data.getOpenDaysArrayList().get(i).getDay_slug()){
                    case "Mon":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            mondayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            mondayTv.setText("Close");
                        }
                        break;
                    }
                    case "Tue":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            tuesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time())+" - "+parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            tuesdayTv.setText("Close");
                        }
                        break;
                    }
                    case "Wed":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            wednesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time())+" - "+parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            wednesdayTv.setText("Close");
                        }
                        break;
                    }
                    case "Thu":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            thursdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time())+" - "+parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            thursdayTv.setText("Close");
                        }
                        break;
                    }
                    case "Fri":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            fridayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time())+" - "+parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            fridayTv.setText("Close");
                        }
                        break;
                    }
                    case "Sat":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            saturdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time())+" - "+parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            saturdayTv.setText("Close");
                        }
                        break;
                    }case "Sun":{
                        if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                            sundayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time())+" - "+parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                        }
                        else {
                            sundayTv.setText("Close");
                        }
                        break;
                    }
                }
            }

            if (data.getSpecialDaysArrayList().size()>0) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.getSpecialDaysArrayList().size(); i++) {
                    if (data.getSpecialDaysArrayList().get(i).getSpecial_status().equalsIgnoreCase("1")) {
                        sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) + " " +
                                parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_take_away_start_time()) + " - " +
                                parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_take_away_end_time()) + "\n");
                    } else {
                        sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) + " Closed\n");
                    }
                }
                specialHourTv.setText(sb.toString());
            }else {
                specialTv.setVisibility(View.GONE);
                spTabBar.setVisibility(View.GONE);
            }
}

        spTabBar.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (spTabBar.getSelectedTabPosition() == 0) {
                    StringBuilder sb = new StringBuilder();
                    for (int i=0;i<data.getSpecialDaysArrayList().size();i++) {
                        if (data.getSpecialDaysArrayList().get(i).getSpecial_status().equalsIgnoreCase("1")) {
                            sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                    parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) + " " +
                                    parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_take_away_start_time()) + " - " +
                                    parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_take_away_end_time())+"\n");

                        }
                        else {
                            sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                    parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) +" Closed\n");
                        }
                    }
                    specialHourTv.setText(sb.toString());
                } else if (spTabBar.getSelectedTabPosition() == 1){
                    StringBuilder sb = new StringBuilder();
                    for (int i=0;i<data.getSpecialDaysArrayList().size();i++) {
                        if (data.getSpecialDaysArrayList().get(i).getSpecial_status().equalsIgnoreCase("1")) {
                            sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                    parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) + " " +
                                    parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_delivery_start_time()) + " - " +
                                    parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_delivery_end_time())+"\n");

                        }
                        else {
                            sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                    parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) +" Closed\n");
                        }
                    }
                    specialHourTv.setText(sb.toString());
                }else {
                    StringBuilder sb = new StringBuilder();
                    for (int i=0;i<data.getSpecialDaysArrayList().size();i++) {
                        if (data.getSpecialDaysArrayList().get(i).getSpecial_status().equalsIgnoreCase("1")) {
                            sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                    parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) + " " +
                                    parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_start_time()) + " - " +
                                    parseDateToddMMyyyy(data.getSpecialDaysArrayList().get(i).getSpecial_hour_end_time())+"\n");
                        }
                        else {
                            sb.append(data.getSpecialDaysArrayList().get(i).getDay_name() + " " +
                                    parseDateToddMMyyyy1(data.getSpecialDaysArrayList().get(i).getSpecial_hour_date()) +" Closed\n");
                        }
                    }
                    specialHourTv.setText(sb.toString());                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        tabbar.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tabbar.getSelectedTabPosition() == 0) {
                    for (int i = 0; i < data.getOpenDaysArrayList().size(); i++) {
                        switch (data.getOpenDaysArrayList().get(i).getDay_slug()) {
                            case "Mon": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    mondayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    mondayTv.setText("Close");
                                }
                                break;
                            }
                            case "Tue": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    tuesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    tuesdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Wed": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    wednesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    wednesdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Thu": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    thursdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    thursdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Fri": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    fridayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    fridayTv.setText("Close");
                                }
                                break;
                            }
                            case "Sat": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    saturdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    saturdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Sun": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    sundayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getTake_away_end_time()));
                                }
                                else {
                                    sundayTv.setText("Close");
                                }
                                break;
                            }
                        }
                    }
                } else if (tabbar.getSelectedTabPosition() == 1){
                    for (int i = 0; i < data.getOpenDaysArrayList().size(); i++) {
                        switch (data.getOpenDaysArrayList().get(i).getDay_slug()) {
                            case "Mon": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    mondayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    mondayTv.setText("Close");
                                }
                                break;
                            }
                            case "Tue": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    tuesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    tuesdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Wed": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    wednesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    wednesdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Thu": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    thursdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    thursdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Fri": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    fridayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    fridayTv.setText("Close");
                                }
                                break;
                            }
                            case "Sat": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    saturdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    saturdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Sun": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    sundayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_start_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getDelivery_end_time()));
                                }
                                else {
                                    sundayTv.setText("Close");
                                }
                                break;
                            }
                        }

                    }
                }else {
                    for (int i = 0; i < data.getOpenDaysArrayList().size(); i++) {
                        switch (data.getOpenDaysArrayList().get(i).getDay_slug()) {
                            case "Mon": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    mondayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    mondayTv.setText("Close");
                                }
                                break;
                            }
                            case "Tue": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    tuesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    tuesdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Wed": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    wednesdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    wednesdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Thu": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    thursdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    thursdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Fri": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    fridayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    fridayTv.setText("Close");
                                }
                                break;
                            }
                            case "Sat": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    saturdayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    saturdayTv.setText("Close");
                                }
                                break;
                            }
                            case "Sun": {
                                if (data.getOpenDaysArrayList().get(i).getStatus().equalsIgnoreCase("1")) {
                                    sundayTv.setText(parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getOpen_time()) + " - " + parseDateToddMMyyyy(data.getOpenDaysArrayList().get(i).getClose_time()));
                                }
                                else {
                                    sundayTv.setText("Close");
                                }
                                break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        openSiteTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.delirasoi.fi"));
                startActivity(i);
            }
        });

        directionTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("geo:0,0?q="+data.getAddress()));
                startActivity(intent);
            }
        });

        contactTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+data.getContact()));
                startActivity(intent);
            }
        });


        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "H.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseDateToddMMyyyy1(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "d.MM.yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}