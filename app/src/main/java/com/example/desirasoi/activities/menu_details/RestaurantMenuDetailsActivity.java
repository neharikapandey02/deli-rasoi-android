package com.example.desirasoi.activities.menu_details;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.LoginActivity;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.MyCartActivity;
import com.example.desirasoi.activities.OrderSummaryActivity;
import com.example.desirasoi.activities.addaddress.CheckoutMain;
import com.example.desirasoi.activities.addaddress.SelectAddressActivity;
import com.example.desirasoi.activities.lunch_details.LunchDetailsActivity;
import com.example.desirasoi.activities.ui.reserved_table.AvailableDates;
import com.example.desirasoi.activities.ui.reserved_table.AvailableTiming;
import com.example.desirasoi.activities.ui.reserved_table.DateObject;
import com.example.desirasoi.activities.ui.reserved_table.ReservedTableFragment;
import com.example.desirasoi.activities.ui.reserved_table.TimeObject;
import com.example.desirasoi.adapters.FilterTagAdapter;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.CategoryListMain;
import com.example.desirasoi.models.CategoryListMainData;
import com.example.desirasoi.models.FoodMenuMain;
import com.example.desirasoi.models.FoodMenuMainData;
import com.example.desirasoi.models.OpenDays;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.models.RestaurantListMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

public class RestaurantMenuDetailsActivity extends AppCompatActivity {
//    @BindView(R.id.imgview_back)
//    ImageView imgview_back;

    @BindView(R.id.optionImg)
    ImageView optionImg;

  @BindView(R.id.backImage)
    ImageView backImage;


    static RecyclerView recyclerview_details;
    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerview_category)
    RecyclerView recyclerview_category;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    Spinner dateSpn;
    Spinner timeSpn;

    @BindView(R.id.restNameTv)
    TextView restNameTv;

    @BindView(R.id.filterTv)
    TextView filterTv;

    @BindView(R.id.specialityTv)
    TextView specialityTv;

    @BindView(R.id.estimateTimeTv)
    TextView estimateTimeTv;

    @BindView(R.id.estimateTimeChangeTv)
    TextView estimateTimeChangeTv;

    @BindView(R.id.timingTv)
    TextView timingTv;

    @BindView(R.id.searchEt)
    EditText searchEt;

    @BindView(R.id.closeTv)
    TextView closeTv;

    @BindView(R.id.lunchBtn)
    Button lunchBtn;

    ArrayList<AvailableDates> datesArrayList;
    ArrayList<AvailableTiming> timingArrayList;

    static TextView itemCountTv;
    static TextView itemPriceTv;
    static String cartId;
    TextView viewCartTv;
    String orderType = "1";
    int datePos = 0, timePos = 0;
    static RelativeLayout viewCartRl;
    String startTime = "", endTime = "";
    String date = "";
    RestaurantMenuDetailsAdapter  restaurantListAdapter;
    static CategoryListAdapter categoryListAdapter;
    private SharedPreferencesData sharedPreferencesData;
    String userId;
    ArrayList<TagData> tagArrayList = new ArrayList<>();
    SwipeRefreshLayout swipeRefresh;

    private List<FoodMenuMainData> foodMenuMainDataList;
    private List<CategoryListMainData> categoryListMainData;

    String restaurantId;

    @BindView(R.id.backdrop)
    ImageView backdrop;

    @BindView(R.id.moreInfoTv)
    TextView moreInfoTv;

    int tagStatus = 0;
    ArrayList<String> tagCheckedList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
//        getSupportActionBar().hide();
        setContentView(R.layout.activity_restaurant_menu_details);
        ButterKnife.bind(this);
        viewFinds();
    }



    private void viewFinds() {
        itemCountTv = findViewById(R.id.itemCountTv);
//        swipeRefresh = findViewById(R.id.swipeRefresh);
        itemPriceTv = findViewById(R.id.itemPriceTv);
        viewCartTv = findViewById(R.id.viewCartTv);
        viewCartRl = findViewById(R.id.viewCartRl);
        recyclerview_details = findViewById(R.id.recyclerview_details);

        RestaurantListMainData rest_obj = (RestaurantListMainData) getIntent().getSerializableExtra("rest_obj");

        viewCartTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("")) {
                    Intent intent = new Intent(RestaurantMenuDetailsActivity.this, LoginActivity.class);
                    finish();
                    startActivity(intent);
                } else {
                    if (NetworkUtil.checkNetworkStatus(RestaurantMenuDetailsActivity.this)) {
                        String oDate = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
                        String oTime = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().docheckout(getCheckoutCallback, userId, cartId, getIntent().getStringExtra("service_type"), orderType, oDate, oTime);
                    }
                }
            }
        });

        estimateTimeChangeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(RestaurantMenuDetailsActivity.this);
                dialog.setContentView(R.layout.advance_order_layout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                LinearLayout nowLL = dialog.findViewById(R.id.nowLL);
                LinearLayout futureLL = dialog.findViewById(R.id.futureLL);
                ImageView nowImg = dialog.findViewById(R.id.nowImg);
                ImageView futureImg = dialog.findViewById(R.id.futureImg);
                int stt = 1;

                if (rest_obj.getRestaurant_status().equalsIgnoreCase("Off")){
                    stt = 2;
                    nowImg.setVisibility(View.INVISIBLE);
                    nowLL.setVisibility(View.GONE);
                    futureImg.setVisibility(View.VISIBLE);
                }



                nowLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nowImg.setVisibility(View.VISIBLE);
                        futureImg.setVisibility(View.INVISIBLE);
                        orderType = "1";
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE, "");
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME, "");
//                        startActivity(new Intent(RestaurantMenuDetailsActivity.this, MyCartActivity.class));
                        estimateTimeTv.setText("As soon as possible");
                        dialog.dismiss();
                    }
                });

                futureLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nowImg.setVisibility(View.INVISIBLE);
                        futureImg.setVisibility(View.VISIBLE);

                        Dialog dialog1 = new Dialog(RestaurantMenuDetailsActivity.this);
                        dialog1.setContentView(R.layout.order_for_future_layout);
                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog1.setCancelable(false);
                        dateSpn = dialog1.findViewById(R.id.dateSpn);
                        timeSpn = dialog1.findViewById(R.id.timeSpn);
                        TextView continueBtn = dialog1.findViewById(R.id.continueBtn);
                        TextView closeBtn = dialog1.findViewById(R.id.closeBtn);


                        RetrofitHelper.getInstance().getOrderDates(datesCallback, restaurantId, getIntent().getStringExtra("service_type"));


                        timeSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                timePos = position;
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        dateSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                datePos = position;
                                if (NetworkUtil.checkNetworkStatus(RestaurantMenuDetailsActivity.this)) {
                                    RetrofitHelper.getInstance().getOrderTime(timesCallback,restaurantId, datesArrayList.get(position).getBook_date(),
                                            getIntent().getStringExtra("service_type"));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });




                        closeBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog1.dismiss();
                            }
                        });
                        Calendar cal = Calendar.getInstance();
//                        tv_time.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (!tv_date.getText().toString().equalsIgnoreCase("")) {
//                                    TimePickerDialog tpd = new TimePickerDialog(RestaurantMenuDetailsActivity.this, (view1, hourOfDay, minute) -> {
//                                        try {
//                                            Date time1 = new SimpleDateFormat("HH:mm:ss").parse(startTime);
//                                            Calendar calendar1 = Calendar.getInstance();
//                                            calendar1.setTime(time1);
//                                            calendar1.add(Calendar.DATE, 1);
//
//
//                                            Date time2 = new SimpleDateFormat("HH:mm:ss").parse(endTime);
//                                            Calendar calendar2 = Calendar.getInstance();
//                                            calendar2.setTime(time2);
//                                            calendar2.add(Calendar.DATE, 1);
//
//                                            String someRandomTime = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute) + ":00";
//                                            Date d = new SimpleDateFormat("HH:mm:ss").parse(someRandomTime);
//                                            Calendar calendar3 = Calendar.getInstance();
//                                            calendar3.setTime(d);
//                                            calendar3.add(Calendar.DATE, 1);
//
//                                            Date x = calendar3.getTime();
//                                            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
//                                                //checkes whether the current time is between 14:49:00 and 20:11:13.
//                                                tv_time.setText(String.format("%02d", hourOfDay) + "." + String.format("%02d", minute));
//
//                                                sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.B_TIME, String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
//
//                                            }
//                                            else {
//                                                tv_time.setText("");
//                                                Toast.makeText(RestaurantMenuDetailsActivity.this, "Restaurant only work from "+startTime+" to "+ endTime, Toast.LENGTH_LONG).show();
//                                            }
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//                                        //Toast.makeText(getContext(), String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute), Toast.LENGTH_SHORT).show();
//                                    }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), DateFormat.is24HourFormat(RestaurantMenuDetailsActivity.this));
//                                    tpd.show();
//                                }else {
//                                    Toast.makeText(RestaurantMenuDetailsActivity.this, "Please select date first.", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        });
//
//                        tv_date.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                DatePickerDialog dpd = new DatePickerDialog(RestaurantMenuDetailsActivity.this, (view1, year, month, dayOfMonth) -> {
//                                    SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
//                                    Date date = null;
//                                    try {
//                                        date = inFormat.parse(String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
//                                    } catch (ParseException e) {
//                                        e.printStackTrace();
//                                    }
//                                    SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
//                                    String goal = outFormat.format(date);
//                                    ArrayList<OpenDays> arrayList = new ArrayList<>();
//                                    arrayList.addAll(rest_obj.getOpenDaysArrayList());
//                                    for (int i=0;i<arrayList.size();i++){
//                                        if (goal.equalsIgnoreCase(rest_obj.getOpenDaysArrayList().get(i).getDay_name())){
//                                            if (getIntent().getStringExtra("service_type").equalsIgnoreCase("1")) {
//                                                startTime = rest_obj.getOpenDaysArrayList().get(i).getDelivery_start_time();
//                                                endTime = rest_obj.getOpenDaysArrayList().get(i).getDelivery_end_time();
//                                            }
//                                            else if (getIntent().getStringExtra("service_type").equalsIgnoreCase("2")){
//                                                startTime = rest_obj.getOpenDaysArrayList().get(i).getTake_away_start_time();
//                                                endTime = rest_obj.getOpenDaysArrayList().get(i).getTake_away_end_time();
//                                            }
//                                            Log.e("ssssssss", startTime);
//                                            Log.e("ssssssss", endTime);
//                                            sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.B_DATE, String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
//                                            tv_date.setText(String.format("%d", dayOfMonth) + "." + String.format("%02d", month + 1) + "." + String.format("%02d", year));
//                                            return;
//                                        }else if (i==arrayList.size()-1){
//                                            tv_date.setText("");
//                                            Toast.makeText(RestaurantMenuDetailsActivity.this, "Restaurent is not available on this day.", Toast.LENGTH_SHORT).show();
//                                        }
////                                        Log.e("dkfd", i+", "+arrayList.size());
//                                    }
//
//                                    //Toast.makeText(getContext(), String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth), Toast.LENGTH_SHORT).show();
//                                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
//
//                                dpd.show();
//                            }
//                        });

                        continueBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!dateSpn.getSelectedItem().toString().equalsIgnoreCase("")&& !timeSpn.getSelectedItem().toString().equalsIgnoreCase("")){
//                                    startActivity(new Intent(RestaurantMenuDetailsActivity.this, MyCartActivity.class));
                                    orderType = "2";
                                    estimateTimeTv.setText("Schedule Order "+datesArrayList.get(datePos).getFormat_date()+" "+timingArrayList.get(timePos).getFormatted_time());
                                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE, datesArrayList.get(datePos).getBook_date());
                                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME, timingArrayList.get(timePos).getOrder_time()+":00");
                                    dialog1.dismiss();
                                    dialog.dismiss();
                                }
                                else {
                                    Toast.makeText(RestaurantMenuDetailsActivity.this, "Please select both date and time.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        dialog1.show();
                    }
                });
                dialog.show();
            }
        });

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackDialog(); }
        });

        moreInfoTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RestaurantMenuDetailsActivity.this,
                        RestaurantInfoActivity.class);
                intent.putExtra("rest_obj", (RestaurantListMainData) getIntent().getSerializableExtra("rest_obj"));
                startActivity(new Intent(intent));
            }
        });

        optionImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(RestaurantMenuDetailsActivity.this, optionImg);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.restaurant_menu_option, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.place_info:
                                Intent intent = new Intent(RestaurantMenuDetailsActivity.this,
                                        RestaurantInfoActivity.class);
                                intent.putExtra("rest_obj", (RestaurantListMainData) getIntent().getSerializableExtra("rest_obj"));
                                startActivity(new Intent(intent));
                                break;

                            case R.id.share:
                                try {
                                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                                    shareIntent.setType("text/plain");
                                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Deli Rasoi");
                                    String shareMessage= "\nLet me recommend you this application\n\n";
                                    shareMessage = shareMessage + "https://www.delirasoi.fi ";
                                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                                } catch(Exception e) {
                                    //e.toString();
                                }
                                break;
                        }
                        return true;
                    }
                });

                popup.show(); //showing popup menu
            }
        });
        restaurantId=getIntent().getStringExtra(Constants.ID_RESTAURANT);

        if (getIntent().getStringExtra("resturant_status").equalsIgnoreCase("Off")){
            closeTv.setVisibility(View.VISIBLE);
            estimateTimeTv.setText("Schedule for later");

        }
        else {
            if (getIntent().getStringExtra("service_type").equalsIgnoreCase("1")){

                estimateTimeTv.setText("Delivery in "+rest_obj.getFood_ready_time_delivery()+" minutes");
            }
            else {
                estimateTimeTv.setText("Take away in "+rest_obj.getFood_ready_time_delivery()+" minutes");
            }
            closeTv.setVisibility(View.GONE);
        }
        try {
            String qty = getIntent().getStringExtra("cart_qty");
            if (!qty.equalsIgnoreCase("0")){
                viewCartRl.setVisibility(View.VISIBLE);
                itemCountTv.setText(qty+ " item");
                String price = getIntent().getStringExtra("cart_amt");
                price = price.replace(".", ",");
                itemPriceTv.setText(price + getResources().getString(R.string.euro_sign));
                cartId = getIntent().getStringExtra("cart_id");
            }
            else {
                viewCartRl.setVisibility(View.GONE);

            }


//            Log.e("dkfjd", "kk"+rest_obj.getOpenTime());
//            try {
//                    timingTv.setText(parseDateToddMMyyyy(rest_obj.getOpenTime()) + " - " +
//                            parseDateToddMMyyyy(rest_obj.getCloseTime()));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }



        } catch (Exception e) {
            viewCartRl.setVisibility(View.GONE);
            e.printStackTrace();
        }



        specialityTv.setText(rest_obj.getSpeciality());
        try {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("EEEE");
            String formattedDate = df.format(c);
            ArrayList<OpenDays> arrayList = new ArrayList<>();
            arrayList.addAll(rest_obj.getOpenDaysArrayList());
            for (int i=0;i<arrayList.size();i++) {
                if (formattedDate.equalsIgnoreCase(rest_obj.getOpenDaysArrayList().get(i).getDay_name())) {
                    String startTime = rest_obj.getOpenDaysArrayList().get(i).getOpen_time();
                    String endTime = rest_obj.getOpenDaysArrayList().get(i).getClose_time();
                    timingTv.setText("Open today: "+parseDateToddMMyyyy(startTime)+" - "+parseDateToddMMyyyy(endTime));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        restNameTv.setText(getIntent().getStringExtra("rest_name"));

        Picasso.with(RestaurantMenuDetailsActivity.this)
                .load(getIntent().getStringExtra("res_image"))
                .error(R.drawable.restaurant_list)
                .fit()
                .into(backdrop);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview_details.setLayoutManager(layoutManager);
//        recyclerview_details.setHasFixedSize(true);

        recyclerview_category.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        recyclerview_category.setHasFixedSize(true);

        lunchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RestaurantMenuDetailsActivity.this, LunchDetailsActivity.class);
                intent.putExtra(Constants.ID_RESTAURANT, restaurantId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

//        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (swipeRefresh.isRefreshing()){
//                    swipeRefresh.setRefreshing(false);
//                }
//                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                    sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
//                    userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
//                    progressBar.setVisibility(View.VISIBLE);
////            RetrofitHelper.getInstance().doCategoryList(categoryCallback, restaurantId,
////                    getIntent().getStringExtra("service_type"));
//
//
//                    String b_date = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
//                    String b_time = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
//                    if (!b_date.equalsIgnoreCase("")){
//                        date = b_date;
//                    }
//                    else {
//                        Date c = Calendar.getInstance().getTime();
//
//                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                        date = df.format(c);
//                    }
//                    RetrofitHelper.getInstance().doFoodMenuList(menuListCallback,userId,restaurantId,
//                            sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
//                            date, getIntent().getStringExtra("service_type"), "");
//
//
//                } else {
//                    Snackbar.make(recyclerview_details, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                }
//            }
//        });

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
            userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
            progressBar.setVisibility(View.VISIBLE);
//            RetrofitHelper.getInstance().doCategoryList(categoryCallback, restaurantId,
//                    getIntent().getStringExtra("service_type"));


            String b_date = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
            String b_time = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
            if (!b_date.equalsIgnoreCase("")){
                date = b_date;
            }
            else {
                Date c = Calendar.getInstance().getTime();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                date = df.format(c);
            }
            RetrofitHelper.getInstance().doFoodMenuList(menuListCallback,userId,restaurantId,
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
                    date, getIntent().getStringExtra("service_type"), "");


        } else {
            Snackbar.make(recyclerview_details, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }


recyclerview_details.addOnScrollListener(new RecyclerView.OnScrollListener() {
    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int scrollPosition = layoutManager.findFirstVisibleItemPosition();
        categoryListAdapter.getData(scrollPosition);
        categoryListAdapter.notifyDataSetChanged();
        if (scrollPosition>0) {
            recyclerview_category.scrollToPosition(scrollPosition + 1);
        }else {
            recyclerview_category.scrollToPosition(scrollPosition);
        }
    }
});
    }


    Callback<CheckoutMain> getCheckoutCallback = new Callback<CheckoutMain>() {
        @Override
        public void onResponse(Call<CheckoutMain> call, Response<CheckoutMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("")) {
                        Intent intent = new Intent(RestaurantMenuDetailsActivity.this, LoginActivity.class);
                        finish();
                        startActivity(intent);
                    } else {
                        String oDate = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_DATE);
                        String oTime = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.B_TIME);
                        Intent intent=new Intent(getApplicationContext(), OrderSummaryActivity.class);
                        intent.putExtra(Constants.CHECKOUT_ID, cartId);
                        intent.putExtra("service_type", getIntent().getStringExtra("service_type"));
                        intent.putExtra("order_type", orderType);
                        intent.putExtra("o_date", oDate);
                        intent.putExtra("o_time", oTime);
                        intent.putExtra("res_lat", getIntent().getStringExtra("res_lat"));
                        intent.putExtra("res_lng", getIntent().getStringExtra("res_lng"));
                        startActivity(intent);
                    }
                } else {
                    Snackbar.make(recyclerview_details, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<CheckoutMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<GetTag> tagCallback = new Callback<GetTag>() {
        @Override
        public void onResponse(Call<GetTag> call, Response<GetTag> response) {
            try {
                if (response.body().isSuccess()){
                    tagStatus = 1;
                    tagArrayList = response.body().getTags();

//                    try {
//                        if (tagArrayList.size()>0){
//                            String sb ="";
//                            for (int i=0;i<tagArrayList.size();i++){
//                                String initials = "";
//                                for (String s : tagArrayList.get(i).getName().split(" ")) {
//                                    initials = initials+s.charAt(0);
//                                }
//                                sb = " ("+initials+") ";
//                                TagData data = new TagData();
//                                data.setId(tagArrayList.get(i).getId());
//                                data.setName(tagArrayList.get(i).getName()+sb.toString());
//                                data.setName_fi(tagArrayList.get(i).getName_fi());
//                                tagArrayList.add(i, data);
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<GetTag> call, Throwable t) {

        }
    };

    public static void ScrollToPosition(int position){
        recyclerview_details.scrollToPosition(position);
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "HH.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    class SelectDatesAdapter extends ArrayAdapter<String> {
        public SelectDatesAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(datesArrayList.get(position).getFormat_date());
            return mySpinner;
        }
    }

    class SelectTimeAdapter extends ArrayAdapter<String> {
        public SelectTimeAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
    }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(timingArrayList.get(position).getFormatted_time());
            return mySpinner;
        }
    }

    Callback<TimeObject> timesCallback = new Callback<TimeObject>() {
        @Override
        public void onResponse(Call<TimeObject> call, Response<TimeObject> response) {
            try {
                if (response.body().isSuccess()) {
                    timePos = 0;
                    timingArrayList = new ArrayList<>();
                    timingArrayList = response.body().getArrayList();
                    SelectTimeAdapter selectTableAdapter = new SelectTimeAdapter(RestaurantMenuDetailsActivity.this, R.layout.item_spinner_text_home, timingArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    timeSpn.setAdapter(selectTableAdapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<TimeObject> call, Throwable t) {

        }
    };

    Callback<DateObject> datesCallback = new Callback<DateObject>() {
        @Override
        public void onResponse(Call<DateObject> call, Response<DateObject> response) {
            try {
                progressBar.setVisibility(View.GONE);
                if (response.body().isSuccess()) {
                    datePos = 0;
                    datesArrayList = new ArrayList<>();
                    datesArrayList = response.body().getArrayList();
                    SelectDatesAdapter selectTableAdapter = new SelectDatesAdapter(RestaurantMenuDetailsActivity.this, R.layout.item_spinner_text_home, datesArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    //Setting the ArrayAdapter data on the Spinner
                    dateSpn.setAdapter(selectTableAdapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DateObject> call, Throwable t) {

        }
    };



    Callback<FoodMenuMain> menuListCallback = new Callback<FoodMenuMain>() {
        @Override
        public void onResponse(Call<FoodMenuMain> call, Response<FoodMenuMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {

                        foodMenuMainDataList=new ArrayList<>();
                        foodMenuMainDataList.clear();
                        foodMenuMainDataList.addAll(response.body().getData());

                        LinkedHashMap<String, ArrayList<FoodMenuMainData>> hashMap = new LinkedHashMap<>();
                        for (int i=0;i<response.body().getData().size();i++){

                            ArrayList<FoodMenuMainData> arrayList = new ArrayList<>();
                            hashMap.put(response.body().getData().get(i).getCategory(), arrayList);
                        }
                        for (int i=0;i<hashMap.size();i++){
//                            if (!hashMap.keySet().toArray()[i].toString().equalsIgnoreCase("Lunch")) {
                                ArrayList<FoodMenuMainData> arrayList = new ArrayList<>();
                                for (int j = 0; j < response.body().getData().size(); j++) {

                                    if (hashMap.keySet().toArray()[i].toString().equalsIgnoreCase(
                                            response.body().getData().get(j).getCategory()
                                    )) {
                                        arrayList.add(response.body().getData().get(j));
                                    }
                                }
                                hashMap.put(hashMap.keySet().toArray()[i].toString(), arrayList);
//                            }
                        }


                        categoryListAdapter= new CategoryListAdapter(getApplicationContext(),hashMap,RestaurantMenuDetailsActivity.this);
                        recyclerview_category.setAdapter(categoryListAdapter);

                        RestaurantMenuDetailsAdapter2 adapter2 = new RestaurantMenuDetailsAdapter2(RestaurantMenuDetailsActivity.this, hashMap, restaurantId, progressBar);
                        recyclerview_details.setAdapter(adapter2);

                        searchEt.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                try {
                                    if (!searchEt.getText().toString().equalsIgnoreCase("")) {
                                        LinkedHashMap<String, ArrayList<FoodMenuMainData>> hashMap1 = new LinkedHashMap<>();

                                        for (int i=0;i<response.body().getData().size();i++){
                                            if (response.body().getData().get(i).getMessage() == null) {
                                                if (response.body().getData().get(i).getMenu().contains(searchEt.getText().toString().trim())) {
                                                    ArrayList<FoodMenuMainData> arrayList = new ArrayList<>();
                                                    hashMap1.put(response.body().getData().get(i).getCategory(), arrayList);
                                                }
                                            }
                                        }

                                        for (int i=0;i<hashMap1.size();i++){
                                            ArrayList<FoodMenuMainData> arrayList = new ArrayList<>();
                                            for (int j = 0;j<response.body().getData().size();j++) {

                                                if (hashMap1.keySet().toArray()[i].toString().equalsIgnoreCase(
                                                        response.body().getData().get(j).getCategory()
                                                )){
                                                    if (response.body().getData().get(j).getMenu().contains(searchEt.getText().toString().trim())) {
                                                        arrayList.add(response.body().getData().get(j));
                                                    }
                                                }
                                            }
                                            hashMap1.put(hashMap1.keySet().toArray()[i].toString(), arrayList);
                                        }


                                        adapter2.updateList(hashMap1);
                                        categoryListAdapter.updateList(hashMap1);

                                    }
                                    else {
                                        adapter2.updateList(hashMap);
                                        categoryListAdapter.updateList(hashMap);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        if (tagStatus ==0){
                            RetrofitHelper.getInstance().getTagList(tagCallback, userId);
                        }
                        filterTv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Dialog dialog = new Dialog(RestaurantMenuDetailsActivity.this);
                                dialog.setContentView(R.layout.filter_tag_layout);
                                RecyclerView listView = dialog.findViewById(R.id.listView);
                                TextView applyBtn = dialog.findViewById(R.id.applyBtn);
                                listView.setLayoutManager(new LinearLayoutManager(RestaurantMenuDetailsActivity.this,LinearLayoutManager.VERTICAL,false));
                                listView.setHasFixedSize(true);
                                FilterTagAdapter adapter = new FilterTagAdapter(RestaurantMenuDetailsActivity.this, tagArrayList, tagCheckedList);
                                listView.setAdapter(adapter);

                                applyBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        tagCheckedList = FilterTagAdapter.idList;
                                    if(FilterTagAdapter.idList.size()>0) {
                                        String listString = "";
                                        tagCheckedList = FilterTagAdapter.idList;
                                        for (String s : FilterTagAdapter.idList)
                                        {
                                            listString += s;
                                        }
                                        RetrofitHelper.getInstance().doFoodMenuList(menuListCallback, userId, restaurantId,
                                                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID),
                                                date, getIntent().getStringExtra("service_type"), listString);
//                                            Toast.makeText(RestaurantMenuDetailsActivity.this, ""+FilterTagAdapter.idList.toString(), Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                    }else {
                                        RetrofitHelper.getInstance().doFoodMenuList(menuListCallback, userId, restaurantId,
                                                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID),
                                                date, getIntent().getStringExtra("service_type"), "");
                                    dialog.dismiss();
                                    }
                                    }
                                });

                                dialog.show();
                            }
                        });

//                        restaurantListAdapter= new RestaurantMenuDetailsAdapter(getApplicationContext(),foodMenuMainDataList,restaurantId,progressBar);
//                        recyclerview_details.setAdapter(restaurantListAdapter);

                    } else {
                        foodMenuMainDataList = new ArrayList<>();
                        foodMenuMainDataList.clear();
//                        restaurantListAdapter= new RestaurantMenuDetailsAdapter(getApplicationContext(),foodMenuMainDataList,restaurantId,progressBar);
//                        recyclerview_details.setAdapter(restaurantListAdapter);
                        Snackbar.make(recyclerview_details, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<FoodMenuMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    Callback<CategoryListMain> categoryCallback = new Callback<CategoryListMain>() {
        @Override
        public void onResponse(Call<CategoryListMain> call, Response<CategoryListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {

                        categoryListMainData=new ArrayList<>();
                        categoryListMainData.clear();
                        categoryListMainData.addAll(response.body().getData());
//                        categoryListAdapter= new CategoryListAdapter(getApplicationContext(),categoryListMainData,RestaurantMenuDetailsActivity.this);
//                        recyclerview_category.setAdapter(categoryListAdapter);

                    } else {
                        Snackbar.make(recyclerview_details, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<CategoryListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };



    public void onBackDialog(){

        if (viewCartRl.getVisibility() == View.VISIBLE) {
            Dialog dialog = new Dialog(RestaurantMenuDetailsActivity.this);
            dialog.setContentView(R.layout.back_pressed_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            TextView leaveTv = dialog.findViewById(R.id.leaveTv);
            TextView cancelTv = dialog.findViewById(R.id.cancelTv);

            leaveTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RestaurantMenuDetailsActivity.super.onBackPressed();
                }
            });
            cancelTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
        else {
            RestaurantMenuDetailsActivity.super.onBackPressed();
        }
    }

//    @OnClick(R.id.imgview_back)
//    public void backClicked(View view){
//        onBackDialog();
//    }

    @Override
    public void onBackPressed(){

       onBackDialog();
    }

   public void callByCategory(String categoryId){

       if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
           sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
           String userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
           progressBar.setVisibility(View.VISIBLE);
          // RetrofitHelper.getInstance().doCategoryList(categoryCallback);
           RetrofitHelper.getInstance().doFoodMenuByCatList(menuListCallback,userId,restaurantId,categoryId,
                   sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID
                   ), date, getIntent().getStringExtra("service_type"));

       } else {
           Snackbar.make(recyclerview_details, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
       }
   }
}
