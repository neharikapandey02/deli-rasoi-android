package com.example.desirasoi.activities.menu_details;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.desirasoi.R;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddMinusMain;
import com.example.desirasoi.models.FoodMenuMain;
import com.example.desirasoi.models.FoodMenuMainData;
import com.example.desirasoi.models.RestaurantListMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantMenuDetailsAdapter extends RecyclerView.Adapter<RestaurantMenuDetailsAdapter.RestaurantList> {
    private Context context;
    private List<FoodMenuMainData> foodMenuMainDataList;
    SharedPreferencesData sharedPreferencesData;
    String userId;
    String restaurantId;
    private int position;
    private String quantity;
    ProgressBar progressBar;
    private LayoutInflater inflater;
    TextView tv_count;
    public RestaurantMenuDetailsAdapter(Context context,List<FoodMenuMainData> foodMenuMainDataList,String restaurantId,ProgressBar progressBar){
        this.context=context;
        this.foodMenuMainDataList=foodMenuMainDataList;
         sharedPreferencesData=new SharedPreferencesData(context);
         userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
         this.restaurantId=restaurantId;
         this.progressBar=progressBar;
    }


    @NonNull
    @Override
    public RestaurantMenuDetailsAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view= inflater.inflate(R.layout.item_menu_list_details,null);
        RestaurantMenuDetailsAdapter.RestaurantList restaurantList=new RestaurantMenuDetailsAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantMenuDetailsAdapter.RestaurantList holder, int position) {
        try {
            if (foodMenuMainDataList.get(position).getImage()!=null || !foodMenuMainDataList.get(position).getImage().equalsIgnoreCase("")){
                Picasso.with(context)
                        .load(foodMenuMainDataList.get(position).getImage())
                        .into(holder.imageView_restaurant);
                holder.imageView_restaurant.setVisibility(View.VISIBLE);
            }else {
                holder.imageView_restaurant.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (holder.image2.getVisibility() == View.VISIBLE){
            holder.imageView_restaurant.setVisibility(View.GONE);
        }

        final int[] m = {0};
        Log.e("gfd", ""+m[0]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m[0] ==0){
                    try {
                        if (foodMenuMainDataList.get(position).getImage()!=null || !foodMenuMainDataList.get(position).getImage().equalsIgnoreCase("")){
                            Picasso.with(context)
                                    .load(foodMenuMainDataList.get(position).getImage())
                                    .into(holder.image2);
                            holder.image2.setVisibility(View.VISIBLE);
                        }else {
                            holder.image2.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    holder.categoryDescTv.setVisibility(View.VISIBLE);
//                    holder.layout_LinearAddMinus.setVisibility(View.VISIBLE);
                    holder.imageView_restaurant.setVisibility(View.GONE);
                    m[0] = 1;
                }else {
                    try {
                        if (foodMenuMainDataList.get(position).getImage()!=null || !foodMenuMainDataList.get(position).getImage().equalsIgnoreCase("")){
                            Picasso.with(context)
                                    .load(foodMenuMainDataList.get(position).getImage())
                                    .into(holder.imageView_restaurant);
                            holder.imageView_restaurant.setVisibility(View.VISIBLE);
                        }else {
                            holder.imageView_restaurant.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    m[0] = 0;
//                    holder.layout_LinearAddMinus.setVisibility(View.GONE);
//                    holder.categoryDescTv.setVisibility(View.GONE);
                    holder.image2.setVisibility(View.GONE);
                }

            }
        });
//        holder.imageView_restaurant.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
//                View mView = inflater.inflate(R.layout.dialog_custom_image_preview, null);
//                PhotoView photoView = mView.findViewById(R.id.imageView);
//                Glide.with(context).load(foodMenuMainDataList.get(position).getImage()
//                ).into(photoView);
//                mBuilder.setView(mView);
//                AlertDialog mDialog = mBuilder.create();
//                mDialog.show();
//            }
//        });

        String price = foodMenuMainDataList.get(position).getPrice();
        price = price.replace(".", ",");

        holder.tv_price.setText(price+context.getResources().getString(R.string.euro_sign));
        holder.categoryNameTv.setText(foodMenuMainDataList.get(position).getCategory());
        holder.categoryDescTv.setText(Html.fromHtml(foodMenuMainDataList.get(position).getDescription()));
        holder.tv_menuName.setText(foodMenuMainDataList.get(position).getMenu());
        holder.tv_quantity.setText(foodMenuMainDataList.get(position).getCartQuantity()+"");
        ArrayList<String> tagList = foodMenuMainDataList.get(position).getTags();
        try {
            if (tagList.size()>0){
                holder.tagTv.setVisibility(View.VISIBLE);
                StringBuilder sb =new StringBuilder();
                for (int i=0;i<tagList.size();i++){
                    StringBuilder initials = new StringBuilder();
                    for (String s : tagList.get(i).split(" ")) {
                        initials.append(s.charAt(0));
                    }
                    sb.append("("+initials+") ");
                }
                holder.tagTv.setText(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.addLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
//                    position=getAdapterPosition();

                    progressBar.setVisibility(View.VISIBLE);
                    quantity=(foodMenuMainDataList.get(position).getCartQuantity()+1)+"";
                    RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,foodMenuMainDataList.get(position).getId(),quantity,
                            sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));
                    holder.imageView_restaurant.setVisibility(View.GONE);
                } else {
                    Snackbar.make(holder.imageView_restaurant, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return foodMenuMainDataList.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{

    ImageView imageView_restaurant, image2;
    TextView tv_menuName,tv_quantity,tv_price, categoryNameTv, categoryDescTv, tagTv;
    RatingBar ratingBar_menu;
    LinearLayout addLinearLayout,minusLinearLayout,layout_LinearAddMinus;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            imageView_restaurant=itemView.findViewById(R.id.imageView_restaurant);
            image2=itemView.findViewById(R.id.image2);
            tv_menuName=itemView.findViewById(R.id.tv_menuName);
            tv_price=itemView.findViewById(R.id.tv_price);
            categoryNameTv=itemView.findViewById(R.id.categoryNameTv);
            categoryDescTv=itemView.findViewById(R.id.categoryDescTv);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            addLinearLayout=itemView.findViewById(R.id.addLinearLayout);
            minusLinearLayout=itemView.findViewById(R.id.minusLinearLayout);
            layout_LinearAddMinus=itemView.findViewById(R.id.layout_LinearAddMinus);
            tagTv=itemView.findViewById(R.id.tagTv);
            tv_count=tv_quantity;
            minusLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        if (foodMenuMainDataList.get(getAdapterPosition()).getCartQuantity()!=0){
                            progressBar.setVisibility(View.VISIBLE);
                            position=getAdapterPosition();
                            quantity=(foodMenuMainDataList.get(position).getCartQuantity()-1)+"";
                            RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,foodMenuMainDataList.get(getAdapterPosition()).getId(),quantity,
                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));

                        }

                    } else {
                        Snackbar.make(imageView_restaurant, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

        }
    }
    Callback<AddMinusMain> addItemCallback = new Callback<AddMinusMain>() {
        @Override
        public void onResponse(Call<AddMinusMain> call, Response<AddMinusMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                //if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        FoodMenuMainData foodMenuMainData=new FoodMenuMainData();
                        foodMenuMainData.setId(foodMenuMainDataList.get(position).getId());
                        foodMenuMainData.setMenu(foodMenuMainDataList.get(position).getMenu());
                        foodMenuMainData.setMenuType(foodMenuMainDataList.get(position).getMenuType());
                        foodMenuMainData.setImage(foodMenuMainDataList.get(position).getImage());
                        foodMenuMainData.setDescription(foodMenuMainDataList.get(position).getDescription());
                        foodMenuMainData.setPrice(foodMenuMainDataList.get(position).getPrice());
                        foodMenuMainData.setStatus(foodMenuMainDataList.get(position).getStatus());
                        foodMenuMainData.setCreatedAt(foodMenuMainDataList.get(position).getCreatedAt());
                        foodMenuMainData.setUpdatedAt(foodMenuMainDataList.get(position).getUpdatedAt());
                        foodMenuMainData.setCategory(foodMenuMainDataList.get(position).getCategory());
                        foodMenuMainData.setCartQuantity(Integer.parseInt(quantity));
                        foodMenuMainDataList.set(position,foodMenuMainData);
                        try {
                            if (response.body().getCart().getTotalItems() != null) {
                                RestaurantMenuDetailsActivity.itemCountTv.setText(response.body().getCart().getTotalItems() + " item");
                                RestaurantMenuDetailsActivity.cartId = response.body().getCart().getId();
                                String price = response.body().getCart().getCartAmount();
                                price = price.replace(".", ",");
                                RestaurantMenuDetailsActivity.itemPriceTv.setText(price+context.getResources().getString(R.string.euro_sign));
                                RestaurantMenuDetailsActivity.viewCartRl.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            RestaurantMenuDetailsActivity.viewCartRl.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                        notifyDataSetChanged();
                        Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
               // }
            }
        }

        @Override
        public void onFailure(Call<AddMinusMain> call, Throwable t) {
         progressBar.setVisibility(View.GONE);

        }
    };
}
