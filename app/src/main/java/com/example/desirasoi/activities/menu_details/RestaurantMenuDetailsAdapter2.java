package com.example.desirasoi.activities.menu_details;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.models.FoodMenuMainData;

import java.util.ArrayList;
import java.util.HashMap;


public class RestaurantMenuDetailsAdapter2 extends RecyclerView.Adapter<RestaurantMenuDetailsAdapter2.RestaurantList> {

    HashMap<String, ArrayList<FoodMenuMainData>> hashMap;
    Context context;
    String restaurantId;
    ProgressBar progressBar;

    RestaurantMenuDetailsAdapter2(Context context, HashMap<String, ArrayList<FoodMenuMainData>> hashMap, String restaurantId, ProgressBar progressBar){
        this.context = context;
        this.hashMap = hashMap;
        this.progressBar = progressBar;
        this.restaurantId = restaurantId;
    }

    public void updateList(HashMap<String, ArrayList<FoodMenuMainData>> hashMap){
        this.hashMap = hashMap;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.food_menu_new_layout,null);
        RestaurantMenuDetailsAdapter2.RestaurantList restaurantList=new RestaurantMenuDetailsAdapter2.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantList holder, int position) {

        holder.categoryTv.setText(hashMap.keySet().toArray()[position].toString());
        try {
            holder.desTv.setText(Html.fromHtml(hashMap.get(hashMap.keySet().toArray()[position].toString()).get(0).getCategory_description()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (hashMap.keySet().toArray()[position].toString().equalsIgnoreCase("Lunch")){
            if (hashMap.get(hashMap.keySet().toArray()[position].toString()).get(0).getMessage() != null &&
                    !hashMap.get(hashMap.keySet().toArray()[position].toString()).get(0).getMessage().equalsIgnoreCase("")){
                holder.messageTv.setMovementMethod(LinkMovementMethod.getInstance());
                holder.messageTv.setVisibility(View.VISIBLE);
                holder.messageTv.setText(Html.fromHtml(hashMap.get(hashMap.keySet().toArray()[position].toString()).get(0).getMessage()));
                holder.recycleView.setVisibility(View.GONE);
            }else {
                holder.messageTv.setVisibility(View.GONE);
                holder.recycleView.setVisibility(View.VISIBLE);
                RestaurantMenuDetailsAdapter restaurantListAdapter = new RestaurantMenuDetailsAdapter(context, hashMap.get(hashMap.keySet().toArray()[position].toString()), restaurantId, progressBar);
                holder.recycleView.setAdapter(restaurantListAdapter);
            }
        }else {
            holder.messageTv.setVisibility(View.GONE);
            holder.recycleView.setVisibility(View.VISIBLE);
            RestaurantMenuDetailsAdapter restaurantListAdapter = new RestaurantMenuDetailsAdapter(context, hashMap.get(hashMap.keySet().toArray()[position].toString()), restaurantId, progressBar);
            holder.recycleView.setAdapter(restaurantListAdapter);
        }
//        RestaurantMenuDetailsActivity.getData(position);

    }

    @Override
    public int getItemCount() {
        return hashMap.size();
    }

    public class RestaurantList extends RecyclerView.ViewHolder{

        TextView categoryTv, desTv, messageTv;
        RecyclerView recycleView;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            categoryTv = itemView.findViewById(R.id.categoryTv);
            desTv = itemView.findViewById(R.id.desTv);
            messageTv = itemView.findViewById(R.id.messageTv);
            recycleView = itemView.findViewById(R.id.recycleView);
            recycleView.setLayoutManager(new GridLayoutManager(context, 1));
            recycleView.setHasFixedSize(true);
            recycleView.setNestedScrollingEnabled(false);
        }
    }
}
