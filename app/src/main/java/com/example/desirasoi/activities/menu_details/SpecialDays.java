package com.example.desirasoi.activities.menu_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SpecialDays implements Serializable {

    @SerializedName("day_name")
    @Expose
    private String day_name;

@SerializedName("special_hour_date")
    @Expose
    private String special_hour_date;

@SerializedName("special_hour_start_time")
    @Expose
    private String special_hour_start_time;

@SerializedName("special_hour_end_time")
    @Expose
    private String special_hour_end_time;

@SerializedName("special_hour_lunch_start_time")
    @Expose
    private String special_hour_lunch_start_time;

@SerializedName("special_hour_lunch_end_time")
    @Expose
    private String special_hour_lunch_end_time;

    @SerializedName("special_hour_take_away_start_time")
    @Expose
    private String special_hour_take_away_start_time;

    @SerializedName("special_hour_take_away_end_time")
    @Expose
    private String special_hour_take_away_end_time;

    @SerializedName("special_hour_delivery_start_time")
    @Expose
    private String special_hour_delivery_start_time;

    @SerializedName("special_hour_delivery_end_time")
    @Expose
    private String special_hour_delivery_end_time;

  @SerializedName("special_status")
    @Expose
    private String special_status;

    public String getSpecial_status() {
        return special_status;
    }

    public void setSpecial_status(String special_status) {
        this.special_status = special_status;
    }

    public String getDay_name() {
        return day_name;
    }

    public void setDay_name(String day_name) {
        this.day_name = day_name;
    }

    public String getSpecial_hour_date() {
        return special_hour_date;
    }

    public void setSpecial_hour_date(String special_hour_date) {
        this.special_hour_date = special_hour_date;
    }

    public String getSpecial_hour_start_time() {
        return special_hour_start_time;
    }

    public void setSpecial_hour_start_time(String special_hour_start_time) {
        this.special_hour_start_time = special_hour_start_time;
    }

    public String getSpecial_hour_end_time() {
        return special_hour_end_time;
    }

    public void setSpecial_hour_end_time(String special_hour_end_time) {
        this.special_hour_end_time = special_hour_end_time;
    }

    public String getSpecial_hour_lunch_start_time() {
        return special_hour_lunch_start_time;
    }

    public void setSpecial_hour_lunch_start_time(String special_hour_lunch_start_time) {
        this.special_hour_lunch_start_time = special_hour_lunch_start_time;
    }

    public String getSpecial_hour_lunch_end_time() {
        return special_hour_lunch_end_time;
    }

    public void setSpecial_hour_lunch_end_time(String special_hour_lunch_end_time) {
        this.special_hour_lunch_end_time = special_hour_lunch_end_time;
    }

    public String getSpecial_hour_take_away_start_time() {
        return special_hour_take_away_start_time;
    }

    public void setSpecial_hour_take_away_start_time(String special_hour_take_away_start_time) {
        this.special_hour_take_away_start_time = special_hour_take_away_start_time;
    }

    public String getSpecial_hour_take_away_end_time() {
        return special_hour_take_away_end_time;
    }

    public void setSpecial_hour_take_away_end_time(String special_hour_take_away_end_time) {
        this.special_hour_take_away_end_time = special_hour_take_away_end_time;
    }

    public String getSpecial_hour_delivery_start_time() {
        return special_hour_delivery_start_time;
    }

    public void setSpecial_hour_delivery_start_time(String special_hour_delivery_start_time) {
        this.special_hour_delivery_start_time = special_hour_delivery_start_time;
    }

    public String getSpecial_hour_delivery_end_time() {
        return special_hour_delivery_end_time;
    }

    public void setSpecial_hour_delivery_end_time(String special_hour_delivery_end_time) {
        this.special_hour_delivery_end_time = special_hour_delivery_end_time;
    }
}
