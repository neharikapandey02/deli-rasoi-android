package com.example.desirasoi.activities.menu_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TagData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("name_fi")
    @Expose
    private String name_fi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_fi() {
        return name_fi;
    }

    public void setName_fi(String name_fi) {
        this.name_fi = name_fi;
    }
}
