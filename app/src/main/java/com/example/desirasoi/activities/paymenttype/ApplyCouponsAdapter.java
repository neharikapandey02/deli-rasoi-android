package com.example.desirasoi.activities.paymenttype;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.OrderSummaryActivity;
import com.example.desirasoi.activities.history_details.HistoryDetailsAdapter;
import com.example.desirasoi.activities.ui.history.HistoryOrderMenuMainData;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ApplyCouponsAdapter extends RecyclerView.Adapter<ApplyCouponsAdapter.RestaurantList> {
    private Context context;
    List<HistoryOrderMenuMainData> orderMenuMainData;
    List<CouponMainData> couponMainData;
    OrderSummaryActivity paymentTypeActivity;

    public ApplyCouponsAdapter(Context context, List<CouponMainData> couponMainData, OrderSummaryActivity paymentTypeActivity){
        this.context=context;
        this.couponMainData=couponMainData;
        this.paymentTypeActivity=paymentTypeActivity;

    }


    @NonNull
    @Override
    public ApplyCouponsAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_apply_coupons,null);
        ApplyCouponsAdapter.RestaurantList restaurantList=new ApplyCouponsAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull ApplyCouponsAdapter.RestaurantList holder, int position) {
        holder.tv_couponName.setText("Coupon "+(position+1));
        holder.tv_couponCode.setText(couponMainData.get(position).getCouponCode());
        holder.tv_discount.setText(couponMainData.get(position).getDiscount());
        holder.tv_endDate.setText(couponMainData.get(position).getEndDate());
        holder.btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentTypeActivity.selectCouponType(position);
            }
        });
    }



    @Override
    public int getItemCount() {
        return couponMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
     TextView tv_couponName;
     TextView tv_couponCode;
     TextView tv_discount;
     TextView tv_endDate;
     Button btn_select;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            tv_couponName=itemView.findViewById(R.id.tv_couponName);
            tv_couponCode=itemView.findViewById(R.id.tv_couponCode);
            tv_discount=itemView.findViewById(R.id.tv_discount);
            tv_endDate=itemView.findViewById(R.id.tv_endDate);
            btn_select=itemView.findViewById(R.id.btn_select);

        }
    }
}
