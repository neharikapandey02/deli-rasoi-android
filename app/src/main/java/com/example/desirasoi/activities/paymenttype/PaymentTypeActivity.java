package com.example.desirasoi.activities.paymenttype;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.LoginActivity;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.WebViewPaymentActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class PaymentTypeActivity extends AppCompatActivity {
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.cod)
    RadioButton cod;

    @BindView(R.id.paynow)
    RadioButton paynow;

    @BindView(R.id.btn_orderPlaced)
    Button btn_orderPlaced;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_viewAllCoupons)
    TextView tv_viewAllCoupons;

    @BindView(R.id.tv_subTotal)
    TextView tv_subTotal;

    @BindView(R.id.tv_couponDiscount)
    TextView tv_couponDiscount;

    @BindView(R.id.tv_grandTotal)
    TextView tv_grandTotal;

    @BindView(R.id.et_couponCode)
    EditText et_couponCode;

    @BindView(R.id.btn_apply)
    Button btn_apply;

    @BindView(R.id.linearLayout_details)
    LinearLayout linearLayout_details;

    private SharedPreferencesData sharedPreferencesData;

    String userId;
    String checkoutId;
    private String paymentType = "2";
    ApplyCouponsAdapter applyCouponsAdapter;

    List<CouponMainData> couponMains;
    List<ApplyCouponMainData> applyCouponMainData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_payment_type);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        applyCouponMainData = new ArrayList<>();
        couponMains = new ArrayList<>();
        checkoutId = getIntent().getStringExtra(Constants.CHECKOUT_ID);
        Log.e("CheckoutId=", checkoutId);
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.cod:
                        btn_orderPlaced.setText("Place Order");
                        paymentType = "2";
                        break;
                    case R.id.paynow:
                        btn_orderPlaced.setText("Pay Now");
                        paymentType = "1";
                        break;
                }
            }
        });

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doGetCoupons(getCouponsCallback, userId);

        } else {
            Snackbar.make(paynow, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_orderPlaced)
    public void btnClicked(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
//            RetrofitHelper.getInstance().doOrderPlace(placeOrderCallback, userId, checkoutId, paymentType);
        } else {
            Snackbar.make(paynow, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }


    Callback<OrderPlaceMain> placeOrderCallback = new Callback<OrderPlaceMain>() {
        @Override
        public void onResponse(Call<OrderPlaceMain> call, Response<OrderPlaceMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), MainPageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(intent);

                } else {
                    Intent intent=new Intent(getApplicationContext(), WebViewPaymentActivity.class);
                    intent.putExtra("checkoutId", checkoutId);
                    startActivity(intent);
                    finish();
                    Snackbar.make(paynow, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<OrderPlaceMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<CouponMain> getCouponsCallback = new Callback<CouponMain>() {
        @Override
        public void onResponse(Call<CouponMain> call, Response<CouponMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    couponMains.clear();
                    couponMains.addAll(response.body().getData());
                } else {


                }
            }
        }

        @Override
        public void onFailure(Call<CouponMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgview_back)
    public void backClicked(View view) {
        onBackPressed();
    }

    @OnClick(R.id.tv_viewAllCoupons)
    public void viewAllCouponsTextClicked(View view) {
        dialogForCoupons();
    }

    Dialog dialog;

    private void dialogForCoupons() {
        if (couponMains != null && couponMains.size() > 0) {
            dialog = new Dialog(PaymentTypeActivity.this);
            dialog.setContentView(R.layout.dialog_apply_coupons);
            Window window = dialog.getWindow();
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            RecyclerView recyclerview_applyCoupons = dialog.findViewById(R.id.recyclerview_applyCoupons);
            recyclerview_applyCoupons.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
            recyclerview_applyCoupons.setHasFixedSize(true);
//            applyCouponsAdapter = new ApplyCouponsAdapter(getApplicationContext(), couponMains, PaymentTypeActivity.this);
            recyclerview_applyCoupons.setAdapter(applyCouponsAdapter);
            dialog.show();
        } else {
            Snackbar.make(btn_orderPlaced, getResources().getString(R.string.no_coupons_available), Snackbar.LENGTH_LONG).show();
        }

    }

    int positionOfCoupons;

    public void selectCouponType(int position) {
        dialog.cancel();
        positionOfCoupons = position;
        et_couponCode.setText(couponMains.get(position).getCouponCode());

    }

    @OnClick(R.id.btn_apply)
    public void applyButtonClicked(View view) {
        if (et_couponCode.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_orderPlaced, getResources().getString(R.string.enter_coupon_code), Snackbar.LENGTH_LONG).show();
        } else {
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                String couponCode = couponMains.get(positionOfCoupons).getCouponCode();
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().doApplyCoupons(doApplyCallback, userId, checkoutId, couponCode);

            } else {
                Snackbar.make(paynow, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    Callback<ApplyCouponMain> doApplyCallback = new Callback<ApplyCouponMain>() {
        @Override
        public void onResponse(Call<ApplyCouponMain> call, Response<ApplyCouponMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    linearLayout_details.setVisibility(View.VISIBLE);
                    applyCouponMainData.clear();
                    applyCouponMainData.add(response.body().getData());
                    tv_subTotal.setText(response.body().getData().getGrandTotal());
                    tv_couponDiscount.setText(response.body().getData().getCouponDiscount());
                    tv_grandTotal.setText((Float.parseFloat(response.body().getData().getGrandTotal()) - Float.parseFloat(response.body().getData().getCouponDiscount())) + "");
                    Snackbar.make(paynow, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                } else {
                    linearLayout_details.setVisibility(View.GONE);
                    Snackbar.make(paynow, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<ApplyCouponMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
