package com.example.desirasoi.activities.ui.change_password;

import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.adapters.ForgotPasswordMain;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends Fragment {

    private ChangePasswordViewModel mViewModel;
    TextView tv_title_name;
    @BindView(R.id.et_oldPassword)
    EditText et_oldPassword;

    @BindView(R.id.et_newPassword)
    EditText et_newPassword;

    @BindView(R.id.et_confirmNewPassword)
    EditText et_confirmNewPassword;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.btn_changePassword)
    Button btn_changePassword;


    View view;
    public static ChangePassword newInstance() {
        return new ChangePassword();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.change_password_fragment, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        tv_title_name=view.getRootView().findViewById(R.id.tv_title_name);


    }

    @Override
    public void onResume() {
        super.onResume();
        tv_title_name.setText("Change Password");
    }


    @OnClick(R.id.btn_changePassword)
    public void updatePasswordClicked(View view){
        //Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        if (NetworkUtil.checkNetworkStatus(getActivity())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                String oldPassword = et_oldPassword.getText().toString().trim();
                String newPassword = et_newPassword.getText().toString().trim();
                RetrofitHelper.getInstance().doChangePassword(forgotPasswordCallback, newPassword, newPassword,oldPassword,"");
            }



        } else {
            Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }


    Callback<ForgotPasswordMain> forgotPasswordCallback = new Callback<ForgotPasswordMain>() {
        @Override
        public void onResponse(Call<ForgotPasswordMain> call, Response<ForgotPasswordMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess().equalsIgnoreCase("true")){
                    Snackbar.make(tv_title_name,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }else{
                    Snackbar.make(tv_title_name,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<ForgotPasswordMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    public boolean validation(){
        if (et_oldPassword.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(tv_title_name, getResources().getString(R.string.enter_old_password), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_newPassword.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(tv_title_name, getResources().getString(R.string.enter_new_password), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_confirmNewPassword.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(tv_title_name, getResources().getString(R.string.re_enter_password), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (!et_confirmNewPassword.getText().toString().equalsIgnoreCase(et_newPassword.getText().toString())){
            Snackbar.make(tv_title_name, getResources().getString(R.string.new_password_and_confirm), Snackbar.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
