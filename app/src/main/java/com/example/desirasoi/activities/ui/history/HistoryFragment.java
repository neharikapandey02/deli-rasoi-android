package com.example.desirasoi.activities.ui.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.adapters.ForgotPasswordMain;
import com.example.desirasoi.adapters.HistoryAdapter;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HistoryFragment extends Fragment {

    private ToolsViewModel toolsViewModel;
    TextView tv_title_name;
    View view;

    @BindView(R.id.recyclerview_History)
    RecyclerView recyclerview_History;

    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private HistoryAdapter historyAdapter;

    private SharedPreferencesData sharedPreferencesData;

    private List<HistoryOrderMainData> historyOrderMainData;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        sharedPreferencesData = new SharedPreferencesData(getContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {

            View root = inflater.inflate(R.layout.fragment_history, container, false);
            ButterKnife.bind(this, root);

            return root;
        }else return null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_title_name = view.getRootView().findViewById(R.id.tv_title_name);
        swipeRefresh = view.getRootView().findViewById(R.id.swipeRefresh);

        recyclerview_History.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerview_History.setHasFixedSize(true);
        tv_title_name.setText("History");
        MainPageActivity.swipeRefresh.setEnabled(false);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeRefresh.isRefreshing()){
                    swipeRefresh.setRefreshing(false);
                }
                if (NetworkUtil.checkNetworkStatus(getContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    String userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
                    RetrofitHelper.getInstance().doGetOrderHistory(forgotHistoryCallback, userId);

                } else {
                    Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });


        if (NetworkUtil.checkNetworkStatus(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            String userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
            RetrofitHelper.getInstance().doGetOrderHistory(forgotHistoryCallback, userId);

        } else {
            Snackbar.make(tv_title_name, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }


    Callback<HistoryMain> forgotHistoryCallback = new Callback<HistoryMain>() {
        @Override
        public void onResponse(Call<HistoryMain> call, Response<HistoryMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    historyOrderMainData = new ArrayList<>();
                    historyOrderMainData.clear();
                    historyOrderMainData.addAll(response.body().getOrders());
//                    Collections.reverse(historyOrderMainData);
                    historyAdapter = new HistoryAdapter(getContext(),historyOrderMainData);
                    recyclerview_History.setAdapter(historyAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<HistoryMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
    }

}