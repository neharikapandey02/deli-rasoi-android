package com.example.desirasoi.activities.ui.history;

import android.os.Parcel;


import com.example.desirasoi.activities.addaddress.SelectAddressAdapter;
import com.example.desirasoi.models.GetAddressMainData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HistoryOrderMainData implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("restaurant_id")
    @Expose
    private String restaurantId;
    @SerializedName("delivery_charge")
    @Expose
    private String deliveryCharge;
    @SerializedName("sub_total")
    @Expose
    private String subTotal;
    @SerializedName("tax")
    @Expose
    private String tax;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("coupon_discount")
    @Expose
    private String coupon_discount;

    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("order_payment_id")
    @Expose
    private String orderPaymentId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("order_number")
    @Expose
    private String order_number;
    @SerializedName("order_date")
    @Expose
    private String order_date;
    @SerializedName("order_time")
    @Expose
    private String order_time;
    @SerializedName("restaurant")
    @Expose
    private HistoryRestaurantMainData restaurant;

    @SerializedName("delivery_address")
    @Expose
    private GetAddressMainData delivery_address;


    @SerializedName("payment")
    @Expose
    private HistoryOrderPaymentMainData payment;
    @SerializedName("order_notifications")
    @Expose
    private List<HistoryOrderNotificationMainData> orderNotifications = null;
    @SerializedName("order_menus")
    @Expose
    private List<HistoryOrderMenuMainData> orderMenus = null;

    public String getOrder_date() {
        return order_date;
    }

    public GetAddressMainData getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(GetAddressMainData delivery_address) {
        this.delivery_address = delivery_address;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    protected HistoryOrderMainData(Parcel in) {
        id = in.readString();
        userId = in.readString();
        restaurantId = in.readString();
        deliveryCharge = in.readString();
        subTotal = in.readString();
        tax = in.readString();
        grandTotal = in.readString();
        serviceType = in.readString();
        orderPaymentId = in.readString();
        status = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        coupon_discount = in.readString();
    }

//    public static final Creator<HistoryOrderMainData> CREATOR = new Creator<HistoryOrderMainData>() {
//        @Override
//        public HistoryOrderMainData createFromParcel(Parcel in) {
//            return new HistoryOrderMainData(in);
//        }
//
//        @Override
//        public HistoryOrderMainData[] newArray(int size) {
//            return new HistoryOrderMainData[size];
//        }
//    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getOrderPaymentId() {
        return orderPaymentId;
    }

    public void setOrderPaymentId(String orderPaymentId) {
        this.orderPaymentId = orderPaymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public HistoryRestaurantMainData getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(HistoryRestaurantMainData restaurant) {
        this.restaurant = restaurant;
    }

    public HistoryOrderPaymentMainData getPayment() {
        return payment;
    }

    public void setPayment(HistoryOrderPaymentMainData payment) {
        this.payment = payment;
    }

    public List<HistoryOrderNotificationMainData> getOrderNotifications() {
        return orderNotifications;
    }

    public void setOrderNotifications(List<HistoryOrderNotificationMainData> orderNotifications) {
        this.orderNotifications = orderNotifications;
    }

    public List<HistoryOrderMenuMainData> getOrderMenus() {
        return orderMenus;
    }

    public void setOrderMenus(List<HistoryOrderMenuMainData> orderMenus) {
        this.orderMenus = orderMenus;
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }

//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeString(id);
//        parcel.writeString(userId);
//        parcel.writeString(restaurantId);
//        parcel.writeString(deliveryCharge);
//        parcel.writeString(subTotal);
//        parcel.writeString(tax);
//        parcel.writeString(grandTotal);
//        parcel.writeString(coupon_discount);
//        parcel.writeString(serviceType);
//        parcel.writeString(orderPaymentId);
//        parcel.writeString(status);
//        parcel.writeString(createdAt);
//        parcel.writeString(updatedAt);
//    }
}

