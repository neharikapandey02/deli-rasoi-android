package com.example.desirasoi.activities.ui.history;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HistoryOrderMenuMainData  implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("menu_name")
    @Expose
    private String menuName;
    @SerializedName("menu_description")
    @Expose
    private String menuDescription;
    @SerializedName("menu_image")
    @Expose
    private String menuImage;
    @SerializedName("menu_type")
    @Expose
    private String menuType;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("image")
    @Expose
    private String image;

   @SerializedName("discount")
    @Expose
    private String discount;

    protected HistoryOrderMenuMainData(Parcel in) {
        id = in.readString();
        orderId = in.readString();
        userId = in.readString();
        menuId = in.readString();
        menuName = in.readString();
        menuDescription = in.readString();
        menuImage = in.readString();
        menuType = in.readString();
        quantity = in.readString();
        price = in.readString();
        createdAt = in.readString();
        image = in.readString();
        discount = in.readString();
    }

//    public static final Creator<HistoryOrderMenuMainData> CREATOR = new Creator<HistoryOrderMenuMainData>() {
//        @Override
//        public HistoryOrderMenuMainData createFromParcel(Parcel in) {
//            return new HistoryOrderMenuMainData(in);
//        }
//
//        @Override
//        public HistoryOrderMenuMainData[] newArray(int size) {
//            return new HistoryOrderMenuMainData[size];
//        }
//    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuDescription() {
        return menuDescription;
    }

    public void setMenuDescription(String menuDescription) {
        this.menuDescription = menuDescription;
    }

    public String getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(String menuImage) {
        this.menuImage = menuImage;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeString(id);
//        parcel.writeString(orderId);
//        parcel.writeString(userId);
//        parcel.writeString(menuId);
//        parcel.writeString(menuName);
//        parcel.writeString(menuDescription);
//        parcel.writeString(menuImage);
//        parcel.writeString(menuType);
//        parcel.writeString(quantity);
//        parcel.writeString(price);
//        parcel.writeString(createdAt);
//        parcel.writeString(image);
//    }


    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
