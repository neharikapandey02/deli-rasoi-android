package com.example.desirasoi.activities.ui.home;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.GPSTracker;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.GetGuestDetails;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.models.RestaurantListMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    @BindView(R.id.recyclerview_list)
    RecyclerView recyclerview_list;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

//    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.shimmer_text)
    ShimmerLayout shimmer_text;
    private SharedPreferencesData sharedPreferencesData;


    private RestaurantListAdapter restaurantListAdapter;

    TextView tv_title_name;
    View view;
    private List<RestaurantListMainData> restaurantListMainData;
    private GPSTracker gpsTracker;
    double latitude;
    double longitude;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,root);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_title_name = view.getRootView().findViewById(R.id.tv_title_name);
//        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        tv_title_name.setText(getResources().getString(R.string.deli_rasoi));
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getActivity());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "true");

        shimmer_text.startShimmerAnimation();
        recyclerview_list.setLayoutManager(new GridLayoutManager(getContext(), 1));
        recyclerview_list.setHasFixedSize(true);
        MainPageActivity.swipeRefresh.setEnabled(true);
        MainPageActivity.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                    if (MainPageActivity.swipeRefresh.isRefreshing()) {
                        MainPageActivity.swipeRefresh.setRefreshing(false);
                    }
                if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE).equalsIgnoreCase("true")) {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                    alert.setCancelable(false);
//                    alert.setTitle("Location Service");
//                    alert.setMessage("We would like to use your location to show restaurants near you.");
//                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            try {
//                                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
//                                }
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }

                            getLocation();
//                        }
//                    });
//                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("") &&
//                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID).equalsIgnoreCase("")) {
//                                if (NetworkUtil.checkNetworkStatus(getActivity())) {
//                                    RetrofitHelper.getInstance().getGuestDetails(guestCallback);
//                                } else {
//                                    Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                                }
//                            } else {
//                                if (NetworkUtil.checkNetworkStatus(getActivity())) {
//                                    progressBar.setVisibility(View.VISIBLE);
//                                    RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID),
//                                            sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID),
//                                            "", "");
//
//                                } else {
//                                    Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                                }
//                            }
//                        }
//                    });
//                    alert.show();
                }
            }
        });

//        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if (swipeRefresh.isRefreshing()){
//                    swipeRefresh.setRefreshing(false);
//                }
//                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity().getApplicationContext());
//                alert.setCancelable(false);
//                alert.setTitle("Location Service");
//                alert.setMessage("We would like to use your location to show restaurants near you.");
//                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        try {
//                            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
//                                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
//                            }
//                        } catch (Exception e){
//                            e.printStackTrace();
//                        }
//
//                        getLocation();
//                    }
//                });
//                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("") &&
//                                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID).equalsIgnoreCase("")){
//                            if (NetworkUtil.checkNetworkStatus(getActivity())) {
//                                RetrofitHelper.getInstance().getGuestDetails(guestCallback);
//                            } else {
//                                Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                            }
//                        }else {
//                            if (NetworkUtil.checkNetworkStatus(getActivity())) {
//                                progressBar.setVisibility(View.VISIBLE);
//                                RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID),
//                                        sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
//                                        "", "");
//
//                            } else {
//                                Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                            }
//                        }
//                    }
//                });
//                alert.show();
//            }
//        });

//        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//        alert.setCancelable(false);
//        alert.setTitle("Location Service");
//        alert.setMessage("We would like to use your location to show restaurants near you.");
//        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        getLocation();
//            }
//        });
//        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("") &&
//                        sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID).equalsIgnoreCase("")){
//                    if (NetworkUtil.checkNetworkStatus(getActivity())) {
//                        RetrofitHelper.getInstance().getGuestDetails(guestCallback);
//                    } else {
//                        Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                    }
//                }else {
//                    if (NetworkUtil.checkNetworkStatus(getActivity())) {
//                        progressBar.setVisibility(View.VISIBLE);
//                        RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID),
//                                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
//                                "", "");
//
//                    } else {
//                        Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });
//        alert.show();

    }



    public void getLocation(){
        gpsTracker = new GPSTracker(getActivity());
        if(gpsTracker.canGetLocation()){
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();

            Log.e("dkfjd", String.valueOf(latitude));
            Log.e("dkfjd", String.valueOf(longitude));

            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("") &&
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID).equalsIgnoreCase("")){
                if (NetworkUtil.checkNetworkStatus(getActivity())) {
                    RetrofitHelper.getInstance().getGuestDetails(guestCallback);
                } else {
                    Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }else {
                    if (NetworkUtil.checkNetworkStatus(getActivity())) {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID),
                                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.GUEST_ID),
                                String.valueOf(latitude), String.valueOf(longitude));

                    } else {
                        Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
            }
        }else{
            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("") &&
                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID).equalsIgnoreCase("")){
                if (NetworkUtil.checkNetworkStatus(getActivity())) {
                    RetrofitHelper.getInstance().getGuestDetails(guestCallback);
                } else {
                    Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }else {
                if (NetworkUtil.checkNetworkStatus(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID),
                            sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
                            "", "");

                } else {
                    Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }

            gpsTracker.showSettingsAlert();
        }
    }



    Callback<GetGuestDetails> guestCallback = new Callback<GetGuestDetails>() {
        @Override
        public void onResponse(Call<GetGuestDetails> call, Response<GetGuestDetails> response) {
            try {
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID,response.body().getResult().getGuest_id());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.AUTH_TOKEN,response.body().getResult().getAuth_token());
                if (NetworkUtil.checkNetworkStatus(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID),
                            sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
                            String.valueOf(latitude), String.valueOf(longitude));

                } else {
                    Snackbar.make(recyclerview_list, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<GetGuestDetails> call, Throwable t) {

        }
    };

    Callback<RestaurantListMain> restaurantList = new Callback<RestaurantListMain>() {
        @Override
        public void onResponse(Call<RestaurantListMain> call, Response<RestaurantListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        shimmer_text.stopShimmerAnimation();
                        shimmer_text.setVisibility(View.GONE);
                        restaurantListMainData = new ArrayList<>();
                        restaurantListMainData.clear();
                        restaurantListMainData.addAll(response.body().getRestaurantList());
                        restaurantListAdapter = new RestaurantListAdapter(getContext(), restaurantListMainData, String.valueOf(latitude), String.valueOf(longitude));
                        recyclerview_list.setAdapter(restaurantListAdapter);
                    } else {
                        Snackbar.make(recyclerview_list, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }
        @Override
        public void onFailure(Call<RestaurantListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
            shimmer_text.stopShimmerAnimation();
            shimmer_text.setVisibility(View.GONE);
        }
    };

    @Override
    public void onResume() {
        tv_title_name.setText(getResources().getString(R.string.deli_rasoi));
        super.onResume();
    }
}