package com.example.desirasoi.activities.ui.logout;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.addaddress.AddNewAddressActivity;
import com.example.desirasoi.activities.addaddress.SelectAddressActivity;
import com.example.desirasoi.activities.addaddress.SelectAddressAdapter;
import com.example.desirasoi.activities.addaddress.SelectAddressAdapter1;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.GetAddressMain;
import com.example.desirasoi.models.GetAddressMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddressFragment extends Fragment {

    String userId;
    RecyclerView recyclerview_selectAddress;
    private SharedPreferencesData sharedPreferencesData;
    List<GetAddressMainData> getAddressMainData;
    LinearLayout linearLayout_addNewAddress;
    SelectAddressAdapter1 historyAdapter;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefresh;
    public AddressFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sharedPreferencesData = new SharedPreferencesData(getContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("")) {

            View view = inflater.inflate(R.layout.fragment_address, container, false);
            ButterKnife.bind(this, view);
            return view;
        } else {
            return null;
        }
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerview_selectAddress = view.findViewById(R.id.recyclerview_selectAddress);
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        linearLayout_addNewAddress = view.findViewById(R.id.linearLayout_addNewAddress);
        progressBar = view.findViewById(R.id.progressBar);
        sharedPreferencesData = new SharedPreferencesData(getActivity());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
        MainPageActivity.swipeRefresh.setEnabled(false);
        recyclerview_selectAddress.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerview_selectAddress.setHasFixedSize(true);

        if (NetworkUtil.checkNetworkStatus(getActivity())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doGetAddress(getAddressCallback, userId);

        } else {
            Snackbar.make(recyclerview_selectAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeRefresh.isRefreshing()){
                    swipeRefresh.setRefreshing(false);
                }
                if (NetworkUtil.checkNetworkStatus(getActivity())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().doGetAddress(getAddressCallback, userId);

                } else {
                    Snackbar.make(recyclerview_selectAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }

            }
        });

        linearLayout_addNewAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddNewAddressActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("")) {

            if (NetworkUtil.checkNetworkStatus(getActivity())) {

                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().doGetAddress(getAddressCallback, userId);
            } else {
                Snackbar.make(linearLayout_addNewAddress, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
            }
        }

    }

    Callback<GetAddressMain> getAddressCallback = new Callback<GetAddressMain>() {
        @Override
        public void onResponse(Call<GetAddressMain> call, Response<GetAddressMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess()) {
                    getAddressMainData = new ArrayList<>();
                    getAddressMainData.clear();
                    getAddressMainData.addAll(response.body().getData());
                    historyAdapter = new SelectAddressAdapter1(getActivity(), getAddressMainData);
                    recyclerview_selectAddress.setAdapter(historyAdapter);
                    //Snackbar.make(linearLayout_addNewAddress,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                } else {
                    getAddressMainData = new ArrayList<>();
                    getAddressMainData.clear();
                    historyAdapter = new SelectAddressAdapter1(getActivity(), getAddressMainData);
                    recyclerview_selectAddress.setAdapter(historyAdapter);
                    Snackbar.make(recyclerview_selectAddress, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<GetAddressMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
    }