package com.example.desirasoi.activities.ui.logout;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.LoginActivity;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.OrderSummaryActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.facebook.login.LoginManager;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LogoutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogoutFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ProgressBar progress_bar;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SharedPreferencesData sharedPreferencesData;

    public LogoutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogoutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LogoutFragment newInstance(String param1, String param2) {
        LogoutFragment fragment = new LogoutFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferencesData = new SharedPreferencesData(getActivity());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        MainPageActivity.swipeRefresh.setEnabled(false);
        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID).equalsIgnoreCase("")) {

            return inflater.inflate(R.layout.fragment_logout, container, false);
        }else {
            return null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_logout);
        Window window = dialog.getWindow();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        Button button_yes = dialog.findViewById(R.id.button_yes);
        progress_bar = dialog.findViewById(R.id.progress_bar);

        button_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.checkNetworkStatus(getActivity())) {
                    String device_token = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.DEVICE_TOKEN);
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
                    progress_bar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().doLogout(logoutCallback, userId, device_token);


                } else {
                    Snackbar.make(button_yes, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }

            }
        });


        Button button_no = dialog.findViewById(R.id.button_no);
        button_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();


    }

    Callback<SuccessResponse> logoutCallback = new Callback<SuccessResponse>() {
        @Override
        public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
      try {
          progress_bar.setVisibility(View.GONE);

          if (response.body().isSuccess()){
              sharedPreferencesData.clearSharedPreferenceData(Constants.USER_CREATE);
              LoginManager.getInstance().logOut();

              Intent intent=new Intent(getActivity(), LoginActivity.class);
              startActivity(intent);
              getActivity().finish();
          }
      }catch (Exception e){
          progress_bar.setVisibility(View.GONE);
          e.printStackTrace();
      }

        }

        @Override
        public void onFailure(Call<SuccessResponse> call, Throwable t) {

        }
    };

    }
