package com.example.desirasoi.activities.ui.myprofile;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.ChangePasswordActivity;
import com.example.desirasoi.activities.LoginActivity;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.WebViewPaymentActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.RealPathUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.models.UpdateProfileMain;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyProfileFragment extends Fragment {

    private GalleryViewModel galleryViewModel;

  View view;
  TextView tv_title_name;

  @BindView(R.id.et_firstName)
  EditText et_firstName;

  @BindView(R.id.et_lastName)
  EditText et_lastName;

  @BindView(R.id.et_email)
  EditText et_email;
  @BindView(R.id.et_mobileNumber)
  EditText et_mobileNumber;
  @BindView(R.id.et_city)
  EditText et_city;
  @BindView(R.id.et_state)
  EditText et_state;
  @BindView(R.id.et_zipCode)
  EditText et_zipCode;


  @BindView(R.id.et_country)
  EditText et_country;


  @BindView(R.id.btn_updateProfile)
  Button btn_updateProfile;

  @BindView(R.id.btn_changePassword)
  Button btn_changePassword;

  @BindView(R.id.newsLetterSwitch)
    Switch newsLetterSwitch;

  @BindView(R.id.progressBar)
  ProgressBar progressBar;
    private String userImage = "";

  @BindView(R.id.circleImageView_profile)
  CircleImageView circleImageView_profile;
    private static final int PERMISSION_REQUEST_CODE_CAMERA = 101;
    private static final int PERMISSION_REQUEST_CODE_GALLERY = 102;

    private Dialog dialog;

  private SharedPreferencesData sharedPreferencesData;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        sharedPreferencesData=new SharedPreferencesData(getContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {

            View root = inflater.inflate(R.layout.fragment_myprofile, container, false);
            ButterKnife.bind(this, root);
            return root;
        }
        else
        {startActivity(new Intent(getActivity(), LoginActivity.class));
            return null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_title_name=view.getRootView().findViewById(R.id.tv_title_name);
        tv_title_name.setText("My Profile");
        MainPageActivity.swipeRefresh.setEnabled(false);
        if (sharedPreferencesData!=null&&!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")){
           et_firstName.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME));
           et_lastName.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME));
           et_email.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL));
           et_mobileNumber.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.MOBILE_NUMBER));
           et_city.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.CITY));
           et_state.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.STATE));
           et_zipCode.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.POSTAL_CODE));
           et_country.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.COUNTRY));
           String imagePath= sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE);
           if (imagePath!=null&& !imagePath.equalsIgnoreCase("")){
               Picasso.with(getContext())
                       .load(imagePath)
                       .error(R.drawable.profile_placeholder)
                       .fit()
                       .placeholder(R.drawable.profile_placeholder)
                       .into(circleImageView_profile);
           }

           String newsLetter = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.NEWSLETTER_STATUS);
           if (newsLetter.equalsIgnoreCase("1")){
               newsLetterSwitch.setChecked(true);
           }
           else if(newsLetter.equalsIgnoreCase("2")){
               newsLetterSwitch.setChecked(false);
           }

        }

    }

    @OnClick(R.id.btn_updateProfile)
    public void updateClicked(View view){
        if (NetworkUtil.checkNetworkStatus(getContext())) {
            if (validation()){

                progressBar.setVisibility(View.VISIBLE);
                String firstName=et_firstName.getText().toString();
                String lastName=et_lastName.getText().toString();
                String email=et_email.getText().toString();
                String number=et_mobileNumber.getText().toString();
                String city=et_city.getText().toString();
                String state=et_state.getText().toString();
                String postalcode=et_zipCode.getText().toString();
                String country=et_country.getText().toString();
                String id=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
                String image=userImage;





                String newsLetter;
                if (newsLetterSwitch.isChecked()){
                    newsLetter = "1";
                }else {
                    newsLetter = "2";
                }



                MapboxGeocoding mapboxGeocoding = MapboxGeocoding.builder()
                        .accessToken(getString(R.string.mapbox_access_token))
                        .query(state +" "+city +" "+postalcode)
                        .build();

                mapboxGeocoding.enqueueCall(new Callback<GeocodingResponse>() {
                    @Override
                    public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {

                        List<CarmenFeature> results = response.body().features();

                        if (results.size() > 0) {

                            Point firstResultPoint = results.get(0).center();

                            String lng = String.valueOf(firstResultPoint.coordinates().get(0));
                            String lat = String.valueOf(firstResultPoint.coordinates().get(1));
                            RetrofitHelper.getInstance().doUpdateProfile(updateCallback, id,firstName,lastName,email,number,city,state,postalcode,country,image,
                                    newsLetter, lat, lng);
                        } else {

                            // No result for your request were found.
                            Log.e("onResponse", "onResponse: No result found");

                        }
                    }

                    @Override
                    public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
            }
        } else {
            Snackbar.make(btn_updateProfile, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<UpdateProfileMain> updateCallback = new Callback<UpdateProfileMain>() {
        @Override
        public void onResponse(Call<UpdateProfileMain> call, Response<UpdateProfileMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()){
                    if (response.body().getUserDetail()!=null){
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID,response.body().getUserDetail().getId());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME,response.body().getUserDetail().getFirstName());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME,response.body().getUserDetail().getLastName());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.MOBILE_NUMBER,response.body().getUserDetail().getContact());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL,response.body().getUserDetail().getEmail());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.USER_IMAGE,response.body().getUserDetail().getImage());
//                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.HOUSE,response.body().getUserDetail().getHouse());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.CITY,response.body().getUserDetail().getCity());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.STATE,response.body().getUserDetail().getStreet());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.POSTAL_CODE,response.body().getUserDetail().getPostalCode());
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.NEWSLETTER_STATUS,response.body().getUserDetail().getNewsletter_status());
//                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE,Constants.COUNTRY,response.body().getUserDetail().getCountry());
                        Snackbar.make(et_email,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                    }
                }else{
                    Snackbar.make(et_email,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<UpdateProfileMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    public boolean validation(){
        if (et_firstName.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_firstName,getResources().getString(R.string.enter_first_name),Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_lastName.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_firstName,getResources().getString(R.string.enter_last_name),Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (et_city.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_firstName,getResources().getString(R.string.enter_city),Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_state.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_firstName,getResources().getString(R.string.enter_state),Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_zipCode.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_firstName,getResources().getString(R.string.enter_postalcode),Snackbar.LENGTH_LONG).show();
            return false;
        }
        /*else if (et_country.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_firstName,getResources().getString(R.string.enter_country),Snackbar.LENGTH_LONG).show();
            return false;
        }*/
        return true;
    }

    @OnClick(R.id.imgview_editImage)
    public void imageClicked(View view){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_select_dialogue);
        TextView tv_gallery = dialog.findViewById(R.id.tv_gallery);
        TextView tv_camera = dialog.findViewById(R.id.tv_camera);
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    requestGallery();
                } else {

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, PERMISSION_REQUEST_CODE_GALLERY);

                }


            }
        });
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    request();
                } else {

                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);

                }
            }
        });

        dialog.show();
    }
    public void requestGallery() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_GALLERY);


    }
    public void request() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_CAMERA);


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        dialog.cancel();
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_GALLERY) {

           /* Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, PERMISSION_REQUEST_CODE_GALLERY);*/

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PERMISSION_REQUEST_CODE_GALLERY);


        } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_CAMERA) {


            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);


        } else {
            Toast.makeText(getContext(), "Please allow permission", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dialog.cancel();

        if (requestCode == PERMISSION_REQUEST_CODE_CAMERA && data != null) {
            Bitmap image = (Bitmap) data.getExtras().get("data");


            circleImageView_profile.setImageBitmap(image);
            Uri tempUri = getImageUri(getContext(), image);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            File finalFile = new File(getRealPathFromURI(tempUri));
            userImage = finalFile.toString();
            //imageList.add(finalFile.toString());
            Log.e("pathOfImage", finalFile + "");


        } else if (requestCode == PERMISSION_REQUEST_CODE_GALLERY && data != null) {
            String realPath = "";
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11) {
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getContext(), data.getData());
                Log.e("pathOfImage", realPath + "");

            }

            // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19) {
                realPath = RealPathUtil.getRealPathFromURI_API11to18(getContext(), data.getData());
                Log.e("pathOfImage", realPath + "");
            }

            // SDK > 19 (Android 4.4)
            else {
                realPath = RealPathUtil.getRealPathFromURI_API19(getContext(), data.getData());
                Log.e("pathOfImage", realPath + "");
            }

            userImage = realPath;
            Log.e("pathOfImage1", userImage + "");
            Uri selectedImage = data.getData();

            circleImageView_profile.setImageURI(selectedImage);
            //getImages(data,requestCode,resultCode);

        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContext().getContentResolver() != null) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }


    @OnClick(R.id.btn_changePassword)
    public void changePasswordClicked(View view){
        Intent intent=new Intent(getContext(), ChangePasswordActivity.class);
        startActivity(intent);
    }
}