package com.example.desirasoi.activities.ui.reserved_table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailableTiming {

    @SerializedName("booking_time")
    @Expose
    private String booking_time;

   @SerializedName("order_time")
    @Expose
    private String order_time;

    @SerializedName("formatted_time")
    @Expose
    private String formatted_time;

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getBooking_time() {
        return booking_time;
    }

    public void setBooking_time(String booking_time) {
        this.booking_time = booking_time;
    }

    public String getFormatted_time() {
        return formatted_time;
    }

    public void setFormatted_time(String formatted_time) {
        this.formatted_time = formatted_time;
    }
}
