package com.example.desirasoi.activities.ui.reserved_table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DateObject {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("available_dates")
    @Expose
    private ArrayList<AvailableDates> arrayList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<AvailableDates> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<AvailableDates> arrayList) {
        this.arrayList = arrayList;
    }
}
