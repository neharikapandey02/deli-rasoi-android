package com.example.desirasoi.activities.ui.reserved_table;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.history_details.HistoryDetailsActivity;
import com.example.desirasoi.adapters.HistoryAdapter;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservedTableAdapter extends RecyclerView.Adapter<ReservedTableAdapter.RestaurantList> {
    private Context context;
    List<TableBookedMainData> tableBookedMainData;
    ReservedTableAdapter.RestaurantList holder1;
    private SharedPreferencesData sharedPreferencesData;
    ProgressBar progressBar;
    ReservedTableHistoryActivity activity;

    public ReservedTableAdapter(Context context,List<TableBookedMainData> tableBookedMainData,ProgressBar progressBar,
                                ReservedTableHistoryActivity activity){
        this.tableBookedMainData=tableBookedMainData;
        this.context=context;
        sharedPreferencesData=new SharedPreferencesData(context);
        this.progressBar=progressBar;
        this.activity = activity;
    }


    @NonNull
    @Override
    public ReservedTableAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_reserved_table,null);
        ReservedTableAdapter.RestaurantList restaurantList=new ReservedTableAdapter.RestaurantList(view);
        return restaurantList;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "d.MM.yyyy H.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void onBindViewHolder(@NonNull ReservedTableAdapter.RestaurantList holder, int position) {
        holder1=holder;
        try {
            holder.tv_restaurantName.setText(tableBookedMainData.get(position).getRestaurant().getRestaurantName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tv_dataAndTime.setText(parseDateToddMMyyyy(tableBookedMainData.get(position).getBookDate()+" "+tableBookedMainData.get(position).getBookTime()));
        holder.tv_mealType.setText(tableBookedMainData.get(position).getMealType());
       holder.tv_mealType.setText(tableBookedMainData.get(position).getBookDate()+" "+
               tableBookedMainData.get(position).getBookTime());
       holder.tv_tableName.setText(tableBookedMainData.get(position).getTable().getTableName());
       holder.refTv.setText(tableBookedMainData.get(position).getBookingReferenceId());
       holder.nameTv.setText(tableBookedMainData.get(position).getName());
       holder.tv_bookingHours.setText(tableBookedMainData.get(position).getGuestCount());
       if (tableBookedMainData.get(position).getBookingStatus().equalsIgnoreCase("canceled")){
           holder.tv_status.setText(tableBookedMainData.get(position).getBookingStatus());
           holder.btn_cancelTable.setVisibility(View.GONE);
           holder.btn_edit.setVisibility(View.GONE);

       }else{
           holder.tv_status.setText(tableBookedMainData.get(position).getBookingStatus());
           holder.btn_cancelTable.setVisibility(View.VISIBLE);
           holder.btn_edit.setVisibility(View.VISIBLE);
       }

       holder.btn_edit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
                    Intent intent = new Intent(context, EditReservedTable.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("data", (TableBookedMainData) tableBookedMainData.get(position));
                    context.startActivity(intent);
           }
       });

       holder.btn_cancelTable.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (NetworkUtil.checkNetworkStatus(context)) {
                   progressBar.setVisibility(View.VISIBLE);
                   RetrofitHelper.getInstance().doCancelTableBooked(cancelTableCallback,tableBookedMainData.get(position).getUserId(),tableBookedMainData.get(position).getId());

               } else {
                   Snackbar.make(holder.tv_restaurantName, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
               }
           }
       });

        String orderDate = tableBookedMainData.get(position).getBookDate()+" "+tableBookedMainData.get(position).getBookTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date strDate = null;
        try {
            strDate = sdf.parse(orderDate);
            Log.e("dafdkfj", strDate.toString());
            Log.e("dafdkfj", orderDate.toString());
            if (new Date().after(strDate)) {
                holder.btn_cancelTable.setVisibility(View.GONE);
                holder.btn_edit.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }



    @Override
    public int getItemCount() {
        return tableBookedMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        CardView cardview_one,cardview_two;
        RelativeLayout relativeLayout_top;
        TextView tv_restaurantName;
        TextView tv_mealType;
        TextView tv_dataAndTime;
        TextView tv_tableName;
        TextView tv_bookingHours;
        TextView tv_status, refTv, nameTv;
        Button btn_cancelTable, btn_edit;
        ProgressBar progressBar;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            //cardview_one=itemView.findViewById(R.id.cardview_one);
            //cardview_two=itemView.findViewById(R.id.cardview_two);
            relativeLayout_top=itemView.findViewById(R.id.relativeLayout_top);
            tv_restaurantName=itemView.findViewById(R.id.tv_restaurantName);
            tv_mealType=itemView.findViewById(R.id.tv_mealType);
            tv_dataAndTime=itemView.findViewById(R.id.tv_dataAndTime);
            tv_tableName=itemView.findViewById(R.id.tv_tableName);
            tv_bookingHours=itemView.findViewById(R.id.tv_bookingHours);
            tv_status=itemView.findViewById(R.id.tv_status);
            btn_cancelTable=itemView.findViewById(R.id.btn_cancelTable);
            progressBar=itemView.findViewById(R.id.progressBar);
            refTv=itemView.findViewById(R.id.refTv);
            nameTv=itemView.findViewById(R.id.nameTv);
            btn_edit=itemView.findViewById(R.id.btn_edit);
        }
    }

    Callback<BookedCancelMain> cancelTableCallback = new Callback<BookedCancelMain>() {
        @Override
        public void onResponse(Call<BookedCancelMain> call, Response<BookedCancelMain> response) {
           progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {

                        Snackbar.make(progressBar, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                        activity.onResume();
                    } else {
                        Snackbar.make(progressBar, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<BookedCancelMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };
}
