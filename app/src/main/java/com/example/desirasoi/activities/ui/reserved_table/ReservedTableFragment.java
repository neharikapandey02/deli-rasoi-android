package com.example.desirasoi.activities.ui.reserved_table;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MainActivity;
import com.example.desirasoi.activities.MainPageActivity;
import com.example.desirasoi.activities.menu_details.RestaurantMenuDetailsActivity;
import com.example.desirasoi.activities.ui.reservation.ReservationTable;
import com.example.desirasoi.adapters.RestaurantListAdapter;
import com.example.desirasoi.adapters.RestaurantTableMain;
import com.example.desirasoi.adapters.RestaurantTableMainData;
import com.example.desirasoi.adapters.TableBookMainBooking;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.GPSTracker;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.OpenDays;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.models.RestaurantListMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReservedTableFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReservedTableFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.spinner_selectRestaurant)
    Spinner spinner_selectRestaurant;

    @BindView(R.id.dateSpn)
    Spinner dateSpn;

   @BindView(R.id.timeSpn)
    Spinner timeSpn;

    @BindView(R.id.spinner_selectTable)
    Spinner spinner_selectTable;

    @BindView(R.id.spinner_selectMealType)
    Spinner spinner_selectMealType;
    private GPSTracker gpsTracker;
    @BindView(R.id.tv_date)
    TextView tv_date;

    @BindView(R.id.tv_time)
    TextView tv_time;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.et_name)
    EditText et_name;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.et_guestCount)
    EditText et_guestCount;
    @BindView(R.id.et_contact)
    EditText et_contact;

    ArrayList<AvailableDates> datesArrayList;
    ArrayList<AvailableTiming> timingArrayList;
    ArrayList<RestaurantListMainData> restaurant;
    ArrayList<RestaurantTableMainData> table ;
    String[] mealtype = { "Lunch", "Dinner"};
    TextView tv_title_name;
    int resPos = 0, datePos = 0, timePos = 0;
    String startTime = "", endTime = "";

    SelectRestaurantAdapter tagSpinnerAdapter;

    SharedPreferencesData sharedPreferencesData;
    String userId,restaurantId,table_id,name,email,contact,booking_date,booking_time,guest_count,meal_type;


    public ReservedTableFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReservedTableFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReservedTableFragment newInstance(String param1, String param2) {
        ReservedTableFragment fragment = new ReservedTableFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferencesData = new SharedPreferencesData(getContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.HOMEPAGE, "false");
        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID).equalsIgnoreCase("")) {

            View view = inflater.inflate(R.layout.fragment_reserved_table, container, false);
            ButterKnife.bind(this, view);
            return view;
        }else {
            return null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);

        tv_title_name=view.getRootView().findViewById(R.id.tv_title_name);
        tv_title_name.setText("Book a Table");
        MainPageActivity.swipeRefresh.setEnabled(false);


        //Select restaurant adapter
        SelectMealTypeAdapter selectMealtypeAdapter=new SelectMealTypeAdapter(getActivity(),R.layout.item_spinner_text_home,mealtype);
        selectMealtypeAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
        //Setting the ArrayAdapter data on the Spinner
        spinner_selectMealType.setAdapter(selectMealtypeAdapter);
        /*GPSTracker gpsTracker=new GPSTracker(getContext());
        Log.e("TAG=","Latitudddddd="+gpsTracker.getLatitude());
        Log.e("TAG=","Longitude="+gpsTracker.getLongitude());*/

        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        getLocation();


        et_name.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.FIRST_NAME) +" "+sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.LAST_NAME));
        et_email.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.EMAIL));
        et_contact.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.MOBILE_NUMBER));

        timeSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (NetworkUtil.checkNetworkStatus(getActivity())) {
                        timePos = position;
                        RetrofitHelper.getInstance().doResturantTableList(restaurantTableList, userId, restaurant.get(resPos).getId(), datesArrayList.get(datePos).getBook_date(), timingArrayList.get(position).getBooking_time());
                    } else {
                        Snackbar.make(tv_date, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }else {
                    table=new ArrayList();
                    RestaurantTableMainData data = new RestaurantTableMainData();
                    data.setTableName("Select Table");
                    table.add(0, data);
                    SelectTableAdapter selectTableAdapter=new SelectTableAdapter(getActivity(),R.layout.item_spinner_text_home,table);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    //Setting the ArrayAdapter data on the Spinner
                    spinner_selectTable.setAdapter(selectTableAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dateSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    datePos = position;
                    if (NetworkUtil.checkNetworkStatus(getActivity())) {
                        RetrofitHelper.getInstance().getTableTime(timesCallback, restaurant.get(resPos).getId(), datesArrayList.get(position).getBook_date());
                    } else {
                        Snackbar.make(tv_date, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }

                }
                else {
                    timingArrayList = new ArrayList<>();
                    AvailableTiming data = new AvailableTiming();
                    data.setFormatted_time("Select Time");
                    timingArrayList.add(0, data);
                    SelectTimeAdapter selectTableAdapter = new SelectTimeAdapter(getActivity(), R.layout.item_spinner_text_home, timingArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    timeSpn.setAdapter(selectTableAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_selectRestaurant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!= 0) {
                    resPos = i;

                    if (NetworkUtil.checkNetworkStatus(getActivity())) {
                        progressBar.setVisibility(View.VISIBLE);
//                    Date c = Calendar.getInstance().getTime();
//                    System.out.println("Current time => " + c);
//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                    String formattedDate = df.format(c);
//                    String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                        RetrofitHelper.getInstance().getTableDates(datesCallback, restaurant.get(i).getId());

                    } else {
                        Snackbar.make(tv_date, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
                else {
                    datesArrayList = new ArrayList<>();
                    AvailableDates availableDates = new AvailableDates();
                    availableDates.setFormat_date("Select Date");
                    datesArrayList.add(0, availableDates);
                    SelectDatesAdapter selectTableAdapter = new SelectDatesAdapter(getActivity(), R.layout.item_spinner_text_home, datesArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    //Setting the ArrayAdapter data on the Spinner
                    dateSpn.setAdapter(selectTableAdapter);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    Callback<TimeObject> timesCallback = new Callback<TimeObject>() {
        @Override
        public void onResponse(Call<TimeObject> call, Response<TimeObject> response) {
            try {
                if (response.body().isSuccess()) {
                    timePos = 0;
                    timingArrayList = new ArrayList<>();
                    timingArrayList = response.body().getArrayList();
                    AvailableTiming data = new AvailableTiming();
                    data.setFormatted_time("Select Time");
                    timingArrayList.add(0, data);
                    SelectTimeAdapter selectTableAdapter = new SelectTimeAdapter(getActivity(), R.layout.item_spinner_text_home, timingArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    timeSpn.setAdapter(selectTableAdapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<TimeObject> call, Throwable t) {

        }
    };

    Callback<DateObject> datesCallback = new Callback<DateObject>() {
        @Override
        public void onResponse(Call<DateObject> call, Response<DateObject> response) {
            try {
                progressBar.setVisibility(View.GONE);
                if (response.body().isSuccess()) {
                    datePos = 0;
                    datesArrayList = new ArrayList<>();
                    datesArrayList = response.body().getArrayList();
                    AvailableDates availableDates = new AvailableDates();
                    availableDates.setFormat_date("Select Date");
                    datesArrayList.add(0, availableDates);
                    SelectDatesAdapter selectTableAdapter = new SelectDatesAdapter(getActivity(), R.layout.item_spinner_text_home, datesArrayList);
                    selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                    //Setting the ArrayAdapter data on the Spinner
                    dateSpn.setAdapter(selectTableAdapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DateObject> call, Throwable t) {

        }
    };


    public void getLocation(){
        gpsTracker = new GPSTracker(getActivity());
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            Log.e("dfkjdfk", ""+latitude);
            Log.e("dfkjdfk", ""+longitude);
//            tvLatitude.setText(String.valueOf(latitude));
//            tvLongitude.setText(String.valueOf(longitude));
            if (NetworkUtil.checkNetworkStatus(getActivity())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().doResturantList(restaurantList, sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID),
                        sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID),
                        String.valueOf(latitude), String.valueOf(longitude));

            } else {
                Snackbar.make(tv_date, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
            }
        }else{
            gpsTracker.showSettingsAlert();
        }
    }
    Callback<RestaurantListMain> restaurantList = new Callback<RestaurantListMain>() {
        @Override
        public void onResponse(Call<RestaurantListMain> call, Response<RestaurantListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        restaurant=new ArrayList();
                        restaurant.clear();
                        restaurant.addAll(response.body().getRestaurantList());
                        RestaurantListMainData data = new RestaurantListMainData();
                        data.setRestaurantName("Select Restaurant");
                        restaurant.add(0, data);
                        tagSpinnerAdapter=new SelectRestaurantAdapter(getActivity(),R.layout.item_spinner_text_home,restaurant);
                        tagSpinnerAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                        spinner_selectRestaurant.setAdapter(tagSpinnerAdapter);

                    } else {
                        Snackbar.make(tv_date, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<RestaurantListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    Callback<RestaurantTableMain> restaurantTableList = new Callback<RestaurantTableMain>() {
        @Override
        public void onResponse(Call<RestaurantTableMain> call, Response<RestaurantTableMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        table=new ArrayList();
                        table.clear();
                        table.addAll(response.body().getData());
                        RestaurantTableMainData data = new RestaurantTableMainData();
                        data.setTableName("Select Table");
                        table.add(0, data);
                        SelectTableAdapter selectTableAdapter=new SelectTableAdapter(getActivity(),R.layout.item_spinner_text_home,table);
                        selectTableAdapter.setDropDownViewResource(R.layout.item_spinner_text_home);
                        //Setting the ArrayAdapter data on the Spinner
                        spinner_selectTable.setAdapter(selectTableAdapter);


                    } else {
                        Snackbar.make(tv_date, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<RestaurantTableMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };






    Calendar cal = Calendar.getInstance();
    @OnClick(R.id.tv_date)
    public void dateClicked(View view){
        DatePickerDialog dpd = new DatePickerDialog(getContext(), (view1, year, month, dayOfMonth) -> {
            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = inFormat.parse(String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            String goal = outFormat.format(date);
            ArrayList<OpenDays> arrayList = new ArrayList<>();
            arrayList.addAll(restaurant.get(resPos).getOpenDaysArrayList());
            for (int i=0;i<arrayList.size();i++){
                if (goal.equalsIgnoreCase(restaurant.get(resPos).getOpenDaysArrayList().get(i).getDay_name())){
                    startTime = restaurant.get(resPos).getOpenDaysArrayList().get(i).getOpen_time();
                    endTime = restaurant.get(resPos).getOpenDaysArrayList().get(i).getClose_time();

                    Log.e("ssssssss", startTime);
                    Log.e("ssssssss", endTime);
                    tv_date.setText(String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
                    return;
                }else if (i==arrayList.size()-1){
                    tv_date.setText("");
                    Toast.makeText(getActivity(), "Restaurent is not available on this day.", Toast.LENGTH_SHORT).show();
                }
                Log.e("dkfd", i+", "+arrayList.size());
            }

            //Toast.makeText(getContext(), String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth), Toast.LENGTH_SHORT).show();
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

        dpd.show();
    }

    @OnClick(R.id.tv_time)
    public void timeClicked(View view){
        if (!tv_date.getText().toString().equalsIgnoreCase("")) {
            TimePickerDialog tpd = new TimePickerDialog(getContext(), (view1, hourOfDay, minute) -> {
                try {
                    Date time1 = new SimpleDateFormat("HH:mm:ss").parse(startTime);
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(time1);
                    calendar1.add(Calendar.DATE, 1);


                    Date time2 = new SimpleDateFormat("HH:mm:ss").parse(endTime);
                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTime(time2);
                    calendar2.add(Calendar.DATE, 1);

                    String someRandomTime = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute) + ":00";
                    Date d = new SimpleDateFormat("HH:mm:ss").parse(someRandomTime);
                    Calendar calendar3 = Calendar.getInstance();
                    calendar3.setTime(d);
                    calendar3.add(Calendar.DATE, 1);

                    Date x = calendar3.getTime();
                    if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                        //checkes whether the current time is between 14:49:00 and 20:11:13.
                        tv_time.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
                        System.out.println(true);
                    }
                    else {
                        tv_time.setText("");
                        Toast.makeText(getContext(), "Restaurant only work from "+startTime+" to "+ endTime, Toast.LENGTH_LONG).show();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getContext(), String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute), Toast.LENGTH_SHORT).show();
            }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), DateFormat.is24HourFormat(getContext()));
            tpd.show();
        }else {
            Toast.makeText(getContext(), "Please select date first.", Toast.LENGTH_SHORT).show();

        }

    }

    //Select restaurant adapter
    class SelectRestaurantAdapter extends ArrayAdapter<String> {
        public SelectRestaurantAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(restaurant.get(position).getRestaurantName());
            return mySpinner;
        }
    }


    //Select restaurant adapter
    class SelectTableAdapter extends ArrayAdapter<String> {
        public SelectTableAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(table.get(position).getTableName());
            return mySpinner;
        }
    }

    class SelectDatesAdapter extends ArrayAdapter<String> {
        public SelectDatesAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(datesArrayList.get(position).getFormat_date());
            return mySpinner;
        }
    }

    class SelectTimeAdapter extends ArrayAdapter<String> {
        public SelectTimeAdapter(Context ctx, int txtViewResourceId, ArrayList arrayList) {
            super(ctx, txtViewResourceId, arrayList);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(timingArrayList.get(position).getFormatted_time());
            return mySpinner;
        }
    }

    //Select restaurant adapter
    class SelectMealTypeAdapter extends ArrayAdapter<String> {
        public SelectMealTypeAdapter(Context ctx, int txtViewResourceId, String[] objects) {
            super(ctx, txtViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.item_spinner_text_home, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.tv_dropdwon);
            main_text.setText(mealtype[position]);
            return mySpinner;
        }
    }


    public boolean validation(){
        if (et_name.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_name,getResources().getString(R.string.enter_name),Snackbar.LENGTH_LONG).show();
            return false;

        }else if (et_email.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_name,getResources().getString(R.string.enter_email),Snackbar.LENGTH_LONG).show();
            return false;

        }

        else if (spinner_selectRestaurant.getSelectedItemPosition() == 0){
            Snackbar.make(et_name,getResources().getString(R.string.select_rest),Snackbar.LENGTH_LONG).show();
            return false;

        }

        else if (dateSpn.getSelectedItemPosition() == 0){
            Snackbar.make(et_name,getResources().getString(R.string.select_date),Snackbar.LENGTH_LONG).show();
            return false;

        }
        else if (timeSpn.getSelectedItemPosition()== 0){
            Snackbar.make(et_name,getResources().getString(R.string.select_time),Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (et_guestCount.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_name,getResources().getString(R.string.enter_guset),Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (spinner_selectTable.getSelectedItemPosition()== 0){
            Snackbar.make(et_name,getResources().getString(R.string.select_table),Snackbar.LENGTH_LONG).show();
            return false;
        }

        else if (et_contact.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(et_name,getResources().getString(R.string.enter_number),Snackbar.LENGTH_LONG).show();
            return false;

        }
        return true;
    }

    @OnClick(R.id.btn_bookTable)
    public void bookTableClicked(View view){
        if (NetworkUtil.checkNetworkStatus(getActivity())) {
            if (validation()){
                progressBar.setVisibility(View.VISIBLE);
                table_id=table.get(spinner_selectTable.getSelectedItemPosition()).getId()+"";
                name=et_name.getText().toString();
                email=et_email.getText().toString();
                contact=et_contact.getText().toString();
                booking_date=datesArrayList.get(datePos).getBook_date();
                booking_time=timingArrayList.get(timePos).getBooking_time()+":00";
                guest_count=et_guestCount.getText().toString();
                meal_type=mealtype[spinner_selectMealType.getSelectedItemPosition()];
                RetrofitHelper.getInstance().doResturantTableBook(restaurantTableBook,userId,table_id,name,email,contact,booking_date,booking_time,guest_count,meal_type);
            }

        } else {
            Snackbar.make(tv_date, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }


    Callback<TableBookMainBooking> restaurantTableBook = new Callback<TableBookMainBooking>() {
        @Override
        public void onResponse(Call<TableBookMainBooking> call, Response<TableBookMainBooking> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        Snackbar.make(tv_date, response.body().getMessage(), Snackbar.LENGTH_LONG).show();

                        Intent  reservedTableHistoryActivity=new Intent(getContext(),ReservedTableHistoryActivity.class);
                        startActivity(reservedTableHistoryActivity);

                    } else {
                        Snackbar.make(tv_date, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<TableBookMainBooking> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    @OnClick(R.id.btn_history)
    public void historyDetailsClicked(View view){
        Intent  reservedTableHistoryActivity=new Intent(getActivity(),ReservedTableHistoryActivity.class);
        startActivity(reservedTableHistoryActivity);
    }

}
