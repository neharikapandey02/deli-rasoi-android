package com.example.desirasoi.activities.ui.reserved_table;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.contact_us.ContactUsActivity;
import com.example.desirasoi.activities.contact_us.GetRestaurantMain;
import com.example.desirasoi.adapters.HistoryAdapter;
import com.example.desirasoi.adapters.TableBookMain;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReservedTableHistoryActivity extends AppCompatActivity {
    @BindView(R.id.recyclerview_tableBookedHistory)
    RecyclerView recyclerview_tableBookedHistory;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefresh;


    ReservedTableAdapter reservedTableAdapter;
    private SharedPreferencesData sharedPreferencesData;
    String userId;

     List<TableBookedMainData> tableBookedMainData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_reserved_table_history);
        ButterKnife.bind(this);
        swipeRefresh = findViewById(R.id.swipeRefresh);
        viewFinds();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().doGetResturantBooked(reserverdTableCallback,userId);
        } else {
            Snackbar.make(recyclerview_tableBookedHistory, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (swipeRefresh.isRefreshing()){
                    swipeRefresh.setRefreshing(false);
                }
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().doGetResturantBooked(reserverdTableCallback,userId);
                } else {
                    Snackbar.make(recyclerview_tableBookedHistory, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE, Constants.USER_ID);
        recyclerview_tableBookedHistory.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recyclerview_tableBookedHistory.setHasFixedSize(true);
    }
    Callback<TableBookedMain> reserverdTableCallback = new Callback<TableBookedMain>() {
        @Override
        public void onResponse(Call<TableBookedMain> call, Response<TableBookedMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        tableBookedMainData=new ArrayList<>();
                        tableBookedMainData.clear();
                        tableBookedMainData.addAll(response.body().getBookings());
                        reservedTableAdapter =new ReservedTableAdapter(getApplicationContext(),tableBookedMainData,progressBar,
                                ReservedTableHistoryActivity.this);
                        recyclerview_tableBookedHistory.setAdapter(reservedTableAdapter);

                    } else {
                        Snackbar.make(recyclerview_tableBookedHistory, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                    }

                }
            }
        }

        @Override
        public void onFailure(Call<TableBookedMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };

    @OnClick(R.id.imgview_back)
    public void backClicked(View view){
        onBackPressed();
    }
}
