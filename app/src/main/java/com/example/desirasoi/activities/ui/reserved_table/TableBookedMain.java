package com.example.desirasoi.activities.ui.reserved_table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TableBookedMain {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("bookings")
    @Expose
    private List<TableBookedMainData> bookings = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TableBookedMainData> getBookings() {
        return bookings;
    }

    public void setBookings(List<TableBookedMainData> bookings) {
        this.bookings = bookings;
    }
}
