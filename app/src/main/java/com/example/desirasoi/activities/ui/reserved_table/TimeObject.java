package com.example.desirasoi.activities.ui.reserved_table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TimeObject {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("timing")
    @Expose
    private ArrayList<AvailableTiming> arrayList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<AvailableTiming> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<AvailableTiming> arrayList) {
        this.arrayList = arrayList;
    }
}
