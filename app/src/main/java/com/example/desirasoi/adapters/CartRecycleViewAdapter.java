package com.example.desirasoi.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.MyCartActivity;
import com.example.desirasoi.activities.ui.mycart.MyCart;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddMinusMain;
import com.example.desirasoi.models.CartMenuMainData;
import com.example.desirasoi.models.FoodMenuMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CartRecycleViewAdapter extends RecyclerView.Adapter<CartRecycleViewAdapter.MyViewHolder> {
    boolean addorminus;
    MyViewHolder holder1;
    private Context context;
    private List<CartMenuMainData> addMinusCartMainData;
    double totalAmount;

    SharedPreferencesData sharedPreferencesData;
    String userId;
    String restaurantId;
    private int position;
    private String quantity;
    MyCartActivity myCart;


    public CartRecycleViewAdapter(Context context, List<CartMenuMainData> addMinusCartMainData,MyCartActivity myCart,String restaurantId) {
        this.context = context;
        this.addMinusCartMainData=addMinusCartMainData;
        totalAmount=0.0;
        sharedPreferencesData=new SharedPreferencesData(context);
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.USER_ID);
        this.myCart=myCart;
        this.restaurantId=restaurantId;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cart_layout, null);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

//        if (addMinusCartMainData.get(position).getImage()!=null){
//            Picasso.with(context)
//                    .load(addMinusCartMainData.get(position).getImage())
//                    .error(R.drawable.restaurant_list)
//                    .fit()
//                    .placeholder(R.drawable.restaurant_list)
//                    .into(holder.imgViewCart);
//        }

        holder.dishName.setText(addMinusCartMainData.get(position).getMenu());
        holder.tv_quantity.setText(addMinusCartMainData.get(position).getQuantity());
        String amount = addMinusCartMainData.get(position).getAmount();
        amount = amount.replace(".", ",");
        String totalAmount = addMinusCartMainData.get(position).getTotalAmount();
        totalAmount = totalAmount.replace(".", ",");
        holder.priceTv.setText(amount+" * "+addMinusCartMainData.get(position).getQuantity()+" = "+totalAmount+context.getResources().getString(R.string.euro_sign));
        holder1=holder;








    }

    @Override
    public int getItemCount() {
        return addMinusCartMainData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgViewCart;
        TextView dishName;
        TextView priceTv;
        TextView tv_quantity;
        LinearLayout linearLayout_Minus;
        LinearLayout linearLayout_Plus;
        ProgressBar progressBar;

        MyViewHolder(View itemView) {
            super(itemView);
            imgViewCart = itemView.findViewById(R.id.imgViewCart);
            dishName = itemView.findViewById(R.id.dishName);
            priceTv = itemView.findViewById(R.id.priceTv);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            linearLayout_Minus = itemView.findViewById(R.id.linearLayout_Minus);
            linearLayout_Plus = itemView.findViewById(R.id.linearLayout_Plus);
            progressBar = itemView.findViewById(R.id.progressBar);

            linearLayout_Plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        //progressBar.setVisibility(View.VISIBLE);
                        position=getAdapterPosition();
                        quantity=(Integer.parseInt(addMinusCartMainData.get(position).getQuantity())+1)+"";
                        RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,addMinusCartMainData.get(getAdapterPosition()).getMenuId(),quantity,
                                sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));

                    } else {
                        Snackbar.make(imgViewCart, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            linearLayout_Minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        if (!addMinusCartMainData.get(getAdapterPosition()).getQuantity().equalsIgnoreCase("0")){
                            //progressBar.setVisibility(View.VISIBLE);
                            position=getAdapterPosition();
                            quantity=(Integer.parseInt(addMinusCartMainData.get(position).getQuantity())-1)+"";
                            RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,addMinusCartMainData.get(getAdapterPosition()).getMenuId(),quantity,
                                    sharedPreferencesData.getSharedPreferenceData(Constants.USER_CREATE,Constants.GUEST_ID));

                        }

                    } else {
                        Snackbar.make(imgViewCart, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });


        }


    }
    Callback<AddMinusMain> addItemCallback = new Callback<AddMinusMain>() {
        @Override
        public void onResponse(Call<AddMinusMain> call, Response<AddMinusMain> response) {
           // holder1.progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        myCart.callGetCartWhenItemChanged();
                        //Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<AddMinusMain> call, Throwable t) {
           // holder1.progressBar.setVisibility(View.GONE);

        }
    };
}
