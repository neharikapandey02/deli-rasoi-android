package com.example.desirasoi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;

public class ExtraItemAdapter extends RecyclerView.Adapter<ExtraItemAdapter.ItemAdapter> {
    private Context context;

    public ExtraItemAdapter(Context context){
       this.context=context;
    }


    @NonNull
    @Override
    public ItemAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_extra,null);
        ExtraItemAdapter.ItemAdapter itemAdapter=new ExtraItemAdapter.ItemAdapter(view);
        return itemAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ItemAdapter extends RecyclerView.ViewHolder{
        public ItemAdapter(@NonNull View itemView) {
            super(itemView);
        }
    }
}
