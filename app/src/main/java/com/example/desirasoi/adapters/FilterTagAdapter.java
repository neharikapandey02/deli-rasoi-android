package com.example.desirasoi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.menu_details.GetTag;
import com.example.desirasoi.activities.menu_details.TagData;

import java.util.ArrayList;

public class FilterTagAdapter extends RecyclerView.Adapter<FilterTagAdapter.ItemAdapter> {
    private Context context;
    ArrayList<TagData> arrayList;
    public static ArrayList<String> idList;
    ArrayList<String> checkedList;

    public FilterTagAdapter(Context context, ArrayList<TagData> arrayList, ArrayList<String> checkedList){
       this.context=context;
       idList = new ArrayList<>();
       this.arrayList = arrayList;
       this.checkedList = checkedList;
    }


    @NonNull
    @Override
    public ItemAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.filter_adapter_layout,null);
        FilterTagAdapter.ItemAdapter itemAdapter=new FilterTagAdapter.ItemAdapter(view);
        return itemAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemAdapter holder, int position) {

        holder.checkbox.setText(arrayList.get(position).getName());

        try {
            for (int i=0;i<checkedList.size();i++){
                if (checkedList.get(i).equalsIgnoreCase(arrayList.get(position).getId())){
                    holder.checkbox.setChecked(true);
                    idList.add(arrayList.get(position).getId());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    idList.add(arrayList.get(position).getId());
                }else {
                    if (idList.size()>0){
                        idList.remove(arrayList.get(position).getId());
                    }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ItemAdapter extends RecyclerView.ViewHolder{
        CheckBox checkbox;
        public ItemAdapter(@NonNull View itemView) {
            super(itemView);
            checkbox = itemView.findViewById(R.id.checkbox);
        }
    }
}
