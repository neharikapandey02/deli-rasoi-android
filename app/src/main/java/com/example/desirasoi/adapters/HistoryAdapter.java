package com.example.desirasoi.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.SpecialityRestaurantList;
import com.example.desirasoi.activities.history_details.HistoryDetailsActivity;
import com.example.desirasoi.activities.ui.HistoryDetails;
import com.example.desirasoi.activities.ui.history.HistoryFragment;
import com.example.desirasoi.activities.ui.history.HistoryOrderMainData;
import com.example.desirasoi.constants.Constants;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.RestaurantList> {
    private Context context;
    private List<HistoryOrderMainData> historyOrderMainData;
    private int itemCount;
    private int adapterPosition;
    public HistoryAdapter(Context context,List<HistoryOrderMainData> historyOrderMainData){
        this.context=context;
        this.historyOrderMainData=historyOrderMainData;

    }


    @NonNull
    @Override
    public HistoryAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_history,null);
        HistoryAdapter.RestaurantList restaurantList=new HistoryAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.RestaurantList holder, int position) {

        itemCount = 0;
        holder.tv_orderNumber.setText("Order No: \n"+historyOrderMainData.get(position).getOrder_number());
        for (int i=0;i<historyOrderMainData.get(position).getOrderMenus().size();i++){
            itemCount=itemCount+Integer.parseInt(historyOrderMainData.get(position).getOrderMenus().get(i).getQuantity());
        }
        holder.tv_itemCount.setText(itemCount+" Items");
        holder.tv_address.setText(historyOrderMainData.get(position).getRestaurant().getRestaurantName());
//        double amount = 0;
//        try {
//            double a = Double.parseDouble(historyOrderMainData.get(position).getGrandTotal());
//            if (historyOrderMainData.get(position).getCoupon_discount() != null) {
//                double b = Double.parseDouble(historyOrderMainData.get(position).getCoupon_discount());
//                amount = Double.parseDouble(historyOrderMainData.get(position).getGrandTotal()) - Double.parseDouble(historyOrderMainData.get(position).getCoupon_discount());
//            }else {
//                amount = Double.parseDouble(historyOrderMainData.get(position).getGrandTotal());
//
//            }
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//        }
        String a = historyOrderMainData.get(position).getGrandTotal();
        a = a.replace(".", ",");
        holder.tv_totalPrice.setText(a+ context.getResources().getString(R.string.euro_sign));
        if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("0")){
            holder.tv_status.setText("Pending from restaurant");
        }else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("1")){
            holder.tv_status.setText("Order Accepted");
        }else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("2")){
            holder.tv_status.setText("Order Completed");
        }else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("3")){
            holder.tv_status.setText("Cancelled by Restaurant");
        }else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("4")){
            holder.tv_status.setText("Cancelled by User");
        }
        else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("5")){
            holder.tv_status.setText("Order in progress");
        }
        else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("6")){
            holder.tv_status.setText("Order is ready");
        }
        else if (historyOrderMainData.get(position).getStatus().equalsIgnoreCase("7")){
            holder.tv_status.setText("In delivery");
        }
//        holder.tv_orderDate.setText(getDate(historyOrderMainData.get(position).getOrder_date()+" "+
//                historyOrderMainData.get(position).getOrder_time()));

        holder.tv_orderDate.setText(getDate(historyOrderMainData.get(position).getCreatedAt()));
        holder.relativeLayout_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, HistoryDetailsActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                Bundle bundle = new Bundle();
//                bundle.putParcelableArrayList("arraylist", (ArrayList<HistoryOrderMainData>)historyOrderMainData);
                intent.putExtra("array", (HistoryOrderMainData) historyOrderMainData.get(position));
                 intent.putExtra(Constants.POSITION,position);
                intent.putExtra(Constants.STATUS,historyOrderMainData.get(position).getStatus());
                context.startActivity(intent);

            }
        });
    }



    @Override
    public int getItemCount() {
        return historyOrderMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        CardView cardview_one,cardview_two;
        RelativeLayout relativeLayout_top;
        TextView tv_orderNumber;
        TextView tv_itemCount;
        TextView tv_address;
        TextView tv_totalPrice;
        TextView tv_orderDate;
        TextView tv_status;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            //cardview_one=itemView.findViewById(R.id.cardview_one);
            //cardview_two=itemView.findViewById(R.id.cardview_two);
            relativeLayout_top=itemView.findViewById(R.id.relativeLayout_top);
            tv_orderNumber=itemView.findViewById(R.id.tv_orderNumber);
            tv_itemCount=itemView.findViewById(R.id.tv_itemCount);
            tv_address=itemView.findViewById(R.id.tv_address);
            tv_totalPrice=itemView.findViewById(R.id.tv_totalPrice);
            tv_orderDate=itemView.findViewById(R.id.tv_orderDate);
            tv_status=itemView.findViewById(R.id.tv_status);
        }
    }

    private String getDate(String ourDate)
    {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "d.M.yyyy H.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(ourDate);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
