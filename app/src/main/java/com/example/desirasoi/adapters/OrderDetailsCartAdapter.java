package com.example.desirasoi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.OrderSummaryActivity;
import com.example.desirasoi.activities.menu_details.RestaurantMenuDetailsActivity;
import com.example.desirasoi.activities.ui.history.HistoryOrderMenuMainData;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.NetworkUtil;
import com.example.desirasoi.models.AddMinusMain;
import com.example.desirasoi.models.CheckoutSummaryCart;
import com.example.desirasoi.models.FoodMenuMainData;
import com.example.desirasoi.retrofit.RetrofitHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsCartAdapter extends RecyclerView.Adapter<OrderDetailsCartAdapter.RestaurantList> {
    private Context context;
    List<CheckoutSummaryCart> orderMenuMainData;
    String userId;
    String restaurantId;
    private String quantity;
    ProgressBar progressBar;
    OrderSummaryActivity activity;

    public OrderDetailsCartAdapter(Context context, List<CheckoutSummaryCart> orderMenuMainData, String userId, String restaurantId, ProgressBar progressBar,
                                   OrderSummaryActivity activity){
        this.context=context;
        this.orderMenuMainData=orderMenuMainData;
        this.restaurantId=restaurantId;
        this.progressBar=progressBar;
        this.userId = userId;
        this.activity = activity;
    }


    @NonNull
    @Override
    public OrderDetailsCartAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_order_cart,null);
        OrderDetailsCartAdapter.RestaurantList restaurantList=new OrderDetailsCartAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailsCartAdapter.RestaurantList holder, int position) {
        holder.minusLinearLayout.setVisibility(View.VISIBLE);
        holder.addLinearLayout.setVisibility(View.VISIBLE);
        holder.tv_itemName.setText(orderMenuMainData.get(position).getMenu());
        String price = orderMenuMainData.get(position).getAmount();
        price = price.replace(".", ",");
        holder.tv_rate.setText(price + context.getResources().getString(R.string.euro_sign));
        holder.tv_quantity.setText(orderMenuMainData.get(position).getQuantity());
        String totalPrice = String.valueOf(Float.parseFloat(orderMenuMainData.get(position).getAmount())*Float.parseFloat(orderMenuMainData.get(position).getQuantity()));
        totalPrice = totalPrice.replace(".", ",");
        holder.tv_Totalprice.setText(totalPrice+context.getResources().getString(R.string.euro_sign));
        String dis = orderMenuMainData.get(position).getDiscount();
        if (dis.equalsIgnoreCase("0")){
            holder.discountTv.setVisibility(View.GONE);
            holder.disTv.setVisibility(View.GONE);
        }else {
            dis = dis.replace(".", ",");
            holder.discountTv.setText("-" + dis + context.getString(R.string.euro_sign));
        }
        holder.addLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
//                    position=getAdapterPosition();

                    progressBar.setVisibility(View.VISIBLE);
                    int qt = Integer.parseInt(orderMenuMainData.get(position).getQuantity());
                    quantity=(qt+1)+"";
                    RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId,orderMenuMainData.get(position).getMenu_id(),quantity,
                            "");
                } else {
                    Snackbar.make(holder.tv_itemName, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });
        holder.minusLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
                    if (!orderMenuMainData.get(position).getQuantity().equalsIgnoreCase("0")){
                        progressBar.setVisibility(View.VISIBLE);
                        quantity=(Integer.parseInt(orderMenuMainData.get(position).getQuantity())-1)+"";
                        RetrofitHelper.getInstance().doAddItemList(addItemCallback,userId,restaurantId, orderMenuMainData.get(position).getMenu_id(),quantity,
                                "");

                    }

                } else {
                    Snackbar.make(holder.tv_itemName, context.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    Callback<AddMinusMain> addItemCallback = new Callback<AddMinusMain>() {
        @Override
        public void onResponse(Call<AddMinusMain> call, Response<AddMinusMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                //if (response.body() != null) {
                if (response.body().getSuccess()) {
                    activity.refresh();

                    Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_LONG).show();
                }
                // }
            }
        }

        @Override
        public void onFailure(Call<AddMinusMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);

        }
    };




    @Override
    public int getItemCount() {
        return orderMenuMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        CardView cardview_one,cardview_two;
        RelativeLayout relativeLayout_top;
        TextView tv_itemName;
        TextView tv_rate;
        TextView tv_quantity;
        TextView tv_Totalprice, discountTv, disTv;
        LinearLayout addLinearLayout, minusLinearLayout;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            //cardview_one=itemView.findViewById(R.id.cardview_one);
            //cardview_two=itemView.findViewById(R.id.cardview_two);
            relativeLayout_top=itemView.findViewById(R.id.relativeLayout_top);
            tv_itemName=itemView.findViewById(R.id.tv_itemName);
            tv_rate=itemView.findViewById(R.id.tv_rate);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            tv_Totalprice=itemView.findViewById(R.id.tv_Totalprice);
            discountTv=itemView.findViewById(R.id.discountTv);
            disTv=itemView.findViewById(R.id.disTv);
            addLinearLayout=itemView.findViewById(R.id.addLinearLayout);
            minusLinearLayout=itemView.findViewById(R.id.minusLinearLayout);
        }
    }
}
