package com.example.desirasoi.adapters;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.SpecialityRestaurantList;
import com.example.desirasoi.activities.lunch_details.LunchDetailsActivity;
import com.example.desirasoi.activities.menu_details.RestaurantMenuDetailsActivity;
import com.example.desirasoi.constants.Constants;
import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.OpenDays;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.models.RestaurantListMainData;
import com.google.gson.JsonArray;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.supercharge.shimmerlayout.ShimmerLayout;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.RestaurantList> {
    private Context context;
    private SharedPreferencesData sharedPreferencesData;
    private List<RestaurantListMainData> restaurantListMainData;
    private String startTime , endTime;
    String lat, lng;
    public RestaurantListAdapter(Context context,List<RestaurantListMainData> restaurantListMainData, String lat, String lng){
        this.context=context;
        this.restaurantListMainData=restaurantListMainData;
        this.lat = lat;
        this.lng = lng;
        sharedPreferencesData=new SharedPreferencesData(context);
    }


    @NonNull
    @Override
    public RestaurantListAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_restaurant_list,null);
        RestaurantListAdapter.RestaurantList restaurantList=new RestaurantListAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantListAdapter.RestaurantList holder, int position) {
     holder.cardview_one.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             //Intent intent=new Intent(context, SpecialityRestaurantList.class);
            /* Intent intent=new Intent(context, RestaurantMenuDetailsActivity.class);
             intent.putExtra(Constants.ID_RESTAURANT,restaurantListMainData.get(position).getId());
             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             context.startActivity(intent);*/
         }
     });
        Picasso.with(context)
                .load(restaurantListMainData.get(position).getImage())
                .error(R.drawable.restaurant_list)
                .fit()
                .into(holder.imgview_restaurantImage);
        holder.tv_restaurantName.setText(restaurantListMainData.get(position).getRestaurantName());
        if (restaurantListMainData.get(position).getSpeciality()!=null){
            holder.tv_speciality.setText(restaurantListMainData.get(position).getSpeciality());
        }
        if (restaurantListMainData.get(position).getDelivery_status().equalsIgnoreCase("On")){
            holder.deliveryImg.setImageResource(R.drawable.correct_icon);
        }else {
            holder.deliveryImg.setImageResource(R.drawable.close);
        }
        if (restaurantListMainData.get(position).getTake_away_status().equalsIgnoreCase("On")){
            holder.takeAwayImg.setImageResource(R.drawable.correct_icon);
        }else {
            holder.takeAwayImg.setImageResource(R.drawable.close);
        }

        if (restaurantListMainData.get(position).getDistance() == null){
            holder.distanceTv.setVisibility(View.INVISIBLE);
        }else {
            String distance = restaurantListMainData.get(position).getDistance();
            distance = distance.replace(".", ",");
            holder.distanceTv.setText(distance + " Km");
        }

        if (restaurantListMainData.get(position).getRestaurant_status().equalsIgnoreCase("On")){
            holder.closeTv.setVisibility(View.GONE);
        }else {
            holder.closeTv.setVisibility(View.VISIBLE);
        }

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("EEEE");
        String formattedDate = df.format(c);
        ArrayList<OpenDays> arrayList = new ArrayList<>();
        arrayList.addAll(restaurantListMainData.get(position).getOpenDaysArrayList());
        for (int i=0;i<arrayList.size();i++) {
            if (formattedDate.equalsIgnoreCase(restaurantListMainData.get(position).getOpenDaysArrayList().get(i).getDay_name())) {
                String startTime = restaurantListMainData.get(position).getOpenDaysArrayList().get(i).getOpen_time();
                String endTime = restaurantListMainData.get(position).getOpenDaysArrayList().get(i).getClose_time();
                holder.openTv.setText("Open today: "+parseDateToddMMyyyy(startTime)+" - "+parseDateToddMMyyyy(endTime));
            }
        }
        holder.tv_address.setText(restaurantListMainData.get(position).getAddress());
        holder.addressTv.setText(restaurantListMainData.get(position).getAddress());


        holder.relativeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Order!");
                alert.setMessage("Please select your order type.");
                alert.setCancelable(true);

                alert.setNegativeButton("Home delivery?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.SERVICE_TYPE, "1");
                            Intent intent = new Intent(context, RestaurantMenuDetailsActivity.class);
                            intent.putExtra("rest_obj", restaurantListMainData.get(position));
                            intent.putExtra(Constants.ID_RESTAURANT, restaurantListMainData.get(position).getId());
                            intent.putExtra("res_image", restaurantListMainData.get(position).getImage());
                            intent.putExtra("cart_qty", restaurantListMainData.get(position).getCart_quantity());
                            intent.putExtra("cart_amt", restaurantListMainData.get(position).getCart_amount());
                            intent.putExtra("cart_id", restaurantListMainData.get(position).getCart_id());
                            intent.putExtra("rest_name", restaurantListMainData.get(position).getRestaurantName());
                            intent.putExtra("rest_start_time", startTime);
                            intent.putExtra("resturant_status", restaurantListMainData.get(position).getRestaurant_status());
                            intent.putExtra("rest_end_time", endTime);
                            intent.putExtra("service_type", "1");
                            intent.putExtra("res_lat", lat);
                            intent.putExtra("res_lng", lng);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    });

                    alert.setPositiveButton("Take away?", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sharedPreferencesData.setSharedPreferenceData(Constants.USER_CREATE, Constants.SERVICE_TYPE, "2");

                            Intent intent = new Intent(context, RestaurantMenuDetailsActivity.class);
                            intent.putExtra("rest_obj", restaurantListMainData.get(position));
                            intent.putExtra(Constants.ID_RESTAURANT, restaurantListMainData.get(position).getId());
                            intent.putExtra("res_image", restaurantListMainData.get(position).getImage());
                            intent.putExtra("cart_qty", restaurantListMainData.get(position).getCart_quantity());
                            intent.putExtra("cart_amt", restaurantListMainData.get(position).getCart_amount());
                            intent.putExtra("cart_id", restaurantListMainData.get(position).getCart_id());
                            intent.putExtra("rest_name", restaurantListMainData.get(position).getRestaurantName());
                            intent.putExtra("rest_start_time", startTime);
                            intent.putExtra("resturant_status", restaurantListMainData.get(position).getRestaurant_status());
                            intent.putExtra("rest_end_time", endTime);
                            intent.putExtra("service_type", "2");
                            intent.putExtra("res_lat", lat);
                            intent.putExtra("res_lng", lng);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    });

                alert.show();
            }
        });
        holder.btn_foodMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, RestaurantMenuDetailsActivity.class);
                intent.putExtra(Constants.ID_RESTAURANT,restaurantListMainData.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.btn_lunchMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, LunchDetailsActivity.class);
                intent.putExtra(Constants.ID_RESTAURANT,restaurantListMainData.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


    /* holder.cardview_two.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             Intent intent=new Intent(context, SpecialityRestaurantList.class);
             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             context.startActivity(intent);
         }
     });*/
    }

    @Override
    public int getItemCount() {
        return restaurantListMainData.size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        CardView cardview_one,cardview_two;
        ImageView imgview_restaurantImage, deliveryImg, takeAwayImg;
        TextView tv_restaurantName;
        TextView tv_speciality;
        TextView tv_address, addressTv, distanceTv, openTv, closeTv;
        Button btn_foodMenu;
        Button btn_lunchMenu;
        RelativeLayout relativeLL;

        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            cardview_one=itemView.findViewById(R.id.cardview_one);
            imgview_restaurantImage=itemView.findViewById(R.id.imgview_restaurantImage);
            tv_restaurantName=itemView.findViewById(R.id.tv_restaurantName);
            tv_speciality=itemView.findViewById(R.id.tv_speciality);
            tv_address=itemView.findViewById(R.id.tv_address);
            addressTv=itemView.findViewById(R.id.addressTv);
            btn_foodMenu=itemView.findViewById(R.id.btn_foodMenu);
            btn_lunchMenu=itemView.findViewById(R.id.btn_lunchMenu);
            relativeLL=itemView.findViewById(R.id.relativeLL);
            takeAwayImg=itemView.findViewById(R.id.takeAwayImg);
            deliveryImg=itemView.findViewById(R.id.deliveryImg);
            distanceTv=itemView.findViewById(R.id.distanceTv);
            openTv=itemView.findViewById(R.id.openTv);
            closeTv=itemView.findViewById(R.id.closeTv);

            //cardview_two=itemView.findViewById(R.id.cardview_two);
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "HH.mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
