package com.example.desirasoi.adapters;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desirasoi.R;
import com.example.desirasoi.activities.SingleItemDetails;
import com.example.desirasoi.models.RestaurantDetailsMain;
import com.example.desirasoi.models.RestaurantDetailsMainData;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class RestaurantListSpacialityAdapter extends RecyclerView.Adapter<RestaurantListSpacialityAdapter.RestaurantList> {
    private Context context;
    private List<RestaurantDetailsMainData> restaurantDetailsMains;
    public RestaurantListSpacialityAdapter(Context context,List<RestaurantDetailsMainData> restaurantDetailsMains){
        this.restaurantDetailsMains=restaurantDetailsMains;
        this.context=context;
    }


    @NonNull
    @Override
    public RestaurantListSpacialityAdapter.RestaurantList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_restaurant_spaciality,null);
        RestaurantListSpacialityAdapter.RestaurantList restaurantList=new RestaurantListSpacialityAdapter.RestaurantList(view);
        return restaurantList;
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantListSpacialityAdapter.RestaurantList holder, int position) {
        holder.tv_foodName.setText(restaurantDetailsMains.get(0).getMenu().get(position).getMenu());
        holder.tv_price.setText(restaurantDetailsMains.get(0).getMenu().get(position).getPrice());
        Picasso.with(context)
                .load(restaurantDetailsMains.get(0).getMenu().get(position).getImage())
                .error(R.drawable.restaurant_list)
                .fit()
                .placeholder(R.drawable.restaurant_list)
                .into(holder.imgview_restaurantImage);
       // holder.tv_rating.setText(restaurantDetailsMains.get(0).getMenu().get(position).getRating());
    holder.layout_topDetails.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(context, SingleItemDetails.class);
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    });
    }

    @Override
    public int getItemCount() {
        return restaurantDetailsMains.get(0).getMenu().size();
    }
    public class RestaurantList extends RecyclerView.ViewHolder{
        LinearLayout layout_topDetails;
        TextView tv_foodName;
        TextView tv_price;
        TextView tv_rating;
        ImageView imgview_restaurantImage;
        public RestaurantList(@NonNull View itemView) {
            super(itemView);
            layout_topDetails=itemView.findViewById(R.id.layout_topDetails);
            tv_foodName=itemView.findViewById(R.id.tv_foodName);
            tv_price=itemView.findViewById(R.id.tv_price);
            tv_rating=itemView.findViewById(R.id.tv_rating);
            imgview_restaurantImage=itemView.findViewById(R.id.imgview_restaurantImage);
        }
    }
}
