package com.example.desirasoi.adapters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TableBookMainBooking {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("bookings")
    @Expose
    private List<TableBookMainBooking> bookings = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TableBookMainBooking> getBookings() {
        return bookings;
    }

    public void setBookings(List<TableBookMainBooking> bookings) {
        this.bookings = bookings;
    }

}
