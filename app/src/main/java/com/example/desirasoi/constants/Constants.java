package com.example.desirasoi.constants;

public interface Constants {
    String bundleData="BundleData";
    String BASE_URL="https://licpremium.com/";
    String USER_CREATE="app_shared_preferences";

    String DEVICE_ID ="1" ;
    String DEVICE_TOKEN ="device_token";
    String USER_ID ="userid" ;
    String GUEST_ID ="guest_id" ;
    String B_DATE ="b_date" ;
    String SERVICE_TYPE ="service_type" ;
    String HOMEPAGE ="homepage" ;
    String B_TIME ="b_time" ;
    String USER_NAME ="username" ;
    String MOBILE_NUMBER ="mobilenumber" ;
    String EMAIL ="email" ;
    String HOUSE ="house" ;
    String CITY ="city" ;
    String STATE ="state" ;
    String POSTAL_CODE ="postalcode" ;
    String COUNTRY ="country" ;
    String AUTH_TOKEN ="auth_token";
    String ID_RESTAURANT ="id_restaurant" ;
    String FIRST_NAME ="first_name" ;
    String LAST_NAME ="last_name" ;
    String USER_IMAGE ="user_image" ;
    String EDIT_ADDRESS ="edit_adddress" ;
    String CART_ID ="cart_id" ;
    String CHECKOUT_ID ="checkoutid" ;
    String ORDER_DETAILS = "order_details";
    String ORDERS ="orders" ;
    String POSITION = "position";
    String STATUS ="status" ;
    String NEWSLETTER_STATUS ="newsletter_status " ;
}
