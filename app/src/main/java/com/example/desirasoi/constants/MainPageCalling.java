package com.example.desirasoi.constants;

import android.app.Application;
import android.content.Context;

import com.example.desirasoi.retrofit.RetrofitHelper;

public class MainPageCalling extends Application {

    Context smart;

    @Override
    public void onCreate() {
        super.onCreate();
        smart = this.getApplicationContext();
        RetrofitHelper.getInstance().init(smart);

    }
}