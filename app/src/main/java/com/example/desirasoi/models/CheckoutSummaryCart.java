package com.example.desirasoi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckoutSummaryCart {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("cart_id")
    @Expose
    private String cart_id;

 @SerializedName("user_id")
    @Expose
    private String user_id;

@SerializedName("guest_id")
    @Expose
    private String guest_id;

@SerializedName("menu_id")
    @Expose
    private String menu_id;

@SerializedName("menu_type")
    @Expose
    private String menu_type;

@SerializedName("amount")
    @Expose
    private String amount;

@SerializedName("quantity")
    @Expose
    private String quantity;

@SerializedName("available_status")
    @Expose
    private String available_status;

@SerializedName("total_amount")
    @Expose
    private String total_amount;

@SerializedName("menu")
    @Expose
    private String menu;

@SerializedName("category_id")
    @Expose
    private String category_id;

@SerializedName("description")
    @Expose
    private String description;

@SerializedName("image")
    @Expose
    private String image;

@SerializedName("restaurant_id")
    @Expose
    private String restaurant_id;

@SerializedName("price_without_tax")
    @Expose
    private String price_without_tax;

@SerializedName("product_tax")
    @Expose
    private String product_tax;

@SerializedName("tax_rate")
    @Expose
    private String tax_rate;

@SerializedName("discount")
    @Expose
    private String discount;

@SerializedName("final_price")
    @Expose
    private String final_price;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAvailable_status() {
        return available_status;
    }

    public void setAvailable_status(String available_status) {
        this.available_status = available_status;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getPrice_without_tax() {
        return price_without_tax;
    }

    public void setPrice_without_tax(String price_without_tax) {
        this.price_without_tax = price_without_tax;
    }

    public String getProduct_tax() {
        return product_tax;
    }

    public void setProduct_tax(String product_tax) {
        this.product_tax = product_tax;
    }

    public String getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(String tax_rate) {
        this.tax_rate = tax_rate;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }
}
