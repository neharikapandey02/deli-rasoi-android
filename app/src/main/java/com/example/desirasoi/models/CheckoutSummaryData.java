package com.example.desirasoi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CheckoutSummaryData {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("restaurant_id")
    @Expose
    private String restaurant_id;

    @SerializedName("cart_id")
    @Expose
    private String cart_id;

    @SerializedName("address_id")
    @Expose
    private String address_id;

    @SerializedName("service_type")
    @Expose
    private String service_type;

@SerializedName("delivery_charge")
    @Expose
    private String delivery_charge;

@SerializedName("sub_total")
    @Expose
    private String sub_total;

@SerializedName("tax")
    @Expose
    private String tax;

@SerializedName("grand_total")
    @Expose
    private String grand_total;


@SerializedName("coupon_discount")
    @Expose
    private String coupon_discount;

@SerializedName("coupon_used_id")
    @Expose
    private String coupon_used_id;

@SerializedName("order_number")
    @Expose
    private String order_number;

@SerializedName("comment")
    @Expose
    private String comment;

@SerializedName("order_date")
    @Expose
    private String order_date;

@SerializedName("order_type")
    @Expose
    private String order_type;

@SerializedName("order_time")
    @Expose
    private String order_time;

@SerializedName("restaurant")
    @Expose
    private RestaurantDetailsMainData restaurant;

@SerializedName("delivery_address")
    @Expose
    private GetAddressMainData delivery_address;

@SerializedName("total_items")
    @Expose
    private String total_items;

@SerializedName("order_menus")
    @Expose
    private ArrayList<CheckoutSummaryCart> cart;

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }



    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public String getCoupon_used_id() {
        return coupon_used_id;
    }

    public void setCoupon_used_id(String coupon_used_id) {
        this.coupon_used_id = coupon_used_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public RestaurantDetailsMainData getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantDetailsMainData restaurant) {
        this.restaurant = restaurant;
    }

    public GetAddressMainData getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(GetAddressMainData delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getTotal_items() {
        return total_items;
    }

    public void setTotal_items(String total_items) {
        this.total_items = total_items;
    }

    public ArrayList<CheckoutSummaryCart> getCart() {
        return cart;
    }

    public void setCart(ArrayList<CheckoutSummaryCart> cart) {
        this.cart = cart;
    }
}
