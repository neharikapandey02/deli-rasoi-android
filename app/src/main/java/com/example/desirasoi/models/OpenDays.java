package com.example.desirasoi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OpenDays implements Serializable {

    @SerializedName("day_name")
    @Expose
    private String day_name;

    @SerializedName("day_slug")
    @Expose
    private String day_slug;

    @SerializedName("open_time")
    @Expose
    private String open_time;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("close_time")
    @Expose
    private String close_time;

    @SerializedName("lunch_start_time")
    @Expose
    private String lunch_start_time;

  @SerializedName("lunch_end_time")
    @Expose
    private String lunch_end_time;

  @SerializedName("take_away_start_time")
    @Expose
    private String take_away_start_time;

  @SerializedName("take_away_end_time")
    @Expose
    private String take_away_end_time;

  @SerializedName("delivery_start_time")
    @Expose
    private String delivery_start_time;

  @SerializedName("delivery_end_time")
    @Expose
    private String delivery_end_time;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLunch_start_time() {
        return lunch_start_time;
    }

    public void setLunch_start_time(String lunch_start_time) {
        this.lunch_start_time = lunch_start_time;
    }

    public String getLunch_end_time() {
        return lunch_end_time;
    }

    public void setLunch_end_time(String lunch_end_time) {
        this.lunch_end_time = lunch_end_time;
    }

    public String getTake_away_start_time() {
        return take_away_start_time;
    }

    public void setTake_away_start_time(String take_away_start_time) {
        this.take_away_start_time = take_away_start_time;
    }

    public String getTake_away_end_time() {
        return take_away_end_time;
    }

    public void setTake_away_end_time(String take_away_end_time) {
        this.take_away_end_time = take_away_end_time;
    }

    public String getDelivery_start_time() {
        return delivery_start_time;
    }

    public void setDelivery_start_time(String delivery_start_time) {
        this.delivery_start_time = delivery_start_time;
    }

    public String getDelivery_end_time() {
        return delivery_end_time;
    }

    public void setDelivery_end_time(String delivery_end_time) {
        this.delivery_end_time = delivery_end_time;
    }

    public String getDay_name() {
        return day_name;
    }

    public void setDay_name(String day_name) {
        this.day_name = day_name;
    }

    public String getDay_slug() {
        return day_slug;
    }

    public void setDay_slug(String day_slug) {
        this.day_slug = day_slug;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }
}
