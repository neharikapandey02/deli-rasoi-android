package com.example.desirasoi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestaurantDetailsMainData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("restaurant_name")
    @Expose
    private String restaurantName;
    @SerializedName("speciality")
    @Expose
    private Object speciality;
    @SerializedName("open_time")
    @Expose
    private String openTime;
    @SerializedName("close_time")
    @Expose
    private String closeTime;
    @SerializedName("restaurant_open_close_day")
    @Expose
    private String restaurantOpenCloseDay;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("delivery_range")
    @Expose
    private String deliveryRange;
    @SerializedName("table_booking_hour")
    @Expose
    private String tableBookingHour;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("food_ready_time_take_away")
    @Expose
    private String food_ready_time_take_away;

   @SerializedName("food_ready_time_delivery")
    @Expose
    private String food_ready_time_delivery;

  @SerializedName("restaurant_status")
    @Expose
    private String restaurant_status;


    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRestaurant_status() {
        return restaurant_status;
    }

    public void setRestaurant_status(String restaurant_status) {
        this.restaurant_status = restaurant_status;
    }

    public String getFood_ready_time_take_away() {
        return food_ready_time_take_away;
    }

    public void setFood_ready_time_take_away(String food_ready_time_take_away) {
        this.food_ready_time_take_away = food_ready_time_take_away;
    }

    public String getFood_ready_time_delivery() {
        return food_ready_time_delivery;
    }

    public void setFood_ready_time_delivery(String food_ready_time_delivery) {
        this.food_ready_time_delivery = food_ready_time_delivery;
    }

    @SerializedName("menu")
    @Expose
    private List<RestaurantMenuData> menu = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public Object getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Object speciality) {
        this.speciality = speciality;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getRestaurantOpenCloseDay() {
        return restaurantOpenCloseDay;
    }

    public void setRestaurantOpenCloseDay(String restaurantOpenCloseDay) {
        this.restaurantOpenCloseDay = restaurantOpenCloseDay;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeliveryRange() {
        return deliveryRange;
    }

    public void setDeliveryRange(String deliveryRange) {
        this.deliveryRange = deliveryRange;
    }

    public String getTableBookingHour() {
        return tableBookingHour;
    }

    public void setTableBookingHour(String tableBookingHour) {
        this.tableBookingHour = tableBookingHour;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<RestaurantMenuData> getMenu() {
        return menu;
    }

    public void setMenu(List<RestaurantMenuData> menu) {
        this.menu = menu;
    }
}
