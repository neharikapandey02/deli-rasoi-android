package com.example.desirasoi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestaurantListMain {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("restaurant_list")
    @Expose
    private List<RestaurantListMainData> restaurantList = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<RestaurantListMainData> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(List<RestaurantListMainData> restaurantList) {
        this.restaurantList = restaurantList;
    }

}
