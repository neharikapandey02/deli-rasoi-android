package com.example.desirasoi.models;

import com.example.desirasoi.activities.menu_details.SpecialDays;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class RestaurantListMainData implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("restaurant_name")
    @Expose
    private String restaurantName;
    @SerializedName("speciality")
    @Expose
    private String speciality;
    @SerializedName("open_time")
    @Expose
    private String openTime;
    @SerializedName("close_time")
    @Expose
    private String closeTime;
    @SerializedName("restaurant_open_close_day")
    @Expose
    private String restaurantOpenCloseDay;
    @SerializedName("take_away_status")
    @Expose
    private String take_away_status;

    @SerializedName("delivery_status")
    @Expose
    private String delivery_status;

 @SerializedName("delivery_charge")
    @Expose
    private String delivery_charge;


    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private Object longitude;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("delivery_range")
    @Expose
    private String deliveryRange;
    @SerializedName("table_booking_hour")
    @Expose
    private String tableBookingHour;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

   @SerializedName("cart_quantity")
    @Expose
    private String cart_quantity;

   @SerializedName("cart_amount")
    @Expose
    private String cart_amount;

   @SerializedName("cart_id")
    @Expose
    private String cart_id;

   @SerializedName("food_ready_time_take_away")
    @Expose
    private String food_ready_time_take_away;

   @SerializedName("food_ready_time_delivery")
    @Expose
    private String food_ready_time_delivery;

   @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("restaurant_status")
    @Expose
    private String restaurant_status;

    @SerializedName("open_days")
    @Expose
    private ArrayList<OpenDays> openDaysArrayList;

    @SerializedName("special_days")
    @Expose
    private ArrayList<SpecialDays> specialDaysArrayList;


    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public ArrayList<SpecialDays> getSpecialDaysArrayList() {
        return specialDaysArrayList;
    }

    public void setSpecialDaysArrayList(ArrayList<SpecialDays> specialDaysArrayList) {
        this.specialDaysArrayList = specialDaysArrayList;
    }

    public String getRestaurant_status() {
        return restaurant_status;
    }

    public void setRestaurant_status(String restaurant_status) {
        this.restaurant_status = restaurant_status;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }

    public String getFood_ready_time_take_away() {
        return food_ready_time_take_away;
    }

    public void setFood_ready_time_take_away(String food_ready_time_take_away) {
        this.food_ready_time_take_away = food_ready_time_take_away;
    }

    public String getFood_ready_time_delivery() {
        return food_ready_time_delivery;
    }

    public void setFood_ready_time_delivery(String food_ready_time_delivery) {
        this.food_ready_time_delivery = food_ready_time_delivery;
    }

    public String getTake_away_status() {
        return take_away_status;
    }

    public void setTake_away_status(String take_away_status) {
        this.take_away_status = take_away_status;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getCart_quantity() {
        return cart_quantity;
    }

    public void setCart_quantity(String cart_quantity) {
        this.cart_quantity = cart_quantity;
    }

    public String getCart_amount() {
        return cart_amount;
    }

    public void setCart_amount(String cart_amount) {
        this.cart_amount = cart_amount;
    }

    public ArrayList<OpenDays> getOpenDaysArrayList() {
        return openDaysArrayList;
    }

    public void setOpenDaysArrayList(ArrayList<OpenDays> openDaysArrayList) {
        this.openDaysArrayList = openDaysArrayList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getRestaurantOpenCloseDay() {
        return restaurantOpenCloseDay;
    }

    public void setRestaurantOpenCloseDay(String restaurantOpenCloseDay) {
        this.restaurantOpenCloseDay = restaurantOpenCloseDay;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeliveryRange() {
        return deliveryRange;
    }

    public void setDeliveryRange(String deliveryRange) {
        this.deliveryRange = deliveryRange;
    }

    public String getTableBookingHour() {
        return tableBookingHour;
    }

    public void setTableBookingHour(String tableBookingHour) {
        this.tableBookingHour = tableBookingHour;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
