package com.example.desirasoi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateProfileMain {
    @SerializedName("user_detail")
    @Expose
    private UpdateProfileMainData userDetail;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    public UpdateProfileMainData getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UpdateProfileMainData userDetail) {
        this.userDetail = userDetail;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
