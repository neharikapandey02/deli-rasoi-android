package com.example.desirasoi.retrofit;


import com.example.desirasoi.activities.aboutus.AboutMain;
import com.example.desirasoi.activities.addaddress.CheckoutMain;
import com.example.desirasoi.activities.contact_us.ContactUsMain;
import com.example.desirasoi.activities.contact_us.GetRestaurantMain;
import com.example.desirasoi.activities.eventandmnagmt.NewsMain;
import com.example.desirasoi.activities.history_details.CancelMainData;
import com.example.desirasoi.activities.menu_details.GetTag;
import com.example.desirasoi.activities.paymenttype.ApplyCouponMain;
import com.example.desirasoi.activities.paymenttype.CouponMain;
import com.example.desirasoi.activities.paymenttype.OrderPlaceMain;
import com.example.desirasoi.activities.ui.history.HistoryMain;
import com.example.desirasoi.activities.ui.logout.SuccessResponse;
import com.example.desirasoi.activities.ui.reserved_table.BookedCancelMain;
import com.example.desirasoi.activities.ui.reserved_table.DateObject;
import com.example.desirasoi.activities.ui.reserved_table.TableBookedMain;
import com.example.desirasoi.activities.ui.reserved_table.TimeObject;
import com.example.desirasoi.adapters.ForgotPasswordMain;
import com.example.desirasoi.adapters.RestaurantTableMain;
import com.example.desirasoi.adapters.TableBookMain;
import com.example.desirasoi.adapters.TableBookMainBooking;
import com.example.desirasoi.models.AddAddressMain;
import com.example.desirasoi.models.AddMinusMain;
import com.example.desirasoi.models.CartMain;
import com.example.desirasoi.models.CategoryListMain;
import com.example.desirasoi.models.CheckoutSummary;
import com.example.desirasoi.models.EditAddressMain;
import com.example.desirasoi.models.FoodMenuMain;
import com.example.desirasoi.models.GetAddressMain;
import com.example.desirasoi.models.GetGuestDetails;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.models.RegistrationMain;
import com.example.desirasoi.models.RestaurantDetailsMain;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.models.UpdateProfileMain;

import java.util.zip.CheckedInputStream;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitApi {

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/register")
    Call<RegistrationMain> dosignup(@Field("first_name") String first_name,
                                    @Field("last_name") String last_name,
                                    @Field("number") String email,
                                    @Field("email") String number,
                                    @Field("password") String password,
                                    @Field("device_id") String device_id,
                                    @Field("device_token") String device_token,
                                    @Field("guest_id") String guestId,
                                    @Field("street_address") String street,
                                    @Field("city") String city,
                                    @Field("postal_code") String postCode,
                                    @Field("latitude") String latitude,
                                    @Field("longitude") String longitude
            );


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/add_address")
    Call<AddAddressMain> doaddAddress(@Field("user_id") String user_id,
                                      @Field("first_name") String name,
                                      @Field("contact") String contact,
                                      @Field("apartment") String apartment,
                                      @Field("street") String street,
                                      @Field("city") String city,
                                      @Field("postal_code") String postal_code,
                                      @Field("latitude") String latitude,
                                      @Field("longitude") String longitude,
                                      @Field("last_name") String lastName);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/edit_address")
    Call<EditAddressMain> doEditAddress(@Field("user_id") String user_id,
                                        @Field("address_id") String address_id,
                                        @Field("first_name") String name,
                                        @Field("contact") String contact,
                                        @Field("apartment") String apartment,
                                        @Field("street") String street,
                                        @Field("city") String city,
                                        @Field("postal_code") String postal_code,
                                        @Field("latitude") String latitude,
                                        @Field("longitude") String longitude,
                                        @Field("last_name") String lastName);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/delete_address")
    Call<EditAddressMain> doDeleteAddress(@Field("user_id") String user_id,
                                          @Field("address_id") String address_id);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/checkout")
    Call<CheckoutMain> docheckout(@Field("user_id") String user_id,
                                  @Field("cart_id") String cart_id,
                                  @Field("service_type") String service_type,
                                  @Field("order_type") String orderType,
                                  @Field("order_date") String date,
                                  @Field("order_time") String time
            );


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/order")
    Call<OrderPlaceMain> doorderplace(@Field("user_id") String user_id,
                                      @Field("checkout_id") String checkout_id,
                                      @Field("payment_type") String payment_type,
                                      @Field("order_type") String orderType,
                                      @Field("order_date") String orderDate,
                                      @Field("order_time") String orderTime,
                                      @Field("comment") String comment
                                      );

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/apply_coupon")
    Call<ApplyCouponMain> doapplycoupans(@Field("user_id") String user_id,
                                         @Field("checkout_id") String checkout_id,
                                         @Field("coupon_code") String coupon_code);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_address")
    Call<GetAddressMain> dogetAddress(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_coupons")
    Call<CouponMain> dogetCoupons(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/login")
    Call<LoginMain> dologin(@Field("email") String email,
                            @Field("password") String password,
                            @Field("device_id") String device_id,
                            @Field("device_token") String device_token,
                            @Field("guest_id") String guest_id);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/fblogin")
    Call<LoginMain> dosociallogin(@Field("facebook_id") String facebook_id,
                                  @Field("email") String email,
                                  @Field("name") String name,
                                  @Field("image") String image,
                                  @Field("device_id") String device_id,
                                  @Field("device_token") String device_token,
                                  @Field("guest_id") String guestId);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/food-menu")
    Call<FoodMenuMain> dofoodMenulist(@Field("user_id") String user_id,
                                      @Field("restaurant_id") String restaurant_id,
                                      @Field("guest_id") String guestId,
                                      @Field("menu_date") String date,
                                      @Field("service_type") String serviceType,
                                      @Field("tags_id") String tags);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/manage-cart")
    Call<AddMinusMain> doAddlist(@Field("user_id") String user_id,
                                 @Field("restaurant_id") String restaurant_id,
                                 @Field("menu_id") String menu_id,
                                 @Field("quantity") String quantity,
                                 @Field("guest_id") String guestId);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/contact")
    Call<ContactUsMain> docontactus(@Field("user_id") String user_id,
                                    @Field("restaurant_id") String restaurant_id,
                                    @Field("name") String name,
                                    @Field("email") String email,
                                    @Field("subject") String subject,
                                    @Field("message") String message);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_table_bookings")
    Call<TableBookedMain> dogetrestaurantbooked(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/cancel_table_booking")
    Call<BookedCancelMain> docanceltablebooked(@Field("user_id") String user_id,
                                               @Field("booking_id") String booking_id);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/food-menu")
    Call<FoodMenuMain> dofoodMenuByCategorylist(@Field("user_id") String user_id,
                                                @Field("restaurant_id") String restaurant_id,
                                                @Field("category_id") String category_id,
                                                @Field("guest_id") String guestId,
                                                @Field("menu_date") String date,
                                                @Field("service_type") String serviceType);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/category")
    Call<CategoryListMain> docategorylist(
            @Field("restaurant_id")String restaurantId,
            @Field("service_type") String serviceType
    );


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/change-password")
    Call<ForgotPasswordMain> dochangepassword(@Field("password") String password,
                                              @Field("confirm_password") String confirm_password,
                                              @Field("old_password") String old_password,
                                              @Field("email") String email);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get-cart")
    Call<CartMain> docartlist(@Field("user_id") String user_id,
                              @Field("guest_id") String guestId,
                              @Field("menu_date") String date);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/forget-password")
    Call<com.example.desirasoi.models.ForgotPasswordMain> doforgotpassword(@Field("email") String password);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/restaurant-detail")
    Call<RestaurantDetailsMain> doResturantDetailsList(@Field("id") String id);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_restaurant_tables")
    Call<RestaurantTableMain> dorestaurantTablelist(
                    @Field("user_id") String user_id,
                    @Field("restaurant_id") String restaurant_id,
                    @Field("booking_date") String date,
                    @Field("booking_time") String time
            );

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_orders")
    Call<HistoryMain> dogetorderhistory(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/cancel_order")
    Call<CancelMainData> docancelorder(@Field("user_id") String user_id,
                                       @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/lunch-menu")
    Call<FoodMenuMain> dolunchMenulist(@Field("user_id") String user_id,
                                       @Field("restaurant_id") String restaurant_id,
                                       @Field("week_menu") String week_menu,
                                       @Field("guest_id") String guestId);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/book_table")
    Call<TableBookMainBooking> dorestaurantTableBook(
                    @Field("user_id") String user_id,
                    @Field("table_id") String table_id,
                    @Field("name") String name,
                    @Field("email") String email,
                    @Field("contact") String contact,
                    @Field("booking_date") String booking_date,
                    @Field("booking_time") String booking_time,
                    @Field("guest_count") String guest_count,
                    @Field("meal_type") String meal_type);


    @Multipart
    @POST("/dev/delirasoi/api/update-profile")
    Call<UpdateProfileMain> doupdateprofile(@Part MultipartBody.Part file,
                                            @Part("user_id") RequestBody user_id,
                                            @Part("first_name") RequestBody first_name,
                                            @Part("last_name") RequestBody last_name,
                                            @Part("email") RequestBody email,
                                            @Part("number") RequestBody number,
                                            @Part("city") RequestBody city,
                                            @Part("street") RequestBody street,
                                            @Part("postal_code") RequestBody postal_code,
                                            @Part("newsletter_status") RequestBody newsLeter,
                                            @Part("latitude") RequestBody lat,
                                            @Part("longitude") RequestBody lng

            /*,
                                            @Part("state") RequestBody state,
                                            @Part("house") RequestBody house*/);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/restaurant")
    Call<RestaurantListMain> dorestaurantlist(
            @Field("user_id") String userId,
            @Field("guest_id") String guestId,
            @Field("latitude") String lat,
            @Field("longitude") String lng
    );

    @POST("/dev/delirasoi/api/all_restaurant")
    Call<GetRestaurantMain> dogetrestaurantlist();

    @POST("/dev/delirasoi/api/news")
    Call<NewsMain> dogetnewslist();

    @POST("/dev/delirasoi/api/about_us")
    Call<AboutMain> dogetaboutus();

    @POST("/dev/delirasoi/api/guest")
    Call<GetGuestDetails> getGuestDetails();

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/checkout")
    Call<CheckoutSummary> getCheckoutSummary(
            @Field("user_id") String userId,
            @Field("cart_id") String checkOutId,
            @Field("latitude") String lat,
            @Field("longitude") String lng,
            @Field("service_type") String service_type,
            @Field("order_type") String order_type,
            @Field("order_date") String order_date,
            @Field("order_time") String order_time,
            @Field("coupon_code") String couponCode
            );

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_restaurant_date_for_table")
    Call<DateObject> getTableDates(
            @Field("restaurant_id") String s);


    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_restaurant_available_dates")
    Call<DateObject> getOrderDates(
            @Field("restaurant_id") String s,
            @Field("service_type") String service_type
            );



    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_restaurant_timing_for_table")
    Call<TimeObject> getTableTime(
            @Field("restaurant_id") String s,
            @Field("order_date") String book_date);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_restaurant_timing_for_order")
    Call<TimeObject> getOrderTime(
            @Field("restaurant_id") String s,
            @Field("order_date") String book_date,
            @Field("service_type") String service_type
            );
    @FormUrlEncoded
    @POST("/dev/delirasoi/api/get_tag")
    Call<GetTag> getTagList(@Field("user_id") String userId);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/update_checkout_address")
    Call<CheckoutMain> docheckout2(
                    @Field("user_id") String userId,
                    @Field("checkout_id") String cartId,
                    @Field("address_id") String addressId);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/checkout_update")
    Call<OrderPlaceMain> checkoutUpdate(
            @Field("user_id") String userId,
            @Field("checkout_id") String checkOutId,
            @Field("comment") String comment,
            @Field("order_type") String orderType,
            @Field("order_date") String orderDate,
            @Field("order_time") String orderTime);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/edit_book_table")
    Call<TableBookMainBooking> updateRestTableBook(
            @Field("user_id") String userId,
            @Field("table_id") String table_id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("contact") String contact,
            @Field("datepicker") String booking_date,
            @Field("timepicker") String booking_time,
            @Field("guest_count") String guest_count,
            @Field("addid") String dataid);

    @FormUrlEncoded
    @POST("/dev/delirasoi/api/logout")
    Call<SuccessResponse> doLogout(
            @Field("user_id") String userId,
            @Field("device_token") String device_token);
}
