package com.example.desirasoi.retrofit;

import android.content.Context;
import android.util.Log;

import com.example.desirasoi.activities.aboutus.AboutMain;
import com.example.desirasoi.activities.addaddress.CheckoutMain;
import com.example.desirasoi.activities.contact_us.ContactUsMain;
import com.example.desirasoi.activities.contact_us.GetRestaurantMain;
import com.example.desirasoi.activities.eventandmnagmt.NewsMain;
import com.example.desirasoi.activities.history_details.CancelMainData;
import com.example.desirasoi.activities.menu_details.GetTag;
import com.example.desirasoi.activities.paymenttype.ApplyCouponMain;
import com.example.desirasoi.activities.paymenttype.CouponMain;
import com.example.desirasoi.activities.paymenttype.OrderPlaceMain;
import com.example.desirasoi.activities.ui.history.HistoryMain;
import com.example.desirasoi.activities.ui.logout.SuccessResponse;
import com.example.desirasoi.activities.ui.reserved_table.BookedCancelMain;
import com.example.desirasoi.activities.ui.reserved_table.DateObject;
import com.example.desirasoi.activities.ui.reserved_table.TableBookedMain;
import com.example.desirasoi.activities.ui.reserved_table.TimeObject;
import com.example.desirasoi.adapters.ForgotPasswordMain;
import com.example.desirasoi.adapters.RestaurantTableMain;
import com.example.desirasoi.adapters.TableBookMain;
import com.example.desirasoi.adapters.TableBookMainBooking;
import com.example.desirasoi.constants.Constants;

import com.example.desirasoi.constants.SharedPreferencesData;
import com.example.desirasoi.models.AddAddressMain;
import com.example.desirasoi.models.AddMinusMain;
import com.example.desirasoi.models.CartMain;
import com.example.desirasoi.models.CategoryListMain;
import com.example.desirasoi.models.CheckoutSummary;
import com.example.desirasoi.models.EditAddressMain;
import com.example.desirasoi.models.FoodMenuMain;
import com.example.desirasoi.models.GetAddressMain;
import com.example.desirasoi.models.GetGuestDetails;
import com.example.desirasoi.models.LoginMain;
import com.example.desirasoi.models.RegistrationMain;
import com.example.desirasoi.models.RestaurantDetailsMain;
import com.example.desirasoi.models.RestaurantListMain;
import com.example.desirasoi.models.UpdateProfileMain;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    private static RetrofitHelper ourInstance = new RetrofitHelper();

    public static RetrofitHelper getInstance() {
        return ourInstance;
    }

    private RetrofitHelper() {
    }

    RetrofitApi retrofitapi;

    public void init(final Context context) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).
                connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Content-Type", "application/json");
                        SharedPreferencesData sharedPrefData=new SharedPreferencesData(context);
                        /*if(sharedPrefData!=null){
                            //Log.e("SecurityKey",sharedPrefData.getSharedData().getTokenSecurity());
                            //ongoing.addHeader("Secret-Key",sharedPrefData.getSharedData().getTokenSecurity());
                            //ongoing.addHeader("Authorization","Bearer "+sharedPrefData.getSharedData().getTokenSecurity());
                            ongoing.addHeader("Authorization","Bearer ");
                            return chain.proceed(ongoing.build());
                        }*//*
                        if(Prefs.getBoolean(context,"session",false)){
                            Log.d(">>>>>>>>>", "intercept: " + Prefs.getString(context,TOKENID,null));
                            ongoing.addHeader("Authorization","Bearer "+Prefs.getString(context,TOKENID, null));

                            return chain.proceed(ongoing.build());
                        }*/
                        if(!sharedPrefData.getSharedPreferenceData(Constants.USER_CREATE,Constants.AUTH_TOKEN).toString().equalsIgnoreCase("")){
                            Log.d(">>>>>>>>>", "intercept: " + sharedPrefData.getSharedPreferenceData(Constants.USER_CREATE,Constants.AUTH_TOKEN));
                            ongoing.addHeader("Authorization",sharedPrefData.getSharedPreferenceData(Constants.USER_CREATE,Constants.AUTH_TOKEN));

                            return chain.proceed(ongoing.build());
                        }
                        return chain.proceed(ongoing.build());

                    }
                })
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        retrofitapi = retrofit.create(RetrofitApi.class);
    }
    public void doSignUp(Callback<RegistrationMain> callback, String firstname,String lastname, String number, String email, String password, String deviceToken,
                         String guestId, String street, String city, String postcode, String lat, String lng){
        Call<RegistrationMain> response;
        response=retrofitapi.dosignup(firstname,lastname,number,email,password,Constants.DEVICE_ID, deviceToken,
                guestId, street, city, postcode, lat, lng);
        response.enqueue(callback);
    }

    public void doLogin(Callback<LoginMain> callback, String email, String password, String deviceToken, String guestId){
        Call<LoginMain> response;
        response=retrofitapi.dologin(email,password,Constants.DEVICE_ID, deviceToken, guestId);
        response.enqueue(callback);
    }

    public void getSocialLogin(Callback<LoginMain> callback, String facebook_id, String email, String name, String image, String device_id, String device_token,
                               String guestId){
        Call<LoginMain> response;
        response=retrofitapi.dosociallogin(facebook_id,email,name, image,device_id,device_token, guestId);
        response.enqueue(callback);
    }
    public void doAddAddress(Callback<AddAddressMain> callback, String user_id, String name, String contact, String apartment, String street, String city, String postal_code, String latitude, String longitude, String lastName){
        Call<AddAddressMain> response;
        response=retrofitapi.doaddAddress(user_id,name,contact,apartment,street,city,postal_code,latitude,longitude, lastName);
        response.enqueue(callback);
    }

    public void doEditAddress(Callback<EditAddressMain> callback, String user_id, String addressId, String name, String contact, String apartment, String street, String city, String postal_code, String latitude, String longitude,
                              String lastName){
        Call<EditAddressMain> response;
        response=retrofitapi.doEditAddress(user_id,addressId,name,contact,apartment,street,city,postal_code,latitude,longitude, lastName);
        response.enqueue(callback);
    }
    public void doDeleteAddress(Callback<EditAddressMain> callback, String user_id, String address_id){
        Call<EditAddressMain> response;
        response=retrofitapi.doDeleteAddress(user_id,address_id);
        response.enqueue(callback);
    }

    public void docheckout(Callback<CheckoutMain> callback, String user_id, String cartId,String serviceType, String orderType, String date, String time){
        Call<CheckoutMain> response;
        response=retrofitapi.docheckout(user_id,cartId,serviceType,orderType, date, time);
        response.enqueue(callback);
    }

    public void doOrderPlace(Callback<OrderPlaceMain> callback, String user_id, String checkoutId,String paymentType,
                             String orderType, String orderDate, String orderTime, String comment){
        Call<OrderPlaceMain> response;
        response=retrofitapi.doorderplace(user_id,checkoutId,paymentType, orderType, orderDate, orderTime, comment);
        response.enqueue(callback);
    }
    public void doApplyCoupons(Callback<ApplyCouponMain> callback, String user_id, String checkoutId, String couponCode){
        Call<ApplyCouponMain> response;
        response=retrofitapi.doapplycoupans(user_id,checkoutId,couponCode);
        response.enqueue(callback);
    }

    public void doGetAddress(Callback<GetAddressMain> callback, String user_id){
        Call<GetAddressMain> response;
        response=retrofitapi.dogetAddress(user_id);
        response.enqueue(callback);
    }

    public void doGetCoupons(Callback<CouponMain> callback, String user_id){
        Call<CouponMain> response;
        response=retrofitapi.dogetCoupons(user_id);
        response.enqueue(callback);
    }

    public void doResturantList(Callback<RestaurantListMain> callback, String userId, String guestId,
                                String lat, String lng){
        Call<RestaurantListMain> response;
        response=retrofitapi.dorestaurantlist(userId, guestId, lat, lng);
        response.enqueue(callback);
    }
    public void doGetResturantList(Callback<GetRestaurantMain> callback){
        Call<GetRestaurantMain> response;
        response=retrofitapi.dogetrestaurantlist();
        response.enqueue(callback);
    }

    public void doGetNewsList(Callback<NewsMain> callback){
        Call<NewsMain> response;
        response=retrofitapi.dogetnewslist();
        response.enqueue(callback);
    }
    public void doGetAboutUs(Callback<AboutMain> callback){
        Call<AboutMain> response;
        response=retrofitapi.dogetaboutus();
        response.enqueue(callback);
    }
    public void doFoodMenuList(Callback<FoodMenuMain> callback, String userId, String restaurantId,
                               String guestId, String date, String serviceType, String tagList){
        Call<FoodMenuMain> response;
        response=retrofitapi.dofoodMenulist(userId,restaurantId, guestId, date, serviceType, tagList);
        response.enqueue(callback);
    }

    public void doContactUs(Callback<ContactUsMain> callback, String userId, String restaurantId,String name,String email,String subject,String message){
        Call<ContactUsMain> response;
        response=retrofitapi.docontactus(userId,restaurantId,name,email,subject,message);
        response.enqueue(callback);
    }

    public void doGetResturantBooked(Callback<TableBookedMain> callback, String userId){
        Call<TableBookedMain> response;
        response=retrofitapi.dogetrestaurantbooked(userId);
        response.enqueue(callback);
    }

    public void doCancelTableBooked(Callback<BookedCancelMain> callback, String userId, String booking_id){
        Call<BookedCancelMain> response;
        response=retrofitapi.docanceltablebooked(userId,booking_id);
        response.enqueue(callback);
    }

    public void doAddItemList(Callback<AddMinusMain> callback, String userId, String restaurantId,String menuId,String quantity, String guestId){
        Call<AddMinusMain> response;
        response=retrofitapi.doAddlist(userId,restaurantId,menuId,quantity, guestId);
        response.enqueue(callback);
    }

    public void doFoodMenuByCatList(Callback<FoodMenuMain> callback, String userId, String restaurantId,String categoryId, String guestId, String date,
                                    String serviceType){
        Call<FoodMenuMain> response;
        response=retrofitapi.dofoodMenuByCategorylist(userId,restaurantId,categoryId, guestId, date, serviceType);
        response.enqueue(callback);
    }

    public void doCategoryList(Callback<CategoryListMain> callback, String restaurantId, String serviceType){
        Call<CategoryListMain> response;
        response=retrofitapi.docategorylist(restaurantId, serviceType);
        response.enqueue(callback);
    }

    public void doCartList(Callback<CartMain> callback, String userId, String guestId, String date){
        Call<CartMain> response;
        response=retrofitapi.docartlist(userId, guestId, date);
        response.enqueue(callback);
    }

    public void doResturantTableList(Callback<RestaurantTableMain> callback, String userId, String restaurantId, String date, String time){
        Call<RestaurantTableMain> response;
        response=retrofitapi.dorestaurantTablelist(userId,restaurantId, date, time);
        response.enqueue(callback);
    }
    public void doLunchMenuList(Callback<FoodMenuMain> callback, String userId, String restaurantId,String weekMenu, String guestId){
        Call<FoodMenuMain> response;
        response=retrofitapi.dolunchMenulist(userId,restaurantId,weekMenu, guestId);
        response.enqueue(callback);
    }
    public void doResturantTableBook(Callback<TableBookMainBooking> callback, String user_id, String table_id, String name, String email, String contact, String booking_date, String booking_time, String guest_count, String meal_type){
        Call<TableBookMainBooking> response;
        response=retrofitapi.dorestaurantTableBook(user_id,table_id,name,email,contact,booking_date,booking_time,guest_count,meal_type);
        response.enqueue(callback);
    }

    public void doResturantDetailsList(Callback<RestaurantDetailsMain> callback,String restaurantId){
        Call<RestaurantDetailsMain> response;
        response=retrofitapi.doResturantDetailsList(restaurantId);
        response.enqueue(callback);
    }

    public void doGetOrderHistory(Callback<HistoryMain> callback, String userId){
        Call<HistoryMain> response;
        response=retrofitapi.dogetorderhistory(userId);
        response.enqueue(callback);
    }

    public void doCancelOrder(Callback<CancelMainData> callback, String userId, String orderId){
        Call<CancelMainData> response;
        response=retrofitapi.docancelorder(userId,orderId);
        response.enqueue(callback);
    }

    public void doChangePassword(Callback<ForgotPasswordMain> callback, String password, String confirm_password, String old_password,String email){
        Call<ForgotPasswordMain> response;
        response=retrofitapi.dochangepassword(password,confirm_password,old_password,email);
        response.enqueue(callback);
    }
    public void doForgotPassword(Callback<com.example.desirasoi.models.ForgotPasswordMain> callback, String email){
        Call<com.example.desirasoi.models.ForgotPasswordMain> response;
        response=retrofitapi.doforgotpassword(email);
        response.enqueue(callback);
    }

   /* public void doLogIn(Callback<LoginResponseMain> callback, String user_email, String password, String device_token){
        Call<LoginResponseMain> response;
        response=retrofitapi.dologin(user_email,password,Constant.ROLEID,Constant.DEVICEID,device_token);
        response.enqueue(callback);
    }


    public void getOtpSend(Callback<OtpResponseMain> callback, String number){
        Call<OtpResponseMain> response;
        response=retrofitapi.doOtpSend(number);
        response.enqueue(callback);
    }

    public void doChangePassword(Callback<ChangePasswordMain> callback, String email, String old_password, String password, String confirm_password){
        Call<ChangePasswordMain> response;
        response=retrofitapi.dochangepassword(email,old_password,password,confirm_password);
        response.enqueue(callback);
    }

    public void doForgotPassword(Callback<ForgotPasswordMain> callback, String user_email){
        Call<ForgotPasswordMain> response;
        response=retrofitapi.doforgotpassword(user_email);
        response.enqueue(callback);
    }

    public void doCartItemGet(Callback<CartItemMain> callback, String userId){
        Call<CartItemMain> response;
        response=retrofitapi.doCartItem(userId,Constant.SINGLE);
        response.enqueue(callback);
    }

    public void callCartUpdate(Callback<CartUpdateMain> callback, String cart_id, String quantity){
        Call<CartUpdateMain> response;
        response=retrofitapi.doCartUpdate(cart_id,quantity);
        response.enqueue(callback);
    }

    public void doStripePayment(Callback<CartUpdateMain> callback, String cart_id, String quantity){
        Call<CartUpdateMain> response;
        response=retrofitapi.dopayment(cart_id,quantity);
        response.enqueue(callback);
    }

    public void doGetRestaurantType(Callback<RestaurantTypeMain> callback){
        Call<RestaurantTypeMain> response;
        response=retrofitapi.dogetrestauranttype(Constant.RESTAURANT_LIST);
        response.enqueue(callback);
    }

    public void doGetRestaurantList(Callback<RestaurantListMain> callback, String user_id){
        Call<RestaurantListMain> response;
        response=retrofitapi.dogetrestaurantlist(user_id);
        response.enqueue(callback);
    }

    public void doEmailVerifier(Callback<VerifyEmailMain> callback, String user_email){
        Call<VerifyEmailMain> response;
        response=retrofitapi.doemailverifier(user_email);
        response.enqueue(callback);
    }

    public void callGetResturantList(Callback<RestaurantListMain> callback, String postalId){
        Call<RestaurantListMain> response;
        response=retrofitapi.doRestaurantList(postalId);
        response.enqueue(callback);
    }

    public void callGetResturantDetailsList(Callback<RestaurantDetailsMain> callback, String restaurantId){
        Call<RestaurantDetailsMain> response;
        response=retrofitapi.doRestaurantDetailsList(restaurantId);
        response.enqueue(callback);
    }


    public void callAddCartItem(Callback<AddCartMain> callback, String item_id, String user_id, String quantity, String amount, String restaurant_id, String plus_minus){
        Call<AddCartMain> response;
        response=retrofitapi.doAddCartItem(item_id,user_id,quantity,amount,Constant.ATTRIBUTEID,Constant.SINGLE,restaurant_id,plus_minus);
        response.enqueue(callback);
    }


    public void getCallSignUp(Callback<SignupMain> callback, String name, String email, String mobile, String country_id, String password, String confirm_password, String device_type, String device_token, String otp){
        Call<SignupMain> response;
        response=retrofitapi.dosignup(name,email,mobile,country_id,password,confirm_password,device_type,device_token,Constant.AGREE,Constant.ROLEID,otp);
        response.enqueue(callback);
    }

    public void callGetResturantSubCategory(Callback<RestaurantSubCategoryMain> callback, String restaurantId){
        Call<RestaurantSubCategoryMain> response;
        response=retrofitapi.doRestaurantSubCategoryList(restaurantId);
        response.enqueue(callback);
    }

    public void getCheckEmailNumber(Callback<EmailNumberCheckMain> callback, String email, String number){
        Call<EmailNumberCheckMain> response;
        response=retrofitapi.doCheckEmailNumber(email,number);
        response.enqueue(callback);
    }

    public void callGetResturantReview(Callback<RestaurantReviewMain> callback, String restaurantId){
        Call<RestaurantReviewMain> response;
        response=retrofitapi.doRestaurantReviewList(restaurantId);
        response.enqueue(callback);
    }

    public void callPostReview(Callback<ReviewPostMain> callback, String restaurantId, String userId, String rating, String review){
        Call<ReviewPostMain> response;
        response=retrofitapi.docallPostReview(restaurantId,userId,rating,review);
        response.enqueue(callback);
    }

    public void doUpdateProfile(Callback<UpdateProfileMain> callback, String pic, String id,
                                String first_name, String country, String city, String state, String zipcode, String number, String email) {
        Call<UpdateProfileMain> response;
        MultipartBody.Part image1 = null;


        if (pic.equalsIgnoreCase("")) {
            image1 = MultipartBody.Part.createFormData("image", "");
        } else {
            File file = new File(pic);

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);

            //here we passed the key for sending the images userFiles[]
            image1 = MultipartBody.Part.createFormData("profile_image", file.getName(), reqFile);
        }

        RequestBody request_id = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody request_first_name = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody request_country = RequestBody.create(MediaType.parse("text/plain"), country);
        RequestBody request_city = RequestBody.create(MediaType.parse("text/plain"), city);
        RequestBody request_state = RequestBody.create(MediaType.parse("text/plain"), state);
        RequestBody request_zipcode = RequestBody.create(MediaType.parse("text/plain"), zipcode);
        RequestBody request_number = RequestBody.create(MediaType.parse("text/plain"), number);
        RequestBody request_email = RequestBody.create(MediaType.parse("text/plain"), email);


        response = retrofitapi.doupdateprofile(image1, request_id,
                request_first_name, request_country, request_city, request_state, request_zipcode,request_number, request_email);
        response.enqueue(callback);
    }
*/

    public void doUpdateProfile(Callback<UpdateProfileMain> callback, String id, String firstName,
                                String lastname, String email, String number, String city, String state, String zipcode, String country,String image,
                                String newsLetter, String lat, String lng) {
        Call<UpdateProfileMain> response;
        MultipartBody.Part image1 = null;


        if (image.equalsIgnoreCase("")) {
            image1 = MultipartBody.Part.createFormData("image", "");
        } else {
            File file = new File(image);

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);

            //here we passed the key for sending the images userFiles[]
            image1 = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        }

        RequestBody request_id = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody request_first_name = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody request_lastname = RequestBody.create(MediaType.parse("text/plain"), lastname);
        RequestBody request_number = RequestBody.create(MediaType.parse("text/plain"), number);
        RequestBody request_email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody request_city = RequestBody.create(MediaType.parse("text/plain"), city);
        RequestBody request_state = RequestBody.create(MediaType.parse("text/plain"), state);
        RequestBody request_zipcode = RequestBody.create(MediaType.parse("text/plain"), zipcode);
        RequestBody request_newsletter = RequestBody.create(MediaType.parse("text/plain"), newsLetter);
        RequestBody request_lat = RequestBody.create(MediaType.parse("text/plain"), lat);
        RequestBody request_lng = RequestBody.create(MediaType.parse("text/plain"), lng);
        RequestBody request_country = RequestBody.create(MediaType.parse("text/plain"), country);
        RequestBody request_house = RequestBody.create(MediaType.parse("text/plain"), "");




        response = retrofitapi.doupdateprofile(image1, request_id,
                request_first_name, request_lastname, request_email, request_number, request_city,request_state, request_zipcode, request_newsletter,
                request_lat, request_lng/*,request_country,request_house*/);
        response.enqueue(callback);
    }

    public void getGuestDetails(Callback<GetGuestDetails> guestCallback) {
        Call<GetGuestDetails> call;
        call = retrofitapi.getGuestDetails();
        call.enqueue(guestCallback);
    }

    public void getCheckoutSummary(Callback<CheckoutSummary> summaryCallback, String userId, String checkOutId,
                                   String lat, String lng, String service_type, String orderType, String date, String time, String couponCode) {
        Call<CheckoutSummary> call;
        call = retrofitapi.getCheckoutSummary(userId, checkOutId, lat, lng, service_type, orderType, date, time, couponCode);
        call.enqueue(summaryCallback);
    }

    public void getTableDates(Callback<DateObject> datesCallback, String id) {
        Call<DateObject> call;
        call = retrofitapi.getTableDates(id);
        call.enqueue(datesCallback);
    }
    public void getOrderDates(Callback<DateObject> datesCallback, String id, String serviceType) {
        Call<DateObject> call;
        call = retrofitapi.getOrderDates(id, serviceType);
        call.enqueue(datesCallback);
    }

    public void getTableTime(Callback<TimeObject> timesCallback, String id, String book_date) {
        Call<TimeObject> call;
        call = retrofitapi.getTableTime(id, book_date);
        call.enqueue(timesCallback);
    }
    public void getOrderTime(Callback<TimeObject> timesCallback, String id, String book_date, String serviceType) {
        Call<TimeObject> call;
        call = retrofitapi.getOrderTime(id, book_date, serviceType);
        call.enqueue(timesCallback);
    }

    public void getTagList(Callback<GetTag> tagCallback, String userId) {
        Call<GetTag> call;
        call = retrofitapi.getTagList(userId);
        call.enqueue(tagCallback);
    }

    public void docheckout2(Callback<CheckoutMain> getCheckoutCallback, String userId, String cartId, String addressId) {
        Call<CheckoutMain> call;
        call = retrofitapi.docheckout2(userId, cartId, addressId);
        call.enqueue(getCheckoutCallback);
    }

    public void checkoutUpdate(Callback<OrderPlaceMain> checkoutUpdateCallback, String userId, String checkOutId, String comment
            , String orderType, String orderDate, String orderTime) {

        Call<OrderPlaceMain> call;
        call = retrofitapi.checkoutUpdate(userId, checkOutId, comment, orderType, orderDate, orderTime);
        call.enqueue(checkoutUpdateCallback);

    }

    public void updateResturantTableBook(Callback<TableBookMainBooking> restaurantTableBook, String userId,
                                         String table_id, String name, String email, String contact,
                                         String booking_date, String booking_time, String guest_count,
                                         String dataid) {
        Call<TableBookMainBooking> call;
        call = retrofitapi.updateRestTableBook(userId, table_id, name, email, contact, booking_date, booking_time,
                guest_count, dataid);
        call.enqueue(restaurantTableBook);

    }

    public void doLogout(Callback<SuccessResponse> logoutCallback, String userId, String device_token) {
        Call<SuccessResponse> call;
        call = retrofitapi.doLogout(userId, device_token);
        call.enqueue(logoutCallback);
    }
}
